plugins {
    kotlin("multiplatform")
    application
}

java.sourceCompatibility = ext.get("javaSourceCompatibility") as JavaVersion

// we centrally defined the gullitools-ext in the root build.gradle-file
// * in groovy build-files we can just use it as
//   implementation(gullitools)
// * in kotlin-gradle-files (build.gradle.kts) things are a bit more complicated
//   1. the gradle-retrieval of the property gullitools is just syntactic sugar for
//      ext.get("gullitools")
//      In Kotlin we don't have this syntactic-groovy-sugar and need to call it explicitly
//   2. ext.get(...) returns Any?, but we need Any here. => So add  '!!' to state, that we expect this to be non-null
//   3. in groovy, we can call this everywhere. In kotlin, it depends on the scope, what ext.get() delivers.
//      => we cannot just call this method, where we want to use it (sourceSets -> implementation),
//         but outside all scopes and later apply it inside our target scope
val gullitoolsCore = ext.get("gullitoolsCore")!!
val javaTarget = (ext.get("javaTarget")!!).toString()

kotlin {
    /* Targets configuration omitted. 
    *  To find out how to configure the targets, please follow the link:
    *  https://kotlinlang.org/docs/reference/building-mpp-with-gradle.html#setting-up-targets */

    js {
        browser{
            webpackTask {
                mainOutputFileName = "RemoveIt.js"
            }
        }
        binaries.executable()
    }

    mingwX64("windows") {
        binaries.executable(listOf(RELEASE))
    }

    linuxArm32Hfp("linux"){
        binaries.executable(listOf(RELEASE))
    }

    macosArm64("mac"){
        binaries.executable (listOf(RELEASE))
    }

    jvm() {
        compilations.all {
            kotlinOptions.jvmTarget = javaTarget
        }
        withJava()
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation(project(":Drawing.Base"))
                implementation(gullitoolsCore)
            }
        }

        val jsMain by getting{
            dependencies{
                implementation("org.jetbrains.kotlin:kotlin-stdlib-js")
            }
        }

        val nativeMain by creating{
            dependsOn(commonMain)
        }

        val windowsMain by getting{
            dependsOn(nativeMain)
        }

        val linuxMain by getting{
            dependsOn(nativeMain)
        }

        val macMain by getting{
            dependsOn(nativeMain)
        }
    }
}

val MAIN_CLASS = "gulligames.removeit.JvmMain"

application {
    mainClass.set(MAIN_CLASS)
}

// jar with dependencies
tasks.register<Jar>("fatjar")  {
    description = "Builds a fat executable jar including all dependencies"
    group = "Build"
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE

    // by default, the jar would be named Gui.jar
    archiveFileName.set("RemoveIt.jar")
    from(sourceSets.main.get().output)

    dependsOn(configurations.runtimeClasspath)

    manifest {
        attributes(Pair("Main-Class", MAIN_CLASS))
    }

    from(
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    )
}

tasks {
    "assemble" {
        dependsOn("fatjar")
    }

    // in a build.gradle simply jvmProcessResources{duplicatesStrategy = DuplicatesStrategy.EXCLUDE}
    // gradle 7 requires to explicitly state the duplicates strategy for all jars/wars/resources
    "jvmProcessResources" {
        val t = this as Copy
        t.duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    }
}
