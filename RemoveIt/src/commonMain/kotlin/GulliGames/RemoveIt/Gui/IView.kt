package gulligames.removeit.gui

interface IView
{
    fun showRemainingStones(remainingStones : Int)
    fun showScore(score : Int)
    fun handleGameOver(gameWidth : Int, gameHeight : Int, stonesLeft : Int, score : Int)
    fun canUndoChanged(canUndoNow : Boolean)
    fun triggerRedraw()
}