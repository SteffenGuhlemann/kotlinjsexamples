package gulligames.removeit.gui

import gulligames.gui.GridShapedGameViewModelBase
import gulligames.removeit.base.Game
import gulligames.removeit.base.GameColor
import gullitools.draw2d.*
import gullitools.util.Index2D

class ViewModel(private val _view : IView, private val _gameWidth : Int = 15, private val _gameHeight : Int = 15)
    : GridShapedGameViewModelBase()
{
    override val BackgroundBrush: Brush
        get() = LightGray.AsBrush
    override val BackgroundPen: Pen?
        get() = null

    override fun getCurrentGridDimensions() = Index2D(_gameWidth, _gameHeight)

    private var _game : Game

    private val _history : ArrayDeque<Game> = ArrayDeque()

    private fun pushCurrentState() = _history.addLast(_game.clone())

    init
    {
        _game = Game(_gameWidth, _gameHeight)
        updateView()
        _view.canUndoChanged(canUndo)
    }

    fun newGame()
    {
        _game = Game(_gameWidth, _gameHeight)

        val couldUndoBefore = canUndo
        _history.clear()

        updateView()

        // can definitely not undo now
        if(couldUndoBefore)
            _view.canUndoChanged(false)
    }

    private val canUndo : Boolean
        get() = !_history.isEmpty()

    fun undo()
    {
        if(!canUndo)
            return

        _game = _history.removeLast()

        if(!canUndo)
            _view.canUndoChanged(false)

        updateView()
    }

    private fun updateView()
    {
        _view.showRemainingStones(_game.StonesLeft)
        _view.showScore(_game.Score)

        _view.triggerRedraw()
    }

    private fun getDrawColor(stone : Index2D) : Color
    {
        if(_game.isMarked(stone))
            return Black;

        return when(_game.getColor(stone))
        {
            GameColor.Empty  -> DarkGray
            GameColor.Blue   -> Blue
            GameColor.Green  -> Green
            GameColor.Purple -> Purple
            GameColor.Red    -> Red
            GameColor.Yellow -> Yellow
        };
    }

    override fun drawSingleGridCell(dc: IDrawingContext, cellRect: Rect, cellIndex: Index2D)
    {
        dc.drawRectangle(getDrawColor(cellIndex).AsBrush, null, cellRect)
    }

    override fun clickOnCell(cellIndex: Index2D)
    {
        val couldUndoBefore = canUndo

        if(_game.isMarked(cellIndex))
            pushCurrentState()

        _game.clickOn(cellIndex)

        if(!couldUndoBefore && canUndo)
            _view.canUndoChanged(true)

        updateView()

        if(_game.IsGameOver)
            _view.handleGameOver(_gameWidth, _gameHeight, _game.StonesLeft, _game.Score)
    }
}