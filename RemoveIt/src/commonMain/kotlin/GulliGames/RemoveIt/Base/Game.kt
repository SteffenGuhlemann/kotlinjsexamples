package gulligames.removeit.base

import gullitools.util.Index2D
import kotlin.math.abs
import kotlin.random.Random

private val _allColors:Array<GameColor> = arrayOf(
    GameColor.Blue,
    GameColor.Green,
    GameColor.Purple,
    GameColor.Red,
    GameColor.Yellow
)

class Game
{
    constructor(width: Int = 20, height: Int = 10)
    {
        _gameArea = Array2D(width, height, GameColor.Empty)

        val rnd = Random.Default

        for(x in 0 .. width - 1)
            for(y in 0 .. height - 1)
                _gameArea[x, y] = _allColors[abs(rnd.nextInt() % _allColors.size)]

        IsGameOver = !areMovesPossible()
        StonesLeft = width * height
    }

    private val _gameArea : Array2D<GameColor>
    private var _marked : Array2D<Boolean>? = null

    private constructor(rhs : Game, includeMarking: Boolean)
    {
        IsGameOver = rhs.IsGameOver
        StonesLeft = rhs.StonesLeft
        Score = rhs.Score
        _gameArea = rhs._gameArea.clone()

        val rhsMarked = rhs._marked
        _marked = if (includeMarking && rhsMarked != null)
                      rhsMarked.clone()
                  else
                      null
    }

    fun clone(includeMarking : Boolean = false) : Game
    {
        return Game(this, includeMarking)
    }

    private fun areMovesPossible():Boolean
    {
        for(x in 0 .. Width - 1)
        {
            if(_gameArea[x, 0] == GameColor.Empty)
                break

            for(y in 0 .. Height - 1)
            {
                val color = _gameArea[x, y]

                if(color == GameColor.Empty)
                    break

                if(x < Width - 1 && _gameArea[x + 1, y] == color)
                    return true

                if(y < Height - 1 && _gameArea[x, y + 1] == color)
                    return true
            }
        }

        return false
    }

    var IsGameOver : Boolean
        private set

    var StonesLeft : Int
        private set

    var Score : Int = 0
        private set

    val Width : Int
        get() = _gameArea.Width

    val Height : Int
        get() = _gameArea.Height

    fun getColor(stone : Index2D) : GameColor = _gameArea[stone]
    fun isMarked(stone: Index2D): Boolean
    {
        val marked = _marked

        if (marked == null)
            return false

        return marked[stone]
    }

    fun clickOn(stone: Index2D)
    {
        if(_gameArea[stone] == GameColor.Empty)
            return

        val marked = _marked

        if(marked != null)
        {
            if(marked[stone])
            {
                removeMarkedStones(marked)
                IsGameOver = !areMovesPossible()

                // now everything is unmarked - no matter if we have clicked on a marked stone or not
                _marked = null
            }
            else
            {
                // unmark last state and remark according to new click
                _marked = getMarked(stone)
            }
        }
        else
        {
            _marked = getMarked(stone)
        }
    }

    private fun getMarked(stone: Index2D) : Array2D<Boolean>?
    {
        val stack = ArrayDeque<Index2D>()
        val startStone = stone
        stack.addLast(startStone)

        val markedStoneSet = HashSet<Index2D>()
        markedStoneSet.add(startStone)

        val color = _gameArea[stone]

        while (!stack.isEmpty())
        {
            val curr = stack.removeLast()
            val neighbors = arrayOf(
                Index2D(curr.X, curr.Y - 1),
                Index2D(curr.X + 1, curr.Y),
                Index2D(curr.X, curr.Y + 1),
                Index2D(curr.X - 1, curr.Y)
            )

            for(neighbor in neighbors)
            {
                if(neighbor.X < 0 || neighbor.X >= Width || neighbor.Y < 0 || neighbor.Y >= Height || markedStoneSet.contains(neighbor) || _gameArea[neighbor] != color)
                    continue

                markedStoneSet.add(neighbor)
                stack.addLast(neighbor)
            }
        }

        if(markedStoneSet.size < 2)
            return null;

        val markedStoneField = Array2D(Width, Height, false)

        for(markedStone in markedStoneSet)
            markedStoneField[markedStone] = true

        return markedStoneField
    }

    private fun removeMarkedStones(markedStones: Array2D<Boolean>)
    {
        var nextEmptyColLeft : Int? = null
        var removeCnt : Int = 0

        for(x in 0 .. Width - 1)
        {
            // 1. fall down in this column
            var nextEmptyBelow : Int? = null
            for(y in 0 .. Height - 1)
            {
                val stone = Index2D(x, y)
                if(_gameArea[stone] == GameColor.Empty)
                    break

                if(markedStones[stone])
                {
                    // i) this stone gets empty
                    _gameArea[stone] = GameColor.Empty

                    ++removeCnt

                    // ii) possibly, this stone is nextEmptyBelow
                    if(nextEmptyBelow == null)
                        nextEmptyBelow = y
                }
                else
                {
                    // possibly fall down to nextEmptyBelow
                    if(nextEmptyBelow != null)
                    {
                        _gameArea[x, nextEmptyBelow] = _gameArea[stone]
                        _gameArea[stone] = GameColor.Empty
                        ++nextEmptyBelow
                    }
                }
            }

            // 2. move to the left or remember for cols to the right, they might move to the left
            if(_gameArea[x, 0] == GameColor.Empty)
            {
                // this col is empty
                if(nextEmptyColLeft == null)
                {
                    // first empty col, otherwise nothing to do as we have more empty cols
                    nextEmptyColLeft = x
                }
            }
            else
            {
                // this col is not empty - possibly move col to the left
                if(nextEmptyColLeft != null)
                {
                    // move whole col to the left
                    for(y in 0 .. Height - 1)
                    {
                        val stone = Index2D(x, y)
                        if(_gameArea[stone] != GameColor.Empty)
                        {
                            _gameArea[nextEmptyColLeft, y] = _gameArea[stone]
                            _gameArea[stone] = GameColor.Empty
                        }
                    }

                    ++nextEmptyColLeft
                }
            }
        }

        StonesLeft -= removeCnt

        val scoreBase = removeCnt - 1
        Score += (scoreBase * scoreBase)
    }
}