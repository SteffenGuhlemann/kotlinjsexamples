package gulligames.removeit.base

enum class GameColor
{
    Empty,
    Red,
    Green,
    Blue,
    Yellow,
    Purple
}