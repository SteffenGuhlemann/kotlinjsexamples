package gulligames.removeit.base

import gullitools.util.Index2D

class Array2D<T>
{
    constructor(width: Int, height: Int, defaultVal : T)
    {
        Width = width
        Height = height
        _arr = ArrayList<T>(width * height)

        for(i in 1 .. width * height)
            _arr.add(defaultVal)
    }

    private constructor(rhs : Array2D<T>)
    {
        Width = rhs.Width
        Height = rhs.Height

        _arr = ArrayList(rhs._arr.size);
        for(i in rhs._arr.indices)
            _arr.add(rhs._arr[i])
    }

    fun clone() : Array2D<T>
    {
        return Array2D(this)
    }

    val Width : Int
    val Height : Int

    private val _arr : ArrayList<T>

    private fun index(x: Int, y: Int) = x * Height + y
    private fun index(cell : Index2D) = cell.X * Height + cell.Y

    operator fun get(cell : Index2D) : T = _arr[index(cell)]

    operator fun get(x: Int, y: Int) : T = _arr[index(x, y)]

    operator fun set(x:Int, y:Int, value: T)
    {
        _arr[index(x,y)] = value
    }

    operator fun set(cell : Index2D, value: T)
    {
        _arr[index(cell)] = value
    }
}