package gulligames.removeit

import gulligames.gui.GameFrameBase
import gullitools.draw2d.IDrawingContext
import gullitools.draw2d.*
import gulligames.removeit.gui.*
import java.awt.*
import javax.swing.*

private val _helpTxt = "<html><h3>Instructions</h3>" +
        "Click on colored stones which have adjacent at least one stone of the same color to mark the group of equally colored stones.<br/>" +
        "A second click on the click will remove the stones. Stones above will fall down, empty columns will disappear. The bigger the<br/>" +
        "group, the more score will be granted.<br/>" +
        "Game ends, if no move is possible. There are two objectives:" +
        "<ul>" +
            "<li>Clear the field of as many stones as possible</li>" +
            "<li>Maximize score by removing large groups of stones together.</li>" +
        "</ul>" +
        "</html>"

class MainFrame : GameFrameBase, IView
{
    private val _remainingStonesLabel = JLabel()
    private val _scoreLabel = JLabel()
    private val _viewModel : ViewModel
    private val _undoMenuItem = JMenuItem("Undo");

    constructor(exitOnClose : Boolean = true, frameTitlePrefix : String = "",  frameTitle : String = "Remove It!")
            : super(exitOnClose, frameTitlePrefix, frameTitle, AppIcons.Icon, "Game", _helpTxt)
    {
        val newGameItem = JMenuItem("New Game")
        GameMenu.add(newGameItem)
        newGameItem.addActionListener{ _ -> newGameClick()}

        GameMenu.add(_undoMenuItem)
        _undoMenuItem.addActionListener { _ -> undoClick()}

        CoreContentPanel.layout = BorderLayout()

        val resultsPanel = JPanel()
        CoreContentPanel.add(resultsPanel, BorderLayout.NORTH)
        resultsPanel.layout = GridBagLayout()

        fun addToGrid(grid: JPanel, component: JComponent, col: Int, row: Int, alignment: Int, fill: Int, wx: Double)
        {
            val c = GridBagConstraints()
            c.insets = Insets(2, 2, 2, 2)
            c.gridx = col
            c.gridy = row
            c.anchor = alignment
            c.fill = fill
            c.weightx = wx
            grid.add(component, c)
        }

        fun addResultLabel(title: String, resultLabel: JLabel, row: Int)
        {
            addToGrid(resultsPanel, JLabel(title), 0, row, GridBagConstraints.EAST, GridBagConstraints.NONE, 0.0)
            addToGrid(resultsPanel, resultLabel, 1, row, GridBagConstraints.WEST, GridBagConstraints.NONE, 0.0)
        }

        addResultLabel("Score: ", _scoreLabel, 0)
        addResultLabel("Remaining stones: ", _remainingStonesLabel, 1)
        CoreContentPanel.add(GamePanel, BorderLayout.CENTER)

        _viewModel = ViewModel(this)

        finishStartup()
    }

    override fun doRepaint(dc: IDrawingContext, drawSize: Size)
    {
        _viewModel?.repaint(dc, drawSize)
    }

    override fun gamePanelClick(pos: gullitools.draw2d.Point, currPanelSize: Size) = _viewModel.click(pos)

    private fun newGameClick() = _viewModel.newGame()
    private fun undoClick() = _viewModel.undo()

    override fun canUndoChanged(canUndoNow: Boolean)
    {
        _undoMenuItem.isEnabled = canUndoNow
    }

    override fun showRemainingStones(remainingStones: Int)
    {
        _remainingStonesLabel.text = remainingStones.toString()
    }

    override fun showScore(score: Int)
    {
        _scoreLabel.text = score.toString()
    }

    override fun handleGameOver(gameWidth: Int, gameHeight: Int, stonesLeft: Int, score: Int)
    {
        JOptionPane.showMessageDialog(
            this,
            "Game over!\r\n\tStones: $stonesLeft\r\n\tScore: $score",
            "Game over",
            JOptionPane.INFORMATION_MESSAGE
        )
    }
}