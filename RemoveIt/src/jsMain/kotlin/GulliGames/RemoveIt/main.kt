package gulligames.removeitt

import gullitools.draw2d.IDrawingContext
import gullitools.draw2d.*
import gullitools.html.HtmlCanvasDrawingContext
import gullitools.html.getElement
import gulligames.removeit.gui.*
import kotlinx.browser.window
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.HTMLDivElement

private class PageHandler : IView
{
    private val _remainingStonesDiv : HTMLDivElement = getElement("RemainingStonesDiv")
    private val _scoreDiv : HTMLDivElement = getElement("ScoreDiv")
    private val _canvas : HTMLCanvasElement = getElement("mainCanvas")
    private val _newGameBtn : HTMLButtonElement = getElement("NewGameBtn")
    private val _undoBtn: HTMLButtonElement = getElement("UndoBtn")
    private val _drawingContext : IDrawingContext
    private val _viewModel : ViewModel

    constructor()
    {
        _drawingContext = HtmlCanvasDrawingContext(_canvas)
        _viewModel = ViewModel(this)
        _canvas.onclick = {e -> _viewModel.click(Point(e.offsetX, e.offsetY))}
        _newGameBtn.onclick = {_ -> _viewModel.newGame()}
        _undoBtn.onclick = {_ -> _viewModel.undo()}
        triggerRedraw()
    }

    override fun canUndoChanged(canUndoNow : Boolean)
    {
        _undoBtn.disabled = !canUndoNow
    }

    override fun triggerRedraw()
    {
        // Intellij warns, that '?.' is unnecessary, but that's wrong. The method is called during initialization
        // and will cause an exception otherwise
        _viewModel?.repaint(_drawingContext, Size(_canvas.width.toDouble(), _canvas.height.toDouble()))
    }

    override fun showRemainingStones(remainingStones: Int)
    {
        _remainingStonesDiv.innerHTML = remainingStones.toString()
    }

    override fun showScore(score: Int)
    {
        _scoreDiv.innerHTML = score.toString()
    }

    override fun handleGameOver(gameWidth: Int, gameHeight: Int, stonesLeft: Int, score: Int)
    {
        println("game over")
        window.alert("Game over!\r\nverbliebene Steine: $stonesLeft\r\nPunkte: $score")
    }
}

fun main()
{
    val handler = PageHandler()
}