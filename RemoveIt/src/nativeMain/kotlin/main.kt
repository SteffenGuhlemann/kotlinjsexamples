import gulligames.removeit.base.*
import kotlin.math.max
import gulligames.util.*
import gullitools.util.Index2D

// for native windows apps, main must be in the root package
fun main()
{
    val game = Game(10, 10)

    val extraSpacePerCol = max(getDigitCount(game.Width - 1), 2) - 2

    while (!game.IsGameOver)
    {
        printGame(game, extraSpacePerCol)
        if(!game.IsGameOver)
        {
            println("Nächster Zug:")
            val x = queryPositiveInt("X", game.Width, -1)
            if(x == null)
                return

            val y = queryPositiveInt("Y", game.Height, -1)
            if(y == null)
                return

            game.clickOn(Index2D(x, y))
        }
    }

    printGame(game, extraSpacePerCol)
    println("Kein weiterer Zug möglich - Game over")
}

private fun printGame(game: Game, extraSpacePerCol: Int)
{
    fun printLine()
    {
        for(x in 0 until game.Width)
            for(i in 1 .. extraSpacePerCol + 3)
                print('-')

        println("-")
    }

    fun getCharToPrint(color: GameColor) : Char = when(color)
    {
        GameColor.Empty -> ' '
        GameColor.Yellow -> 'Y'
        GameColor.Red -> 'R'
        GameColor.Purple -> 'P'
        GameColor.Green -> 'G'
        GameColor.Blue -> 'B'
    }

    printLine()

    for(y in game.Height - 1 downTo 0)
    {
        for(x in 0 until game.Width)
        {
            val stone = Index2D(x, y)
            print('|')
            print(getCharToPrint(game.getColor(stone)))
            val markChar = if(game.isMarked(stone)) '*' else ' '
            print(markChar)

            for(i in 1 .. extraSpacePerCol)
                print(' ')
        }

        println("|  $y")
    }

    printLine()

    for(x in 0 until game.Width)
    {
        print(' ')
        print(x)

        val extraSpaces = extraSpacePerCol + 2 - getDigitCount(x)
        for(i in 1 .. extraSpaces)
            print(' ')
    }

    println()
    println("Noch ${game.StonesLeft} Steine - Punkte: ${game.Score}")
}
