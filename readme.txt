Samples of Kotlin transpiled to Javascript.
All samples are static websites. 
To deploy to an application server together with a summary site, the WebSite-project can be used. 

To build everything:
* call build.bat in the root dir. This will conduct the following steps:
** for each Kotlin2Js-Example:
*** cd into the subdir
*** call build.bat. 
    This will do:
**** call mvn clean package
**** copy relevant html and js to the src-folder of the WebSite project
** cd into WebSite
** call mvn clean package: 
   this will finally build the war to WebSite\target 
