package gulligames.mandelbrot.base

import gullitools.draw2d.Rect
import gullitools.draw2d.contains
import gulligames.io.Console
import kotlin.math.roundToInt

private const val _pixelsPerSpot=2.0
private const val UNKNOWN_DEPTH = -1

class SimpleAdaptiveDepthMandelbrotRenderer : IMandelbrotRenderer
{
    private class FunctionGrid
    {
        constructor(width: Int, height: Int, initialValue : Int = 0)
        {
            Width = width
            Height = height

            _arr = IntArray(width*height) { _ -> initialValue }
        }

        val Width : Int
        val Height : Int

        private val _arr : IntArray

        private fun index(x: Int, y: Int) = x * Height + y

        operator fun get(coords: Coords) : Int = get(coords.X, coords.Y)
        operator fun get(x: Int, y: Int) : Int = _arr[index(x, y)]
        operator fun set(coords: Coords, value: Int)
        {
            set(coords.X, coords.Y, value)
        }

        operator fun set(x:Int, y:Int, value: Int)
        {
            _arr[index(x, y)] = value
        }
    }

    private fun computeGrid1d(intervalBegin : Double, intervalLength:Double, sampleCount: Int) : DoubleArray
    {
        val spotLength = intervalLength / sampleCount
        val arr = DoubleArray(sampleCount)
        for(i in arr.indices)
            arr[i] = (0.5 + i) * spotLength + intervalBegin

        return arr
    }

    private data class Coords(val X : Int, val Y : Int)

    private fun computeSetOfEdgeCoords(xCount : Int, yCount : Int) : HashSet<Coords>
    {
        val edgeSet = HashSet<Coords>()

        // if we add a duplicate this is no problem due to the set
        for(x in 0 until xCount)
        {
            edgeSet.add(Coords(x, 0))
            edgeSet.add(Coords(x, yCount - 1))
        }

        for(y in 0 until yCount)
        {
            edgeSet.add(Coords(0, y))
            edgeSet.add(Coords(xCount - 1, y))
        }

        return edgeSet
    }

    private fun computeFunctionGrid(xGrid: DoubleArray, yGrid: DoubleArray, initialMaxDepth: Int) : FunctionGrid
    {
        // initially
        val functionGrid = FunctionGrid(xGrid.size, yGrid.size, UNKNOWN_DEPTH)
        var maxDepth = initialMaxDepth
        var knownSpots = 0

        // start testing current grid edge
        var unprocessedEdgeSet = computeSetOfEdgeCoords(functionGrid.Width, functionGrid.Height)
        var edgesNotImprovedNow = HashSet<Coords>()

        var improved = true

        while (improved)
        {
            val currentEdgeSet : Set<Coords>
            val currentlyProcessingSpotsForTheFirstTime : Boolean
            if(unprocessedEdgeSet.size > 0)
            {
                currentEdgeSet = unprocessedEdgeSet
                unprocessedEdgeSet = HashSet()

                // retain depth for now
                currentlyProcessingSpotsForTheFirstTime = true
            }
            else
            {
                currentEdgeSet = edgesNotImprovedNow
                edgesNotImprovedNow = HashSet()
                maxDepth += 10
                currentlyProcessingSpotsForTheFirstTime = false
            }

            //ConsoleLog.log("known: $knownSpots of ${functionGrid.Width*functionGrid.Height} - iterating with ${currentEdgeSet.size} spots and maxD=$maxDepth - not improved are ${edgesNotImprovedNow.size}")

            fun tryAddNeighborToEdge(neighborX: Int, neighborY: Int)
            {
                val neighborSpot = Coords(neighborX, neighborY)

                if(neighborX >= 0 && neighborX < functionGrid.Width &&
                   neighborY >= 0 && neighborY < functionGrid.Height &&
                    functionGrid[neighborSpot]== UNKNOWN_DEPTH &&
                    // otherwise it already might have been computed this round already:
                   !currentEdgeSet.contains(neighborSpot) && !edgesNotImprovedNow.contains(neighborSpot))
                {
                    unprocessedEdgeSet.add(neighborSpot)
                }
            }

            for(edgeSpot in currentEdgeSet)
            {
                val depth = Mandelbrot.computeDepth(xGrid[edgeSpot.X], yGrid[edgeSpot.Y], maxDepth)
                if(depth == null)
                {
                    // nothing found here - try again with next depth
                    edgesNotImprovedNow.add(edgeSpot)
                }
                else
                {
                    // 1. remember this depth
                    functionGrid[edgeSpot] = depth
                    ++knownSpots

                    // 2. try to add all neighbors
                    //tryAddNeighborToEdge(edgeSpot.X - 1, edgeSpot.Y-1);
                    tryAddNeighborToEdge(edgeSpot.X - 1, edgeSpot.Y)
                    //tryAddNeighborToEdge(edgeSpot.X-1, edgeSpot.Y+1)
                    tryAddNeighborToEdge(edgeSpot.X, edgeSpot.Y - 1)
                    tryAddNeighborToEdge(edgeSpot.X, edgeSpot.Y + 1)
                    //tryAddNeighborToEdge(edgeSpot.X+1, edgeSpot.Y-1)
                    tryAddNeighborToEdge(edgeSpot.X + 1, edgeSpot.Y)
                    //tryAddNeighborToEdge(edgeSpot.X+1, edgeSpot.Y+1)
                }
            }

            improved = currentlyProcessingSpotsForTheFirstTime || unprocessedEdgeSet.size > 0

            // edge case: if we select
        }

        return functionGrid
    }

    private data class PastDraw(val FunctionSpaceRect : Rect, val MaxDepth : Int)

    private val _lastDraws : ArrayDeque<PastDraw> = ArrayDeque()

    private fun getInitialMaxDepth(functionSpaceRect: Rect) : Int
    {
        while (!_lastDraws.isEmpty())
        {
            val lastDraw = _lastDraws.last()

            // if the new rect is inside the old one
            //  => zoom in
            //  => keep old draw on the stack and use it's max depth as starting point
            if(lastDraw.FunctionSpaceRect.contains(functionSpaceRect))
                return lastDraw.MaxDepth

            // we are not inside the last rect
            // => not zoomed in
            // => zoom out until we again are inside a draw (if possible)
            _lastDraws.removeLast()
        }

        // nothing on stack
        return 1000
    }

    override fun renderTo(screenRect: Rect, functionSpaceRect: Rect, renderContext: IMandelbrotRenderContext)
    {
        val initialMaxDepth = getInitialMaxDepth(functionSpaceRect)

        val xCount : Int = (screenRect.Width / _pixelsPerSpot).roundToInt()
        val yCount : Int = (screenRect.Height / _pixelsPerSpot).roundToInt()

        val xGrid = computeGrid1d(functionSpaceRect.Left, functionSpaceRect.Width, xCount)
        val yGrid = computeGrid1d(functionSpaceRect.Top, functionSpaceRect.Height, yCount)
        val functionGrid = computeFunctionGrid(xGrid, yGrid, initialMaxDepth)

        fun getDepthToDraw(depthInGrid: Int) : Int?
        {
            return if (depthInGrid == UNKNOWN_DEPTH)
                null
            else
                depthInGrid
        }

        var maxDepth = 0
        var minDepth = Int.MAX_VALUE
        for(x in xGrid.indices)
        {
            val screenLeft = 1.0 * x * _pixelsPerSpot + screenRect.Left
            for(y in yGrid.indices)
            {
                val screenTop = 1.0 * y * _pixelsPerSpot + screenRect.Top
                val depth : Int? = getDepthToDraw(functionGrid[x, y])

                if(depth != null)
                {
                    if (depth > maxDepth)
                        maxDepth = depth

                    if (depth < minDepth)
                        minDepth = depth
                }

                renderContext.renderRect(Rect(screenLeft, screenTop, _pixelsPerSpot, _pixelsPerSpot), depth)
            }
        }

        _lastDraws.addLast(PastDraw(functionSpaceRect, maxDepth))
        Console.log("drawn in depth range $minDepth - $maxDepth")
    }
}