package gulligames.mandelbrot.base

import gullitools.draw2d.Rect
import kotlin.math.roundToInt

private const val _pixelsPerSpot = 2.0

class SimpleMandelbrotRenderer (var MaxDepth: Int) : IMandelbrotRenderer
{
    override fun renderTo(screenRect: Rect, functionSpaceRect: Rect, renderContext: IMandelbrotRenderContext)
    {
        val xCount : Int = (screenRect.Width / _pixelsPerSpot).roundToInt()
        val yCount : Int = (screenRect.Height / _pixelsPerSpot).roundToInt()

        val widthSpotFunctionSpace = functionSpaceRect.Width/xCount
        val heightSpotFunctionSpace = functionSpaceRect.Height/yCount

        for(x in 0 until xCount)
        {
            val screenLeft = 1.0 * x * _pixelsPerSpot + screenRect.Left
            val xFunctionSpace = (0.5 + x) * widthSpotFunctionSpace + functionSpaceRect.Left

            for(y in 0 until yCount)
            {
                val screenTop = 1.0 * y * _pixelsPerSpot + screenRect.Top
                val yFunctionSpace = (0.5 + y) * heightSpotFunctionSpace + functionSpaceRect.Top

                val depth = Mandelbrot.computeDepth(xFunctionSpace, yFunctionSpace, MaxDepth)
                renderContext.renderRect(Rect(screenLeft, screenTop, _pixelsPerSpot, _pixelsPerSpot), depth)
            }
        }
    }
}