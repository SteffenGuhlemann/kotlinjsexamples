package gulligames.mandelbrot.base

import gullitools.draw2d.Color
import gullitools.draw2d.interpolateColor

interface IColorProvider
{
    fun getColor(depth: Int?) : Color
}

class AlternatingColorProvider(private val _unknownDepthColor: Color, private val _alternatingColors : Array<Color>) : IColorProvider
{
    override fun getColor(depth: Int?): Color
    {
        return if (depth == null)
            _unknownDepthColor
        else
            _alternatingColors[depth % _alternatingColors.size]
    }
}

class InterpolatingColorProvider(private val _unknownDepthColor : Color, private val _startColor : Color, private val _endColor : Color, private val _interpolationDepth : Int)
    : IColorProvider
{
    override fun getColor(depth: Int?): Color
    {
        if(depth == null)
            return _unknownDepthColor

        val effectiveDepth = depth % _interpolationDepth
        val relativeDepth = 1f * effectiveDepth / _interpolationDepth
        return interpolateColor(_startColor, _endColor, 1.0 - relativeDepth)
    }
}