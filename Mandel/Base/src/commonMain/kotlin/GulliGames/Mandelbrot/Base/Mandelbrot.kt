package gulligames.mandelbrot.base

class Mandelbrot
{
    private class ComplexNumber(val Real: Double, val Imaginary: Double)
    {
        constructor()
            : this(0.0,0.0)
        {}

        operator fun plus(rhs: ComplexNumber) : ComplexNumber
        {
            return ComplexNumber(Real + rhs.Real, Imaginary + rhs.Imaginary)
        }

        operator fun times(rhs: ComplexNumber) : ComplexNumber
        {
            // a = r1 + i1 * i
            // b = r2 + i1 * i
            // a*b = (r1 + i1 * i) * (r2 + i2 * i)
            //     = (r1 * r2 - i1 * i2) + (r1 * i2 + r2 * i1) * i
            return ComplexNumber(Real * rhs.Real - Imaginary * rhs.Imaginary,
                            Real  * rhs.Imaginary + rhs.Real * Imaginary)
        }

        fun absSqr() : Double
        {
            return Real * Real + Imaginary * Imaginary
        }
    }

    companion object
    {
        private fun computeDepthNaive(x: Double, y: Double, limitDepth: Int) : Int?
        {
            val c = ComplexNumber(x, y)

            // after step 1
            var z = c
            var step = 1

            while (step < limitDepth)
            {
                if(z.absSqr() >= 4)
                    return step

                z = z * z + c
                ++step
            }

            return null
        }

        fun computeDepth(x: Double, y:Double, limitDepth: Int) : Int?
        {
            return computeDepthNaive(x, y, limitDepth)
        }
    }
}

