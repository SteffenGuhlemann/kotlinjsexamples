package gulligames.mandelbrot.base

import gullitools.draw2d.Rect

interface IMandelbrotRenderContext
{
    fun renderRect(rect: Rect, depth: Int?)
    var ColorProvider : IColorProvider
}

interface IMandelbrotRenderer
{
    fun renderTo(screenRect : Rect, functionSpaceRect: Rect, renderContext: IMandelbrotRenderContext)
}