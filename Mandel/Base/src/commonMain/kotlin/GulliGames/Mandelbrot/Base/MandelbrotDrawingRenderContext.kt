package gulligames.mandelbrot.base

import gullitools.draw2d.IDrawingContext
import gullitools.draw2d.*

class MandelbrotDrawingRenderContext(private val _context: IDrawingContext, override var ColorProvider : IColorProvider) : IMandelbrotRenderContext
{
    override fun renderRect(rect: Rect, depth: Int?)
    {
        _context.drawRectangle(ColorProvider.getColor(depth).AsBrush, null, rect)
    }
}