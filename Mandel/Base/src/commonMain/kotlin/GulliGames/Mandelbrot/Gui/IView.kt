package gulligames.mandelbrot.gui

import gullitools.draw2d.*

interface IView
{
    fun drawRubberBandOnTopOfImage(pen : Pen, rubberBandRect : Rect)
    fun triggerRedraw(functionRect : Rect)
    fun triggerRefreshOverviewCanvas(functionRectHistory : List<Rect>, currMainFunctionRect : Rect)
}