package gulligames.mandelbrot.gui

import gulligames.mandelbrot.base.IColorProvider
import gulligames.mandelbrot.base.IMandelbrotRenderer
import gulligames.mandelbrot.base.MandelbrotDrawingRenderContext
import gullitools.draw2d.*
import gulligames.io.Console
import gullitools.util.Stopwatch
import kotlin.math.max
import kotlin.math.min

private val _startFunctionRect = Rect(-2.0, -2.0, 4.0, 4.0)

class ViewModel(private val _view : IView, private var _colorProvider: IColorProvider, private var _renderer : IMandelbrotRenderer, private var _shouldRetainAspectRatio: Boolean)
{
    private val _functionRectHistory = ArrayDeque<Rect>()
    private val _canUndoChangedListeners: ArrayList<() -> (Unit)> = ArrayList()
    private var _positionHighlightColor: Color = Red
    private var _rubberbandColor: Color = Green
    private var _lastMouseDown: Point? = null

    init
    {
        refreshUndoState()

        // first redraw is complicated, as not everything is wired up by now.
        // (we already have a reference to the view, but the view is just creating us and thus has no reference to us)
        //        _view.triggerRedraw(_startFunctionRect)
        // -> do not call _view.triggerRedraw(). This would cause the following problem:
        //   _view.triggerRedraw() internally creates the drawing context and then calls _viewModel.repaint(...)
        //  Problem is, _viewModel is not yet assigned (as we are still in the ctor of _viewModel)
        //  => null reference exception

        // instead "remember" the current rect by placing it in the functionRectHistory
        _functionRectHistory.add(_startFunctionRect)

        // when finished creating the viewModel, the view will call redraw, and if we "remember" a rect, we can redraw it)
    }

    fun setPosition(position: Rect)
    {
        _functionRectHistory.clear()
        _view.triggerRedraw(position)
    }

    var PositionHighlightColor: Color
        get() = _positionHighlightColor
        set(value)
        {
            _positionHighlightColor = value
            repaint()
        }

    var RubberbandColor: Color
        get() = _rubberbandColor
        set(value)
        {
            _rubberbandColor = value
            repaint()
        }

    var ShouldRetainAspectRatio: Boolean
        get() = _shouldRetainAspectRatio
        set(value)
        {
            _shouldRetainAspectRatio = value

            if (!ShouldRetainAspectRatio)
            {
                // we were supposed to retain the aspect ratio, but now, we are not any more
                // => last draw call was something like: plot a function rect, which is 0.1 wide and 0.2 tall
                // => our code decided, that to retain aspect ratio and show all selected area, the drawn rect should be 0.2 wide and tall
                // => now we don't know any more the size of the original call (0.1 by 0.2) and we don't need to
                // => future draws on new rects will draw something different, but current draws don't change
                // ==> don't redraw now:
                //    1. save the computation time
                //    2. don't spoil the redraw stack
                return
            }

            val lastRect = _functionRectHistory.lastOrNull()
            if (lastRect != null)
            {
                val rectWhichWouldBeDrawn = getFunctionRectToDrawConsideringAspectRatio(lastRect)

                // it might be, the last rect was already retaining aspect ratio, even if we don't forced it to
                // (e.g. we just clicked on the checkbox twice)
                // ==> don't redraw now:
                //    1. save the computation time
                //    2. don't spoil the redraw stack
                if (rectWhichWouldBeDrawn == lastRect)
                    return

                _view.triggerRedraw(lastRect)
            }
        }

    var ColorProvider: IColorProvider
        get() = _colorProvider
        set(value)
        {
            _colorProvider = value
            repaint()
        }

    var Renderer: IMandelbrotRenderer
        get() = _renderer
        set(value)
        {
            _renderer = value
            repaint()
        }

    private fun getFunctionRectToDrawConsideringAspectRatio(functionRect: Rect): Rect
    {
        if (!_shouldRetainAspectRatio)
            return functionRect

        // Aspect ratio is always 1:1
        // => use larger of width and height
        val length = max(functionRect.Width, functionRect.Height)
        return Rect(functionRect.Left, functionRect.Top, length, length)
    }

    fun Canvas_MouseDown(position: Point)
    {
        _lastMouseDown = position
    }

    fun Canvas_MouseMove(position: Point)
    {
        val mouseDown = _lastMouseDown
        if (mouseDown != null)
        {
            val xMin = min(position.X, mouseDown.X)
            val xMax = max(position.X, mouseDown.X)

            val yMin = min(position.Y, mouseDown.Y)
            val yMax = max(position.Y, mouseDown.Y)

            _view.drawRubberBandOnTopOfImage(Pen(_rubberbandColor), Rect(xMin, yMin, xMax - xMin, yMax - yMin))
        }
    }

    fun Canvas_MouseUp(position: Point)
    {
        val mouseDownCanvas = _lastMouseDown
        _lastMouseDown = null

        val functionRect = _functionRectHistory.lastOrNull()

        val lastDrawSize = _lastDrawSize
        if (mouseDownCanvas != null && functionRect != null && lastDrawSize != null)
        {
            fun toFunctionPos1d(canvasPos: Double, canvasLength: Double, functionMin: Double, functionLength: Double): Double
            {
                return functionMin + canvasPos / canvasLength * functionLength
            }

            fun toFunctionPt(canvasPt: Point): Point = Point(
                toFunctionPos1d(canvasPt.X, 1.0 * lastDrawSize.Width, functionRect.Left, functionRect.Width),
                toFunctionPos1d(canvasPt.Y, 1.0 * lastDrawSize.Height, functionRect.Top, functionRect.Height))

            val mouseDownFunction = toFunctionPt(mouseDownCanvas)
            val mouseUpFunction = toFunctionPt(position)

            val xMin = min(mouseDownFunction.X, mouseUpFunction.X)
            val xMax = max(mouseDownFunction.X, mouseUpFunction.X)
            val yMin = min(mouseDownFunction.Y, mouseUpFunction.Y)
            val yMax = max(mouseDownFunction.Y, mouseUpFunction.Y)

            fun Double.isValid() = !isNaN() && isFinite()

            if (!xMax.isValid() || !xMin.isValid() || !yMax.isValid() || !yMin.isValid() || xMax <= xMin || yMax <= yMin)
                return // invalid rect

            _view.triggerRedraw(Rect(xMin, yMin, xMax - xMin, yMax - yMin))
        }
    }

    fun Canvas_MouseLeave()
    {
        _lastMouseDown = null
    }

    fun repaint()
    {
        val lastRect = _functionRectHistory.lastOrNull()
        if (lastRect == null)
        // nothing to repaint
            return

        // if we just repaint, we will see the current (and at the same time last) rect in the overview panel - which is not the point of repaint
        // => remove it from stack
        _functionRectHistory.removeLast()
        _view.triggerRedraw(lastRect)
    }

    fun undo()
    {
        if (_functionRectHistory.size < 2)
            return

        _functionRectHistory.removeLast()
        refreshUndoState()

        // most of the time, this will be shown
        // dop it first, it will be added again automatically
        val lastRect = _functionRectHistory.removeLast()
        _view.triggerRedraw(lastRect)
    }

    fun addCanUndoChangedListener(listener: () -> (Unit))
    {
        _canUndoChangedListeners.add(listener)
    }

    val CanUndo: Boolean
        get() = _functionRectHistory.size >= 2

    private fun refreshUndoState()
    {
        for (listener in _canUndoChangedListeners)
        {
            try
            {
                listener()
            } catch (ex: Exception)
            {
            }
        }
    }

    fun undoUntil(functionRect: Rect)
    {
        while (_functionRectHistory.size > 0 && _functionRectHistory.last() != functionRect)
        {
            _functionRectHistory.removeLast()
            refreshUndoState()
        }

        if (_functionRectHistory.size == 0)
        {
            _view.triggerRedraw(_startFunctionRect)
        }
        else
        {
            val lastRect = _functionRectHistory.last()
            _functionRectHistory.removeLast()
            _view.triggerRedraw(lastRect)
        }
    }

    private inline fun getCurrRectInOverviewCanvas(currFunctionRect: Rect, overviewFunctionRect: Rect, overviewCanvasScreenRect: Rect): Rect
    {
        val screenLeft = (currFunctionRect.Left - overviewFunctionRect.Left) / overviewFunctionRect.Width * overviewCanvasScreenRect.Width + overviewCanvasScreenRect.Left
        val screenTop = (currFunctionRect.Top - overviewFunctionRect.Top) / overviewFunctionRect.Height * overviewCanvasScreenRect.Height + overviewCanvasScreenRect.Top
        val screenWidth = currFunctionRect.Width / overviewFunctionRect.Width * overviewCanvasScreenRect.Width
        val screenHeight = currFunctionRect.Height / overviewFunctionRect.Height * overviewCanvasScreenRect.Height

        return Rect(screenLeft, screenTop, screenWidth, screenHeight)
    }

    fun paintRectInOverviewCanvas(dc: IDrawingContext, overviewCanvasRect : Rect, historicFunctionRect: Rect, currMainFunctionRect : Rect)
    {
        val renderContext = MandelbrotDrawingRenderContext(dc, _colorProvider)
        _renderer.renderTo(overviewCanvasRect, historicFunctionRect, renderContext)
        val rectToDraw = getCurrRectInOverviewCanvas(currMainFunctionRect, historicFunctionRect, overviewCanvasRect)

        val positionHighlightPen = Pen(_positionHighlightColor, 2.0)

        // 1. draw rect itself
        dc.drawRectangle(null, positionHighlightPen, rectToDraw)

        // 2. if the rect itself is too small, draw cross-hairs
        if(rectToDraw.Width < 2 || rectToDraw.Height<2)
        {
            val halfLen = max(5.0, min(overviewCanvasRect.Width, overviewCanvasRect.Height) * 0.1)
            val mid = Point(rectToDraw.Left + 0.5 * rectToDraw.Width, rectToDraw.Top + 0.5 * rectToDraw.Height)

            dc.drawLine(positionHighlightPen, Point(mid.X, mid.Y-halfLen), Point(mid.X, mid.Y+halfLen))
            dc.drawLine(positionHighlightPen, Point(mid.X-halfLen, mid.Y), Point(mid.X+halfLen, mid.Y))
        }
    }

    private var _lastDrawSize : Size? = null

    fun repaint(dc: IDrawingContext, drawSize : Size, functionRect : Rect)
    {
        _lastDrawSize = drawSize
        val functionRectToDraw = getFunctionRectToDrawConsideringAspectRatio(functionRect)

        val sw = Stopwatch()

        val canvasRect = Rect(0.0, 0.0, drawSize.Width, drawSize.Height)
        _renderer.renderTo(canvasRect, functionRectToDraw, MandelbrotDrawingRenderContext(dc, ColorProvider))

        _view.triggerRefreshOverviewCanvas(_functionRectHistory, functionRectToDraw)

        // don't add the draw after undo
        val lastRect = _functionRectHistory.lastOrNull()
        if(lastRect == null)
        {
            _functionRectHistory.addLast(functionRectToDraw)
        }
        else if(lastRect != functionRectToDraw)
        {
            // this is not a plain undo (otherwise lastRect == functionRectToDraw)
            // usually we would just stack the new function rect onto the draw stack.

            if(lastRect == functionRect)
            {
                // However, it might be (this execution branch), that this is a redraw
                // * due to undo with now retaining aspect ratio (and initially not) or
                // * due to forcing retain-aspect-ratio by clicking the checkbox
                // => in this situation replace the last Rect by the new one - we are usually making the rect bigger by
                //    enforcing aspect ratio - so undo zoom would be quite misleading
                _functionRectHistory.removeLast()
            }

            // anyhow, store the rect drawn now
            _functionRectHistory.addLast(functionRectToDraw)
        }

        Console.log("rendering of $functionRectToDraw took ${sw.ElapsedSeconds} s")
        refreshUndoState()
    }
}
