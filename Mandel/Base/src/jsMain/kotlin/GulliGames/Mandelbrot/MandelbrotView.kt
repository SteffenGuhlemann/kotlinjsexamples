package gulligames.mandelbrot

import gulligames.mandelbrot.base.IColorProvider
import gulligames.mandelbrot.base.IMandelbrotRenderer
import gullitools.draw2d.*
import gullitools.html.HtmlCanvasDrawingContext
import gulligames.mandelbrot.gui.*
import kotlinx.browser.document
import kotlinx.dom.clear
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.ImageData
import org.w3c.dom.get

private fun HTMLCanvasElement.getRect() = Rect(0.0, 0.0, 1.0 * width, 1.0 * height)

class MandelbrotView : IView
{
    private val _mainCanvas : HTMLCanvasElement
    private val _mainDrawingContext : HtmlCanvasDrawingContext
    private val _overviewDiv : HTMLDivElement
    val ViewModel : ViewModel

    constructor(colorProvider : IColorProvider, renderer: IMandelbrotRenderer, shouldRetainAspectRatio: Boolean,
        mainCanvas : HTMLCanvasElement, overviewDiv : HTMLDivElement)
    {
        _mainCanvas = mainCanvas
        _overviewDiv = overviewDiv
        _mainDrawingContext = HtmlCanvasDrawingContext(_mainCanvas)

        ViewModel = ViewModel(this, colorProvider, renderer, shouldRetainAspectRatio)
        ViewModel.repaint()

        _mainCanvas.onmousedown = { e -> ViewModel.Canvas_MouseDown(Point(e.offsetX, e.offsetY))}
        _mainCanvas.onpointerdown = { e -> ViewModel.Canvas_MouseDown(Point(e.offsetX, e.offsetY))}

        _mainCanvas.onmousemove = { e -> ViewModel.Canvas_MouseMove(Point(e.offsetX, e.offsetY))}
        _mainCanvas.onpointermove = { e -> ViewModel.Canvas_MouseMove(Point(e.offsetX, e.offsetY))}

        _mainCanvas.onmouseup = { e -> ViewModel.Canvas_MouseUp(Point(e.offsetX, e.offsetY))}
        _mainCanvas.onpointerup = { e -> ViewModel.Canvas_MouseUp(Point(e.offsetX, e.offsetY))}

        _mainCanvas.onmouseleave = { _ -> ViewModel.Canvas_MouseLeave()}
        _mainCanvas.onpointerleave = { _ -> ViewModel.Canvas_MouseLeave()}
    }

    override fun drawRubberBandOnTopOfImage(pen: Pen, rubberBandRect: Rect)
    {
        val imgData = _lastImageData
        if(imgData == null)
            return

        _mainDrawingContext.replotEarlierDrawnImage(imgData)
        _mainDrawingContext.drawRectangle(null, pen, rubberBandRect)
    }

    private var _lastImageData : ImageData? = null

    override fun triggerRedraw(functionRect: Rect)
    {
        val mainCanvasSize = Size(1.0 * _mainCanvas.width, 1.0 * _mainCanvas.height)
        ViewModel.repaint(_mainDrawingContext, mainCanvasSize, functionRect)
        _lastImageData = _mainDrawingContext.getDrawingAsImage()
    }

    override fun triggerRefreshOverviewCanvas(functionRectHistory: List<Rect>, currMainFunctionRect: Rect)
    {
        // clean up event handlers which could leak memory
        for(childIndex in 0 until _overviewDiv.childElementCount)
            (_overviewDiv.children[childIndex] as HTMLCanvasElement).onclick = null

        _overviewDiv.clear()

        val overviewWidth = _overviewDiv.clientWidth
        for(fctRect in functionRectHistory)
        {
            val canvas = document.createElement("Canvas") as HTMLCanvasElement
            canvas.width = overviewWidth
            canvas.height = overviewWidth
            canvas.style.margin="5px"
            _overviewDiv.appendChild(canvas)
            canvas.onclick = {_ -> ViewModel.undoUntil(fctRect)}

            val overviewCanvasRect = canvas.getRect()
            val dc = HtmlCanvasDrawingContext(canvas)
            ViewModel.paintRectInOverviewCanvas(dc, overviewCanvasRect, fctRect, currMainFunctionRect)
        }
    }
}