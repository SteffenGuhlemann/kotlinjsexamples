import gullitools.draw2d.*
import gulligames.mandelbrot.base.*
import gulligames.mandelbrot.MandelbrotView
import gulligames.mandelbrot.gui.ViewModel
import kotlinx.browser.document
import org.w3c.dom.Element
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.HTMLDivElement
import kotlin.math.min

private const val _margin : Int = 6 // actual margin + 1 safety pixel
private fun setSize(width : Int, height : Int, mainCanvas : HTMLCanvasElement, overviewDiv : HTMLDivElement)
{
    fun getMainCanvasHeightToUse() : Int
    {
        // by default we use maincanvas 800 x 800 and beside this overview 800 x 200

        // => if the user presents a naked div, clientHeight is huge (1400), but clientWidth is 0
        // if the user has set width/height using CSS (<div style="width: 500px;height: 400px"></div>), clientWidth/clientHeight will contain the specified values
        val minLen = min(height, (width-_margin) * 4 / 5)
        return if (minLen > 0) minLen else 800
    }

    val mainCanvasLenToUse = getMainCanvasHeightToUse()
    // it seems, only setting the width and height (below) actually sets it - otherwise we are 300 x 150
    //mainCanvas.style.width="800px";
    //mainCanvas.style.height="800px";
    mainCanvas.width = mainCanvasLenToUse
    mainCanvas.height = mainCanvasLenToUse

    overviewDiv.style.maxHeight="${mainCanvasLenToUse}px"
    overviewDiv.style.width="${mainCanvasLenToUse/4+_margin}px"
}

@OptIn(ExperimentalJsExport::class)
internal class MyMandelbrot : IMandelbrot
{
    private val _view: MandelbrotView
    private val _viewModel : ViewModel
    private val _mainCanvas : HTMLCanvasElement
    private val _overviewDiv : HTMLDivElement

    constructor(container : Element)
    {
        // 1. create the necessary elements
        // we don't know if element already contains something
        // => create our own root div
        // like this:
        //<div style="display: flex; flex-direction: row">
        //    <canvas id="mainCanvas" width="800" height="800"></canvas>
        //    <div style="display: flex; flex-direction: column-reverse; overflow-y: scroll; max-height: 800px" id="OverviewDiv"></div>
        //</div>

        // understand, whether we had an initial size of container before
        val initialWidth = container.clientWidth
        val initialHeight = container.clientHeight

        //<div style="display: flex; flex-direction: row">
        val rootDiv = document.createElement("Div") as HTMLDivElement
        rootDiv.style.display = "flex"
        rootDiv.style.flexDirection = "row"

        container.appendChild(rootDiv)

        //<canvas id="mainCanvas" width="800" height="800"></canvas>
        _mainCanvas = document.createElement("Canvas") as HTMLCanvasElement

        // it seems, only setting the width and height (below) actually sets it - otherwise we are 300 x 150
        //mainCanvas.style.width="800px";
        //mainCanvas.style.height="800px";
        rootDiv.appendChild(_mainCanvas)

        //<div style="display: flex; flex-direction: column-reverse; overflow-y: auto; max-height: 800px" id="OverviewDiv"></div>
        _overviewDiv = document.createElement("Div") as HTMLDivElement
        _overviewDiv.style.display = "flex"
        _overviewDiv.style.flexDirection = "column-reverse"
        _overviewDiv.style.overflowY = "auto"
        rootDiv.appendChild(_overviewDiv)

        setSize(initialWidth, initialHeight, _mainCanvas, _overviewDiv)

        // 2. setup and run Mandelbrot
        val colorProvider = InterpolatingColorProvider(Blue, Black, White, 10)
        //val colorProvider = AlternatingColorProvider(Color(0f, 0f, 0f), Color(1f, 0f, 0f), Color(0f, 1f, 0f), Color(0f, 0f, 1f) )

        val renderer = SimpleAdaptiveDepthMandelbrotRenderer()

        _view = MandelbrotView(colorProvider, renderer, true, _mainCanvas, _overviewDiv)
        _viewModel = _view.ViewModel
    }

    override var Width: Int
        get() = _mainCanvas.width + _overviewDiv.clientWidth
        set(value)
        {
            setSize(value, Height, _mainCanvas, _overviewDiv)
            _viewModel.repaint()
        }

    override var Height: Int
        get() = _mainCanvas.height
        set(value)
        {
            setSize(Width, value, _mainCanvas, _overviewDiv)
            _viewModel.repaint()
        }

    override var ShouldRetainAspectRatio: Boolean
        get() = _viewModel.ShouldRetainAspectRatio
        set(value)
        {
            _viewModel.ShouldRetainAspectRatio = value
        }

    override val CanUndo: Boolean
        get() = _viewModel.CanUndo

    override fun undo()
    {
        _viewModel.undo()
    }

    override var ColorProvider: IColorProvider
        get() = _viewModel.ColorProvider
        set(value)
        {
            _viewModel.ColorProvider = value
        }

    override var Renderer: IMandelbrotRenderer
        get() = _viewModel.Renderer
        set(value)
        {
            _viewModel.Renderer = value
        }

    override var PositionHighlightColor: Color
        get() = _viewModel.PositionHighlightColor
        set(value)
        {
            _viewModel.PositionHighlightColor = value
        }

    override var RubberbandColor: Color
        get() = _viewModel.RubberbandColor
        set(value)
        {
            _viewModel.RubberbandColor = value
        }

    override fun addCanUndoChangedListener(listener: () -> Unit) =  _viewModel.addCanUndoChangedListener(listener)

    override fun setPosition(left: Double, top: Double, width: Double, height: Double)
    {
        _viewModel.setPosition(Rect(left, top, width, height))
    }
}

