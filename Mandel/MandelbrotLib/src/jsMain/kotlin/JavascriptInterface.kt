import gullitools.draw2d.Color
import gullitools.html.getElement
import gulligames.mandelbrot.base.*
import org.w3c.dom.Element

@ExperimentalJsExport
@JsExport
interface IMandelbrot
{
    var Width : Int
    var Height : Int

    @JsName("setPosition")
    fun setPosition(left : Double, top : Double, width : Double, height : Double)
    fun undo()
    var ShouldRetainAspectRatio : Boolean
    val CanUndo : Boolean
    var ColorProvider : IColorProvider
    var Renderer : IMandelbrotRenderer
    var PositionHighlightColor : Color
    var RubberbandColor : Color

    @JsName("addCanUndoChangedListener")
    fun addCanUndoChangedListener(listener : () -> (Unit))
}

@ExperimentalJsExport
@JsExport
@JsName("createInHtmlElementWithId")
fun createInHtmlElementWithId(htmlId : String) = createInHtmlElement(getElement(htmlId))

@ExperimentalJsExport
@JsExport
@JsName("createInHtmlElement")
fun createInHtmlElement(container: Element) : IMandelbrot = MyMandelbrot(container)

@ExperimentalJsExport
@JsExport
@JsName("rgb")
fun rgb(r: Int, g: Int, b: Int) = gullitools.draw2d.rgb(r,g,b)

@ExperimentalJsExport
@JsExport
@JsName("alternatingColors")
fun alternatingColors(unknownDepthColor: Color, alternatingColors : Array<Color>) : IColorProvider
{
    // AlternatingColorProvider(unknownDepthColor, alternatingColors)
    //val colors = arrayOf(*alternatingColors)
    return AlternatingColorProvider(unknownDepthColor, alternatingColors)
}

@ExperimentalJsExport
@JsExport
@JsName("interpolatedColors")
fun interpolatedColors(unknownDepthColor: Color, startColor : Color, endColor : Color, interpolationDepth : Int) : IColorProvider
{
    return InterpolatingColorProvider(unknownDepthColor, startColor, endColor, interpolationDepth);
}

@ExperimentalJsExport
@JsExport
@JsName("adaptiveRenderer")
fun adaptiveRenderer() : IMandelbrotRenderer = SimpleAdaptiveDepthMandelbrotRenderer()

@ExperimentalJsExport
@JsExport
@JsName("simpleRenderer")
fun simpleRenderer(maxDepth : Int) = SimpleMandelbrotRenderer(maxDepth)
