package gulligames.mandelbrot

import gulligames.gui.GameFrameBase
import gullitools.draw2d.*
import gulligames.mandelbrot.base.*
import gullitools.draw2d.Point
import gullitools.controls.DrawCanvas
import gulligames.mandelbrot.gui.*
import java.awt.*
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import java.awt.event.MouseMotionListener
import javax.swing.*
import kotlin.math.max

private const val _overviewPicSideLen = 100
private const val _overviewPicMargin = 5

private val _helpTxt = "<html><h3>Mandelbrot (Apfelmännchen) instructions</h3>"+
        "This show the mandelbrot set - a nice fractal - zoom in and find surprising new structures - rotating spires or even smaller apple men.<br/>"+
        "<h4>Usage</h3>"+
        "<ul>"+
            "<li>On the left there is the main view of the fractal.<br/>" +
                "Choose an interesting area and select it."+
                "<ul>"+
                    "<li>Click on one corner of the interesting area and hold the left mouse button pressed.</li>"+
                    "<li>Move the mouse to pull a rubber band rect to select the area of interest.</li>"+
                    "<li>Release the left mouse button to actually select that area.</li>"+
                    "<li>The view zooms into the selected area.</li>"+
                "</ul>"+
            "</li>"+
            "<li>As you continuously zoom in, you" +
                "<ul>"+
                    "<li>might wonder, where you currently are in the big picture.</li>" +
                    "<li>might want to go back one or many zoom steps.</li>"+
                "</ul>"+
                "For this, the right side shows the history:"+
                "<ul>"+
                    "<li>Each box shows one zoom step.</li>"+
                    "<li>Most recent steps are shown on top.</li>"+
                    "<li>To go back to that step, just click on it.</li>" +
                    "<li>A red rectangle or square denotes the position of the currently shown area in this previous step.</li>"+
                "</ul>"+
            "</li>"+
            "<li>As you pull the rect to zoom in, you'll likely not always pull a (perfect) square.<br/>" +
                "Technically it is possible to draw a non-square mandelbrot rectangle into the squared region in the window<br/>"+
                "but the mandelbrot structures will look skewed." +
                "<ul>" +
                    "<li>If you want to see skewed mandelbrot, uncheck \"Retain aspect ratio\" and zoom into a non-square rect-</li>" +
                    "<li>By default (Retain aspect ration), zoom will select a bit more than the pulled rect to retain a square.</li>" +
                "</ul>" +
            "</li>"+
        "</ul>"

class MainFrame : GameFrameBase, IView
{
    val ViewModel : ViewModel
    private val _overviewPanel = JPanel()
    private val _retainAspectRatioCb = JCheckBox("Retain aspect ratio")

    constructor(exitOnClose : Boolean = true, frameTitlePrefix : String = "", frameTitle : String = "Mandelbrot")
            : super(exitOnClose, frameTitlePrefix, frameTitle, AppIcons.Icon, null, _helpTxt)
    {
        val colorProvider = InterpolatingColorProvider(Blue, Black, White, 10)
        val renderer = SimpleAdaptiveDepthMandelbrotRenderer()
        val shouldRetainAspectRatio = true
        ViewModel = ViewModel(this, colorProvider, renderer, shouldRetainAspectRatio)
        CoreContentPanel.layout = BorderLayout()

        val cbPanel = JPanel()
        cbPanel.layout = BoxLayout(cbPanel, BoxLayout.Y_AXIS)
        cbPanel.add(Box.createRigidArea(Dimension(0, 5)))

        val hMarginPanel = JPanel()
        cbPanel.add(hMarginPanel)
        hMarginPanel.layout = BoxLayout(hMarginPanel, BoxLayout.X_AXIS)
        hMarginPanel.add(Box.createRigidArea(Dimension(5, 0)))
        hMarginPanel.add(_retainAspectRatioCb)
        hMarginPanel.add(Box.createGlue())
        hMarginPanel.add(Box.createRigidArea(Dimension(5, 0)))
        cbPanel.add(Box.createRigidArea(Dimension(0, 5)))

        _retainAspectRatioCb.toolTipText = "Retain a squared zoom, even if user selects a non-square."
        _retainAspectRatioCb.isSelected = shouldRetainAspectRatio
        _retainAspectRatioCb.addActionListener { ViewModel.ShouldRetainAspectRatio = _retainAspectRatioCb.isSelected }

        CoreContentPanel.add(cbPanel, BorderLayout.NORTH)

        _overviewPanel.preferredSize = Dimension(_overviewPicSideLen + 2 *  _overviewPicMargin, (0.9 * CoreContentPanel.preferredSize.height).toInt())

        val overviewScrollPane = JScrollPane(_overviewPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED)
        overviewScrollPane.preferredSize = Dimension(_overviewPanel.preferredSize.width + 20, _overviewPanel.preferredSize.height)

        CoreContentPanel.add(overviewScrollPane, BorderLayout.EAST)
        CoreContentPanel.add(GamePanel, BorderLayout.CENTER)

        GamePanel.addMouseListener(MyMouseListener())
        GamePanel.addMouseMotionListener(MyMouseMotionListener())

        finishStartup()
    }

    private inner class MyMouseListener : MouseListener
    {
        override fun mouseClicked(e: MouseEvent?)
        {}

        override fun mousePressed(e: MouseEvent)
        {
            // mouse down
            ViewModel.Canvas_MouseDown(e.position())
        }

        override fun mouseReleased(e: MouseEvent)
        {
            ViewModel.Canvas_MouseUp(e.position())
        }

        override fun mouseEntered(e: MouseEvent?)
        {}

        override fun mouseExited(e: MouseEvent)
        {
            ViewModel.Canvas_MouseLeave()
        }
    }

    private inner class MyMouseMotionListener : MouseMotionListener
    {
        // == mouse move with left down
        override fun mouseDragged(e: MouseEvent)
        {
            ViewModel.Canvas_MouseMove(e.position())
        }

        // == mouse moved with left up
        override fun mouseMoved(e: MouseEvent)
        {
            ViewModel.Canvas_MouseMove(e.position())
        }
    }

    override fun doRepaint(dc: IDrawingContext, drawSize: Size)
    {
        val fctRect = _currFunctionRect
        if(fctRect != null)
            ViewModel?.repaint(dc, drawSize, fctRect)
    }

    override fun drawRubberBandOnTopOfImage(pen: Pen, rubberBandRect: Rect)
    {
        GamePanel.drawOverlay{ dc, size ->
            dc.drawRectangle(null, pen, rubberBandRect)
        }
    }

    private var _currFunctionRect : Rect? = null

    override fun triggerRedraw()
    {
        ViewModel.repaint()
    }

    override fun triggerRedraw(functionRect: Rect)
    {
        _currFunctionRect = functionRect
        GamePanel.repaint()
        GamePanel.toolTipText = "<html>Mandelbrot rect: $functionRect</html>"
    }

    override fun triggerRefreshOverviewCanvas(functionRectHistory: List<Rect>, currMainFunctionRect: Rect)
    {
        _overviewPanel.removeAll()
        _overviewPanel.layout = BoxLayout(_overviewPanel, BoxLayout.Y_AXIS)

        val height = max((0.9 * GamePanel.height).toInt(), (_overviewPicSideLen + _overviewPicMargin) * functionRectHistory.size + _overviewPicMargin)

        _overviewPanel.preferredSize = Dimension(_overviewPicSideLen + 2 * _overviewPicMargin, height)
        _overviewPanel.add(Box.createGlue())
        _overviewPanel.add(Box.createRigidArea(Dimension(0, _overviewPicMargin)))

        for(i in functionRectHistory.size -1 downTo 0)
        {
            val fctRect = functionRectHistory[i]

            val marginPanel = JPanel()
            marginPanel.layout = BoxLayout(marginPanel, BoxLayout.X_AXIS)
            marginPanel.add(Box.createRigidArea(Dimension(_overviewPicMargin, 0)))

            // if we count the current view, the first shown step is not zoomed - i.e. zoom step 0
            // in total we have functionRectHistory.size zoom steps
            // (e.g. if there are 2 steps on history - the first one is no zoom. Then there is one zoom step. The final zoom step is
            // not on the history, but in the main area)
            val tooltip = "<html>Zoom step $i (out of ${functionRectHistory.size})<br/>Mandelbrot area: $fctRect</html>"
            marginPanel.add(SingleStepOverviewPanel(fctRect, currMainFunctionRect, tooltip))
            marginPanel.add(Box.createRigidArea(Dimension(_overviewPicMargin, 0)))
            _overviewPanel.add(marginPanel)
            _overviewPanel.add(Box.createRigidArea(Dimension(0, _overviewPicMargin)))
        }

        _overviewPanel.revalidate()
    }

    private inner class SingleStepOverviewPanel : JPanel
    {
        private val _fctRect : Rect
        private val _currMainFctRect : Rect
        private val _drawCanvas : DrawCanvas

        constructor(fctRect : Rect, currMainFunctionRect: Rect, tooltip : String)
        {
            _fctRect = fctRect
            _currMainFctRect = currMainFunctionRect

            this@SingleStepOverviewPanel.layout = BorderLayout()
            _drawCanvas = DrawCanvas(::doRepaintSingleStep, ::handleClick)
            _drawCanvas.toolTipText = tooltip
            this@SingleStepOverviewPanel.add(_drawCanvas, BorderLayout.CENTER)
            this@SingleStepOverviewPanel.preferredSize = Dimension(_overviewPanel.width, _overviewPanel.width)
            this@SingleStepOverviewPanel.maximumSize = this@SingleStepOverviewPanel.preferredSize
            this@SingleStepOverviewPanel.minimumSize = this@SingleStepOverviewPanel.preferredSize

            this@SingleStepOverviewPanel.isVisible = true
        }

        private fun doRepaintSingleStep(dc : IDrawingContext, size : Size)
        {
            ViewModel.paintRectInOverviewCanvas(dc, Rect(0.0, 0.0, size.Width, size.Height), _fctRect, _currMainFctRect)
        }

        private fun handleClick(pos : Point, size : Size)
        {
            ViewModel.undoUntil(_fctRect)
        }
    }
}

private fun MouseEvent.position() = Point(x.toDouble(), y.toDouble())