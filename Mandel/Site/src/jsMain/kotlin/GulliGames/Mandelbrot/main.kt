package gulligames.mandelbrot

import gullitools.draw2d.*
import gullitools.html.getElement
import gulligames.mandelbrot.base.*
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLInputElement

fun main()
{
    val colorProvider = InterpolatingColorProvider(Blue, Black, White, 10)
    //val colorProvider = AlternatingColorProvider(Color(0f, 0f, 0f), Color(1f, 0f, 0f), Color(0f, 1f, 0f), Color(0f, 0f, 1f) )

    val renderer = SimpleAdaptiveDepthMandelbrotRenderer()
    //val renderer = SimpleMandelbrotRenderer(50)

    SiteHandler(colorProvider, renderer)
}

private class SiteHandler
{
    private val _pageRunner : MandelbrotView
    private val _undoBtn : HTMLButtonElement = getElement("UndoZoomBtn")
    private val _retainAspectRatioCb : HTMLInputElement = getElement("RetainAspectRatioCb")

    constructor(colorProvider: IColorProvider, renderer : IMandelbrotRenderer)
    {
        val shouldRetainAspectRatio = _retainAspectRatioCb.checked;

        val mainCanvas : HTMLCanvasElement = getElement("mainCanvas")
        val overviewDiv : HTMLDivElement = getElement("OverviewDiv")

        _pageRunner = MandelbrotView(colorProvider, renderer, shouldRetainAspectRatio, mainCanvas, overviewDiv)

        _pageRunner.ViewModel.addCanUndoChangedListener { _undoBtn.disabled = !_pageRunner.ViewModel.CanUndo }

        _undoBtn.disabled = !_pageRunner.ViewModel.CanUndo
        _undoBtn.onclick = {_ -> _pageRunner.ViewModel.undo()}
        _retainAspectRatioCb.onchange = {_ -> onShouldRetainAspectRatioChanged()}
    }

    private fun onShouldRetainAspectRatioChanged()
    {
        _pageRunner.ViewModel.ShouldRetainAspectRatio = _retainAspectRatioCb.checked
    }
}
