import gulligames.prim.factorIntoPrims
import gullitools.html.getElement
import gullitools.io.toListString
import gullitools.util.Stopwatch
import org.w3c.dom.*

private val _titleH1 : HTMLElement = getElement("titleH1")
private val _mainDiv : HTMLDivElement = getElement("mainDiv")
private val _numberInput : HTMLInputElement = getElement("numberInput")
private val _outputParagraph : HTMLParagraphElement = getElement("outputParagraph")
private val _errorParagraph : HTMLParagraphElement = getElement("errorParagraph")

fun main()
{
    getElement<HTMLButtonElement>("countUntilBtn").addEventListener("click", {onCountUntilClicked()})

    _numberInput.valueAsNumber = 42.0
    _numberInput.addEventListener("change", {onNumberChange()})
    onNumberChange()
}

private fun onCountUntilClicked()
{
    val longNo = getCurrentNumber()
    if(longNo == null)
        return

    val sw = Stopwatch()
    var cnt = 0U
    for (i in 1UL .. longNo)
    {
        val factors = i.factorIntoPrims()
        cnt += factors.sumOf { it.Count }
    }

    getElement<HTMLParagraphElement>("performanceOutputParagraph").innerText = "Until $longNo found total $cnt factors in ${sw.Elapsed}"
}

private fun getCurrentNumber() : ULong?
{
    // the input box represents doubles, but we compute with longs.
    // sometimes, double is not exact enough to represent the long
    // if e.g. entering "10000000000000001",
    // _numberInput.valueAsNumber.toULong() extracts "10000000000000000" and factors this number:
    // 10000000000000000 := 2^16 * 5^16
    val inputTxt = _numberInput.value
    val longNo = inputTxt.toULongOrNull()
    if(longNo == null || longNo < 1UL)
    {
        // e.g. someone entered something like "1.2345"
        _errorParagraph.innerText = "no positive integral number"
        _errorParagraph.style.visibility = "visible"
        _outputParagraph.innerText = ""
        _outputParagraph.style.visibility = "collapsed"
        return null
    }

    _outputParagraph.style.visibility = "visible"
    _errorParagraph.style.visibility = "collapsed"
    _errorParagraph.innerText = ""

    return longNo
}

private fun onNumberChange()
{
    val longNo = getCurrentNumber()
    if(longNo == null)
        return

    // => at least store the number back into the textbox, such that the user gets a consistent experience
    val factors = longNo.factorIntoPrims()

    val txt = if (factors.size == 1 && factors[0].Count == 1U)
        "$longNo is prim!"
    else
        "$longNo := ${factors.toListString(separator = " * ")}"

    _outputParagraph.innerText = txt
}
