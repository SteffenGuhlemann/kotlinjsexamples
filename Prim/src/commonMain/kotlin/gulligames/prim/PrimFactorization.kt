package gulligames.prim

import kotlin.math.ceil
import kotlin.math.sqrt

data class PrimFactor(val Factor : ULong, val Count : UInt)
{
    override fun toString() = if(Count == 1U) Factor.toString() else "$Factor^$Count"
}

fun ULong.factorIntoPrims() : List<PrimFactor>
{
    val res = ArrayList<PrimFactor>()
    var lastFactor = findSmallestPrimDivisor()
    var lastFactorCount : UInt = 1U

    var remainingNumber = this / lastFactor

    while (remainingNumber > 1UL)
    {
        val nextFactor = remainingNumber.findSmallestPrimDivisor()
        remainingNumber /= nextFactor

        if(nextFactor == lastFactor)
        {
            ++lastFactorCount
        }
        else
        {
            res.add(PrimFactor(lastFactor, lastFactorCount))
            lastFactor = nextFactor
            lastFactorCount = 1U
        }
    }

    res.add(PrimFactor(lastFactor, lastFactorCount))
    return res
}

private fun ULong.findSmallestPrimDivisor() : ULong
{
    if(this < 4UL)
        return this

    if(this % 2UL == 0UL)
        return 2UL

    val upperLimit = ceil(sqrt(toDouble())).toULong() + 1UL
    var i = 3UL
    while (i <= upperLimit)
    {
        if(this % i == 0UL)
            return i

        i += 2UL
    }

    // found no divisor
    return this
}

