import gulligames.prim.factorIntoPrims
import gullitools.io.Culture
import gullitools.io.toListString
import gullitools.util.Stopwatch

private class UsageException(msg : String) : Exception(msg)

fun main(args : Array<String>)
{
    try
    {
        if(args.isEmpty() || args.size > 2)
            throw UsageException("")

        fun parseNo(argIndex : Int) : ULong
        {
            val arg = args[argIndex]
            val no = Culture.CurrentCulture.tryParseLong(arg, true)
            if(no == null)
                throw UsageException("'$arg' is not a valid long'")

            return no.toULong()
        }

        if(args.size == 1)
            return runFactorization(parseNo(0))

        val no = parseNo(1)
        val action = args[0].lowercase()
        if(action == "-single" || action == "single" || action == "-s")
            return runFactorization(no)

        if(action == "-until" || action == "until" || action == "-u")
            return runFactorizationUntil(no)

        if(action == "-countuntil" || action == "countuntil" || action == "-cu")
            return countFactorsUntil(no)

        throw UsageException("'${args[0]}' is no valid action")
    }
    catch (uex : UsageException)
    {
        val msg = uex.message
        if(!msg.isNullOrEmpty())
            println(msg)

        println("Usage: prim [[-]until|[-]single]|[[-]countuntil] <positive-number-to-factor>")
        println("\t [-]until <number>: computes all factorizations until <number>")
        println("\t [-]single <number>: factorizes a single number (default)")
        println("\t [-]countuntil <number>: factorizes all numbers until <number> and counts the total factors")
    }
}

private fun countFactorsUntil(number: ULong)
{
    val sw = Stopwatch()
    var cnt = 0U
    for (i in 1UL .. number)
    {
        val factors = i.factorIntoPrims()
        cnt += factors.sumOf { it.Count }
    }

    println("Until $number found total $cnt factors in ${sw.Elapsed}")
}

private fun runFactorizationUntil(number : ULong)
{
    val sw = Stopwatch()
    for (i in 1UL .. number)
        runFactorization(i)

    println("took ${sw.Elapsed}")
}

private fun runFactorization(number: ULong)
{
    val factors = number.factorIntoPrims()
    if(factors.size  == 1 && factors[0].Count == 1U)
    {
        println("$number is prim!")
        return
    }

    println("$number := ${factors.toListString(separator = " * ")}")
}