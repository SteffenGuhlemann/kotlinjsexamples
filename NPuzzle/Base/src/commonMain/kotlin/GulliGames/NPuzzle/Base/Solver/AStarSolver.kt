package gulligames.npuzzle.base.solver

import gulligames.npuzzle.base.TotalEvaluatedStateCnt
import gullitools.collections.Heap
import gullitools.collections.IPriorityQueue
import gulligames.io.Console
import gulligames.npuzzle.base.ICoreNPuzzleGame
import gulligames.npuzzle.base.NPuzzleGame
import gulligames.npuzzle.base.estimateRemainingCosts
import gullitools.util.Index2D
import kotlin.jvm.JvmStatic

private fun ICoreNPuzzleGame.appendMove(move : Index2D, currentCosts: Int) : AStarState
{
    val newGame = NPuzzleGame(clone())
    newGame.tryMove(move)
    return AStarState.createWithActualCostsUntilHere(newGame, currentCosts + 1)
}

@ConsistentCopyVisibility
private data class AStarState private constructor(val Game : ICoreNPuzzleGame, val TotalEstimatedCosts : Int)
{
    companion object
    {
        @JvmStatic
        fun createWithActualCostsUntilHere(game : NPuzzleGame, currentCosts: Int) = AStarState(game.CoreGame, currentCosts + estimateRemainingCosts(game))

        @JvmStatic
        fun createWithTotalEstimatedCosts(coreGame : ICoreNPuzzleGame, estimatedCosts: Int) = AStarState(coreGame, estimatedCosts)
    }
}

internal fun solveNPuzzleAStar(game : NPuzzleGame, useLayeredSearchQueue: Boolean) : Int
{
    val queue = createAStarPriorityQueue(AStarState.createWithActualCostsUntilHere(game, 0), useLayeredSearchQueue)

    val investigatedStates = HashSet<ICoreNPuzzleGame>()
    investigatedStates.add(game.CoreGame)

    while(queue.size() > 0)
    {
        val curr = queue.dequeue()
        investigatedStates.add(curr.Game)
        ++TotalEvaluatedStateCnt

        val currGame = NPuzzleGame(curr.Game)
        val currentCosts = curr.TotalEstimatedCosts - estimateRemainingCosts(currGame)
        if(currGame.IsSolved)
            return currentCosts

        if(investigatedStates.size % 1000000 == 0)
            Console.log("investigated : ${investigatedStates.size}, ${queue.sizeDescription(curr.TotalEstimatedCosts)}")

        for(move in currGame.getPossibleMoves())
        {
            val next = curr.Game.appendMove(move, currentCosts)
            if(investigatedStates.contains(next.Game))
                continue

            queue.enqueue(next)
        }
    }

    throw IllegalStateException("No solution found")
}

// A Star queue
private interface IAStarPriorityQueue
{
    fun size(): Int
    fun enqueue(element: AStarState)
    fun dequeue(): AStarState
    fun sizeDescription(currEstimatedTotalCosts : Int) : String
}

private object AStarComp : Comparator<AStarState>
{
    override fun compare(lhs: AStarState, rhs: AStarState): Int
    {
        return lhs.TotalEstimatedCosts.compareTo(rhs.TotalEstimatedCosts)
    }
}

private fun createAStarPriorityQueue(firstElem : AStarState, useLayeredSearchQueue: Boolean) : IAStarPriorityQueue
{
    if(useLayeredSearchQueue)
        return LayeredAStarPriorityQueue(firstElem)

    val heap = Heap(AStarComp)
    heap.enqueue(firstElem)
    return AStarHeapQueue(heap)
}

private class AStarHeapQueue(private val _priorityQueue : IPriorityQueue<AStarState>) : IAStarPriorityQueue
{
    override fun size() = _priorityQueue.size

    override fun enqueue(element: AStarState)
    {
        _priorityQueue.enqueue(element)
    }

    override fun dequeue() = _priorityQueue.dequeue()

    override fun sizeDescription(currEstimatedTotalCosts: Int) = "queue: ${size()} (curr >= $currEstimatedTotalCosts)"
}

private class LayeredAStarPriorityQueue : IAStarPriorityQueue
{
    private class SingleLayer(firstElement : AStarState)
    {
        val TotalEstimatedCost = firstElement.TotalEstimatedCosts
        private val _layerQueue = ArrayDeque<ICoreNPuzzleGame>()

        val Size : Int
            get() = _layerQueue.size

        fun enqueue(state: AStarState)
        {
            // stack behaviour (depth search inside one layer) to decrease memory footprint
            _layerQueue.addLast(state.Game)
        }

        fun dequeue() = AStarState.createWithTotalEstimatedCosts(_layerQueue.removeLast(), TotalEstimatedCost)

        init
        {
            enqueue(firstElement)
        }
    }

    private val _layers = HashMap<Int, SingleLayer>()

    private var _frontLayer : SingleLayer

    private var _size = 1

    override fun sizeDescription(currentCosts: Int) = "queue: $_size nodes (at cost=${_frontLayer.TotalEstimatedCost}: ${_frontLayer.Size} nodes)"

    constructor(firstElem: AStarState)
    {
       _frontLayer = SingleLayer(firstElem)
       _layers[_frontLayer.TotalEstimatedCost] = _frontLayer
    }

    override fun size() = _size

    override fun dequeue(): AStarState
    {
        if(_size == 0)
            throw IllegalStateException("Queue is empty")

        if(_frontLayer.Size == 0)
        {
            // front queue is empty - we need to use the next layer
            // since the structure is designed for A*, this is a directed process ... once we throw away the current layer, it is
            // an error to ever get a total cost of this again (total cost must never decrease during search)
            // 1. remove front layer from layers
            _layers.remove(_frontLayer.TotalEstimatedCost)

            // 2. find min of remaining layers. since size was not 0, we expect at least one remaining element
            val minCost = _layers.keys.min()
            _frontLayer = _layers[minCost]!!
        }

        // try to fetch from front layer
        // even, if we empty it just now, leave it there
        // The algorithm might just remove the last element, to directly afterwards enqueue multiple children in the same layer
        --_size
        return _frontLayer.dequeue()
    }

    override fun enqueue(element: AStarState)
    {
        // 1. ensure ever-increasing costs - throw if costs are below _frontLayer
        if(element.TotalEstimatedCosts < _frontLayer.TotalEstimatedCost)
            throw IllegalStateException("A-Star-estimated-costs DECREASED (Search operated at ${_frontLayer.TotalEstimatedCost} and suddenly found a node with ${element.TotalEstimatedCosts}")

        val matchingLayer = _layers[element.TotalEstimatedCosts]
        if(matchingLayer != null)
            matchingLayer.enqueue(element)
        else
            _layers[element.TotalEstimatedCosts] = SingleLayer(element)

        ++_size
    }
}