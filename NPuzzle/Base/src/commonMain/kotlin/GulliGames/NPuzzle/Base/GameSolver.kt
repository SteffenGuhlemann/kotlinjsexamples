package gulligames.npuzzle.base

import gulligames.npuzzle.base.solver.solveNPuzzleIDAStar
import kotlin.math.abs

var TotalEvaluatedStateCnt = 0L

fun estimateRemainingCosts(game : NPuzzleGame) : Short
{
    // use manhattan distance
    var dist = 0

    for(x in 0 until game.Length)
    {
        for(y in 0 until  game.Length)
        {
            val stone = game[x, y]
            if(stone == EMPTY_FIELD)
                continue

            val targetCol = (stone - 1) % game.Length

            // the game is constructed in cartesian manner
            // i.e. y = 0 means the bottom row.
            // => computing the target row is a bit more complicated, as stone 1 is supposed to be not on row 0, but on row length - 1
            val targetRow = game.Length - 1 - ((stone - 1) / game.Length)
            dist += abs(targetCol - x) + abs(targetRow - y)
        }
    }

    return dist.toShort()
}

fun solveNPuzzle(game : NPuzzleGame) : Int
{
    //return solveNPuzzleAStar(game, true)
    return solveNPuzzleIDAStar(game)
}