package gulligames.npuzzle.base

import gullitools.math.RandomProvider
import gullitools.util.Index2D
import kotlin.jvm.JvmInline
import kotlin.jvm.JvmStatic

val EMPTY_FIELD = 0

interface ICoreNPuzzleGame
{
    val Length : Int
    operator fun get(x : Int, y : Int) : Int
    fun set(x : Int, y : Int, value : Int) : ICoreNPuzzleGame
    fun clone() : ICoreNPuzzleGame
}

class NPuzzleGame internal constructor(private var _coreGame: ICoreNPuzzleGame)
{
    internal val CoreGame : ICoreNPuzzleGame
        get() = _coreGame

    val Length : Int
        get() = _coreGame.Length

    private fun set(x : Int, y: Int, value : Int)
    {
        _coreGame = _coreGame.set(x, y, value)
    }

    val IsSolved : Boolean
        get()
        {
            val l = Length
            val maxI = l - 1
            for(x in 0 until l)
            {
                for (y in 0 until l)
                {
                    // remember - row 0 is the bottom row
                    val target = if(x == maxI && y == 0) EMPTY_FIELD else x + 1 + (l - 1 - y) * l
                    //val target = if (x == maxI && y == maxI) EMPTY_FIELD else x + 1 + y * l
                    if(target != _coreGame[x, y])
                        return false
                }
            }

            return true
        }

    operator fun get(stone: Index2D) = get(stone.X, stone.Y)
    internal inline operator fun get(x : Int, y : Int) = _coreGame[x, y]

    fun tryMove(stone: Index2D) : Boolean
    {
        val x = stone.X
        val y = stone.Y
        if(x < 0 || x >= Length || y < 0 || y >= Length)
            throw IllegalArgumentException("Indices out of range")

        fun isEmpty(possiblyOutsideX : Int, possiblyOutsideY : Int) : Boolean
        {
            if(possiblyOutsideX < 0 || possiblyOutsideY < 0 || possiblyOutsideX >= Length || possiblyOutsideY >= Length)
                return false

            return get(possiblyOutsideX, possiblyOutsideY) == EMPTY_FIELD
        }

        fun moveIfPossible(possiblyEmptyX : Int, possiblyEmptyY : Int) : Boolean
        {
            if(isEmpty(possiblyEmptyX, possiblyEmptyY))
            {
                set(possiblyEmptyX, possiblyEmptyY, get(x, y))
                set(stone.X, stone.Y, EMPTY_FIELD)
                return true
            }

            return false
        }

        return moveIfPossible(x - 1, y) || moveIfPossible(x + 1, y) ||
                moveIfPossible(x, y - 1) || moveIfPossible(x, y + 1)
    }

    fun getPossibleMoves() : List<Index2D>
    {
        fun getEmptyPos() : Index2D
        {
            for(x in 0 until Length)
                for(y in 0 until Length)
                    if(get(x, y) == EMPTY_FIELD)
                        return Index2D(x, y)

            throw IllegalStateException("should never happen")
        }

        val emptyPos = getEmptyPos()
        val moves = ArrayList<Index2D>()
        fun addIfPossible(x : Int, y : Int)
        {
            if(x >= 0 && y >= 0 && x < Length && y < Length)
                moves.add(Index2D(x, y))
        }

        addIfPossible(emptyPos.X - 1, emptyPos.Y)
        addIfPossible(emptyPos.X + 1, emptyPos.Y)
        addIfPossible(emptyPos.X, emptyPos.Y - 1)
        addIfPossible(emptyPos.X, emptyPos.Y + 1)

        return moves
    }

    fun randomMove()
    {
        val possibleMoves = getPossibleMoves()
        tryMove(possibleMoves[RandomProvider.next(possibleMoves.size)])
    }

    override fun toString(): String
    {
        val sb = StringBuilder()
        fun printRow()
        {
            sb.append("-")
            for(i in 0 until Length)
                sb.append("----")

            sb.append("\r\n")
        }

        printRow()
        for(drawY in 0 until Length)
        {
            sb.append("|")
            val gameY = Length - 1 - drawY

            for(x in 0 until Length)
            {
                val value = get(x, gameY)
                if(value == EMPTY_FIELD)
                    sb.append("   ")
                else if(value >= 100)
                    sb.append(value)
                else if(value >= 10)
                    sb.append("$value ")
                else
                    sb.append(" $value ")

                sb.append("|")
            }
            sb.append("\r\n")
            printRow()
        }

        if(IsSolved)
            sb.append("[Solved]\r\n")

        return sb.toString()
    }

    fun clone() = NPuzzleGame(_coreGame.clone())

    override fun hashCode() =_coreGame.hashCode()

    override fun equals(other: Any?)= (other is NPuzzleGame) && (other._coreGame == _coreGame)

    companion object
    {
        @JvmStatic
        fun createInSolvedState(length : Int) : NPuzzleGame
        {
            var coreGame = createCoreGame(length)

            val maxI = length - 1
            for(x in 0 until length)
            {
                for (y in 0 until length)
                {
                    val target = if (x == maxI && y == 0) EMPTY_FIELD else x + 1 + (length - 1 - y) * length
                    coreGame = coreGame.set(x, y, target)
                }
            }

            return NPuzzleGame(coreGame)
        }

        @JvmStatic
        fun createWithRandomState(length: Int) : NPuzzleGame
        {
            val game = createInSolvedState(length)

            for(i in 0 until 10000)
                game.randomMove()

            return game
        }
    }
}

private inline fun index(length : Int, x : Int, y : Int) = x + y * length

@JvmInline
private value class Puzzle8(private val _value : ULong = 0uL) : ICoreNPuzzleGame
{
    override inline val Length: Int
        get() = 3

    override fun get(x: Int, y: Int): Int
    {
        val i = index(Length, x, y)
        return ((_value shr (i * 4)) and 15uL).toInt()
    }

    override fun set(x: Int, y: Int, value: Int): ICoreNPuzzleGame
    {
        val i = index(Length, x, y)
        val shift = i * 4
        val pattern = (15uL shl shift).inv()
        val l = value.toULong() shl shift

        return Puzzle8((_value and pattern) or l)
    }

    override fun clone() : ICoreNPuzzleGame = Puzzle8(_value)
}

@JvmInline
private value class Puzzle15(private val _value: ULong = 0uL) : ICoreNPuzzleGame
{
    override inline val Length: Int
        get() = 4

    override fun get(x: Int, y: Int): Int
    {
        val i = index(Length, x, y)
        return ((_value shr (i * 4)) and 15uL).toInt()
    }

    override fun set(x : Int, y : Int, value: Int): ICoreNPuzzleGame
    {
        val i = index(Length, x, y)
        val shift = i * 4
        val pattern = (15uL shl shift).inv()
        val l = value.toULong() shl shift

        return Puzzle15((_value and pattern) or l)
    }

    override fun clone() : ICoreNPuzzleGame = Puzzle15(_value)
}

private fun createCoreGame(length: Int) : ICoreNPuzzleGame
{
    if(length == 3)
        return Puzzle8()

    if(length == 4)
        return Puzzle15()

    throw IllegalStateException()
}