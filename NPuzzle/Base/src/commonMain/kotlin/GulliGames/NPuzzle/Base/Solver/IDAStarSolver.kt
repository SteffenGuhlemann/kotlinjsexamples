package gulligames.npuzzle.base.solver
        
import gulligames.npuzzle.base.ICoreNPuzzleGame
import gulligames.npuzzle.base.NPuzzleGame
import gulligames.npuzzle.base.TotalEvaluatedStateCnt
import gulligames.npuzzle.base.estimateRemainingCosts
import gulligames.io.Console

private class IdAStarState(val Game : NPuzzleGame, val ActualCost : Int, val Parent : ICoreNPuzzleGame?)

internal fun solveNPuzzleIDAStar(gameToSolve : NPuzzleGame) : Int
{
    if(gameToSolve.IsSolved)
        return 0

    var totalExpandedStates = 0

    var nextLimit : Int? = estimateRemainingCosts(gameToSolve).toInt()
    while (nextLimit != null)
    {
        val currLimit = nextLimit
        nextLimit = null
        val queue = ArrayDeque<IdAStarState>()
        queue.addLast(IdAStarState(gameToSolve, 0, null))

        var expandedStatesInLevel = 0
        while (!queue.isEmpty())
        {
            val parent = queue.removeLast()
            ++expandedStatesInLevel
            ++totalExpandedStates
            ++TotalEvaluatedStateCnt

            val nextCost = parent.ActualCost + 1
            for(move in parent.Game.getPossibleMoves())
            {
                val child = parent.Game.clone()
                child.tryMove(move)

                if(parent.Parent != null && parent.Parent == child.CoreGame)
                    continue

                if(child.IsSolved)
                {
                    Console.log("found solution after evaluating $totalExpandedStates states")
                    return nextCost
                }

                val estimatedTotal = nextCost + estimateRemainingCosts(child)
                if(estimatedTotal <= currLimit)
                {
                    queue.addLast(IdAStarState(child, nextCost, parent.Game.CoreGame))
                }
                else
                {
                    // don't enqueue, but this might be our next costs
                    if(nextLimit == null || nextLimit > estimatedTotal)
                        nextLimit = estimatedTotal
                }
            }
        }

        Console.log("no solution until level $currLimit ($expandedStatesInLevel states)")
    }

    throw IllegalStateException("No solution found")
}
