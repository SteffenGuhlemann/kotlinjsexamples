package gulligames.npuzzle.base

import gulligames.io.Console
import gullitools.util.Stopwatch
import gullitools.util.toTimeString
import org.junit.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals

private fun testNPuzzleAStar(length : Int, maxStates : Int = 1000000)
{
    Console.DoLogToConsole = false
    val sw = Stopwatch()
    val solvedGame = NPuzzleGame.createInSolvedState(length)
    val moveCntByGame = HashMap<NPuzzleGame, Int>()
    val unexpandedQueue = ArrayDeque<NPuzzleGame>()
    moveCntByGame[solvedGame] = 0
    unexpandedQueue.addLast(solvedGame)

    while (!unexpandedQueue.isEmpty() && moveCntByGame.size < maxStates)
    {
        val nextGame = unexpandedQueue.removeFirst()
        val nextMoveCnt = moveCntByGame[nextGame]!! + 1
        for(move in nextGame.getPossibleMoves())
        {
            val child = nextGame.clone()
            child.tryMove(move)
            if(moveCntByGame.containsKey(child))
                continue // already expanded faster

            moveCntByGame[child] = nextMoveCnt
            unexpandedQueue.addLast(child)
        }
    }

    println("In $length-Puzzle expanded ${moveCntByGame.size} nodes in ${sw.toTimeString()} - max. moves: ${moveCntByGame.values.max()}")

    // sort by depth
    val statesByMoveCnt = HashMap<Int, ArrayList<NPuzzleGame>>()
    for(gameAndCount in moveCntByGame)
    {
        val statesOfMoveCnt = statesByMoveCnt[gameAndCount.value]
        if(statesOfMoveCnt != null)
            statesOfMoveCnt.add(gameAndCount.key)
        else
            statesByMoveCnt[gameAndCount.value] = arrayListOf(gameAndCount.key)
    }

    val depths = statesByMoveCnt.keys.sorted()
    val sw2 = Stopwatch()
    println("By move count:")
    for(moveCnt in depths)
    {
        val statesOfCnt = statesByMoveCnt[moveCnt]!!
        println("\t$moveCnt: ${statesOfCnt.size}x")
        for(state in statesOfCnt)
            assertEquals(moveCnt, solveNPuzzle(state), "A*-Depth")
    }

    println("conducted ${moveCntByGame.size} A*-searches in ${sw2.toTimeString()} (evaluating $TotalEvaluatedStateCnt) states in total")
}

@Ignore("too slow")
class TestAStar
{
    @Test
    fun test3Puzzle() = testNPuzzleAStar(3)

    @Test
    fun test4Puzzle() = testNPuzzleAStar(4)
}