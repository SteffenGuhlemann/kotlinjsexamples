package gulligames.npuzzle

import gulligames.io.Console
import gulligames.npuzzle.base.NPuzzleGame
import gulligames.npuzzle.base.TotalEvaluatedStateCnt
import gulligames.npuzzle.base.estimateRemainingCosts
import gulligames.npuzzle.base.solveNPuzzle
import gullitools.math.RandomProvider
import gullitools.util.Index2D
import gullitools.util.Stopwatch
import gulligames.util.queryIntInRange
import gullitools.util.toTimeString

fun printGame(game : NPuzzleGame)
{
    println(game)
    val sw = Stopwatch()
    val moveCnt = solveNPuzzle(game)
    println("min to solve: $moveCnt (${sw.toTimeString()}) - quick estimate: ${estimateRemainingCosts(game)}")
}

private fun NPuzzleGame.findPosition(stone : Int) : Index2D
{
    for(x in 0 until Length)
    {
        for (y in 0 until Length)
        {
            val pos = Index2D(x, y)
            if(this[pos] == stone)
                return pos
        }
    }

    throw IllegalArgumentException("Stein $stone existiert nicht im Spiel")
}

private fun runRepeatableWithSeed(length : Int, vararg args : String)
{
    fun runGame(seed : Int)
    {
        RandomProvider.makeRepeatable(seed)
        val game = NPuzzleGame.createWithRandomState(length)

        printGame(game)
        println("was run with seed $seed - in total evaluated $TotalEvaluatedStateCnt states")
    }

    fun err(msg : String? = null)
    {
        if(!msg.isNullOrEmpty())
            println(msg)

        println("Usage: NPuzzle.Console [seed]")
    }

    when(args.size)
    {
        0 -> runGame(0)
        1 -> {
            val seed = args[0].toIntOrNull()
            if(seed == null)
                err("${args[0]} is no int")
            else
                runGame(seed)
        }
        else -> err()
    }
}

private fun playGame(length : Int)
{
    Console.DoLogToConsole = false
    val game = NPuzzleGame.createWithRandomState(length)
    printGame(game)

    while (!game.IsSolved)
    {
        while (true)
        {
            val maxStoneExcl = game.Length * game.Length
            val stone = queryIntInRange("Stein", 1, maxStoneExcl, 0)
            if(stone == null)
                return

            if(game.tryMove(game.findPosition(stone)))
                break

            println("Dieser Zug ist nicht möglich")
        }

        printGame(game)
    }
}

fun main(vararg args : String)
{
    playGame(4)
    //runRepeatableWithSeed(3, *args)
    //runRepeatableWithSeed(4, *args)
}