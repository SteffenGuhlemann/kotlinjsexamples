package gulligames.npuzzle

import gulligames.gui.GameFrameBase
import gulligames.npuzzle.gui.IView
import gulligames.npuzzle.gui.ViewModel
import gullitools.draw2d.IDrawingContext
import gullitools.draw2d.Point
import gullitools.draw2d.Size
import java.awt.BorderLayout
import java.awt.Dimension
import javax.swing.*

private val _helpTxt = "<html><h3>N-Puzzle instructions</h3>"+
                       "Try to order the stones - from left to right and from top to bottom. The empty space must be bottom right.<br/>" +
                       "E.g. the 8-Puzzle (3x3) is solved, when it looks like this:<br/>" +
                       "<table style=\"border: 1px solid black\">" +
                            "<tr><td>1</td><td>2</td><td>3</td></tr>" +
                            "<tr><td>4</td><td>5</td><td>6</td></tr>" +
                            "<tr><td>7</td><td>8</td><td></td></tr>" +
                        "</table>" +
                        "<ul>" +
                            "<li>Reorder the puzzle by clicking on a stone next to the empty space. The clicked stone will shift to the<br/>" +
                                "empty space and leave an empty space, where it was.</li>" +
                            "<li>Try to use as few moves as possible to solve the puzzle.</li>" +
                            "<li>The puzzle exists in different (quadratic) sizes - e.g. 3x3 or 4x4. The number of stones names the puzzle.<br/>" +
                                "(The 3x3-Puzzle has 8 stones and one empty field and thus, is called 8-Puzzle.)</li>" +
                        "</ul>" +
                       "</html>"

class MainFrame : GameFrameBase, IView
{
    private val _viewModel : ViewModel
    private val _statusLabel = JLabel()

    constructor(exitOnClose : Boolean = true, frameTitlePrefix : String = "", frameTitle : String = "N-Puzzle")
            : super(exitOnClose, frameTitlePrefix, frameTitle, AppIcons.Icon, "Game", _helpTxt)
    {
        val new3PuzzleMenu = JMenuItem("New 8-Puzzle (3x3)")
        new3PuzzleMenu.toolTipText = "Creates a new (shuffled) 8-Puzzle (3x3)"
        new3PuzzleMenu.addActionListener { newGameClick(3) }
        GameMenu.add(new3PuzzleMenu)

        val new4PuzzleMenu = JMenuItem("New 15-Puzzle (4x4)")
        new4PuzzleMenu.toolTipText = "Creates a new (shuffled) 15-Puzzle (4x4)"
        new4PuzzleMenu.addActionListener { newGameClick(4) }
        GameMenu.add(new4PuzzleMenu)

        CoreContentPanel.layout = BorderLayout()

        val statusPanel = JPanel()
        CoreContentPanel.add(statusPanel, BorderLayout.SOUTH)
        statusPanel.layout = BoxLayout(statusPanel, BoxLayout.X_AXIS)
        statusPanel.add(Box.createRigidArea(Dimension(20, 0)))
        statusPanel.add(_statusLabel)

        CoreContentPanel.add(GamePanel, BorderLayout.CENTER)
        _viewModel = ViewModel(this)
        finishStartup()
    }

    private fun newGameClick(length : Int) = _viewModel.newGame(length)

    override fun doRepaint(dc: IDrawingContext, drawSize: Size)
    {
        _viewModel?.repaint(dc, drawSize)
    }

    override fun gamePanelClick(pos: Point, currPanelSize: Size) = _viewModel.click(pos)

    override fun showMoveCount(moveCount: Int)
    {
        _statusLabel.text = "$moveCount moves"
    }

    override fun handleGameOver(moveCount: Int)
    {
        JOptionPane.showMessageDialog(
            this,
            "Solved in $moveCount moves!",
            "Game over",
            JOptionPane.INFORMATION_MESSAGE
        )
    }
}