package gulligames.npuzzle.gui

import gulligames.gui.GridShapedGameViewModelBase
import gulligames.npuzzle.base.EMPTY_FIELD
import gulligames.npuzzle.base.NPuzzleGame
import gullitools.draw2d.*
import gullitools.util.Index2D
import kotlin.math.min

class ViewModel(private val _view : IView) : GridShapedGameViewModelBase()
{
    override val BackgroundBrush: Brush
        get() = LightGray.AsBrush
    override val BackgroundPen: Pen?
        get() = null

    override fun getCurrentGridDimensions() = Index2D(_game.Length, _game.Length)

    // reinitialized anyhow
    private var _game : NPuzzleGame = NPuzzleGame.createInSolvedState(4)
    private var _moveCount = 0

    init
    {
        newGame(4)
    }

    fun newGame(length : Int)
    {
        _game = NPuzzleGame.createWithRandomState(length)
        _moveCount = 0

        _view.setTitle("$length-Puzzle")
        updateView()
    }

    override fun drawSingleGridCell(dc: IDrawingContext, cellRect: Rect, cellIndex: Index2D)
    {
        val cell = _game[cellIndex]
        if(cell == EMPTY_FIELD)
            // don't draw anything - this is empty
            return

        val minLen = min(cellRect.Width, cellRect.Height)
        val edgeColor = rgb(70, 90, 95)
        dc.drawRectangle(LightSlateGray.AsBrush, Pen(edgeColor, 3.0), cellRect)

        val fontSize = minLen * 0.7
        val drawPos = Point(cellRect.Left + 0.5 * cellRect.Width, cellRect.Top + 0.05 * cellRect.Height)

        val ft = FormattedText(cell.toString(), Typeface("Arial", Color = DarkGreen), fontSize, TextAlignment.Center)
        dc.drawText(ft, drawPos)
    }

    private fun updateView()
    {
        _view.triggerRedraw()
        _view.showMoveCount(_moveCount)

        // todo: propose move or move count
        if(_game.IsSolved)
            _view.handleGameOver(_moveCount)
    }

    override fun clickOnCell(cellIndex: Index2D)
    {
        if(_game.tryMove(cellIndex))
        {
            ++_moveCount
            updateView()
        }
    }
}