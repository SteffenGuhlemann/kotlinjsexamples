package gulligames.npuzzle.gui

interface IView
{
    fun setTitle(title : String)
    fun triggerRedraw()
    fun showMoveCount(moveCount : Int)
    fun handleGameOver(moveCount: Int)
}