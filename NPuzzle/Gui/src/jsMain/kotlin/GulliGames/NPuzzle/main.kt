package gulligames.npuzzle

import gulligames.npuzzle.gui.IView
import gulligames.npuzzle.gui.ViewModel
import gullitools.draw2d.IDrawingContext
import gullitools.draw2d.Point
import gullitools.draw2d.Size
import gullitools.html.HtmlCanvasDrawingContext
import gullitools.html.getElement
import kotlinx.browser.document
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLElement

private class PageHandler : IView
{
    private val _mainDiv : HTMLDivElement = getElement("mainDiv")
    private val _canvas : HTMLCanvasElement = getElement("mainCanvas")
    private val _new8PuzzleGameBtn : HTMLButtonElement = getElement("New8PuzzleGameBtn")
    private val _new15PuzzleGameBtn : HTMLButtonElement = getElement("New15PuzzleGameBtn")
    private val _drawingContext : IDrawingContext
    private val _viewModel : ViewModel
    private val _titleH1 : HTMLElement = getElement("TitleH1")
    private val _stateDiv : HTMLDivElement = getElement("StateDiv")
    private val _gameWrapDiv : HTMLDivElement = getElement("GameWrapDiv")

    private fun resize()
    {
        val len = _gameWrapDiv.clientWidth
        _canvas.width = len
        _canvas.height = len
    }

    init
    {
        _gameWrapDiv.onresize = { _ -> resize()}
        resize()

        _drawingContext = HtmlCanvasDrawingContext(_canvas)
        _viewModel = ViewModel(this)
        _canvas.onclick = {_viewModel.click(Point(it.offsetX, it.offsetY))}
        _new8PuzzleGameBtn.onclick = {newGame(3)}
        _new15PuzzleGameBtn.onclick = {newGame(4)}

        triggerRedraw()
    }

    private fun newGame(length : Int) = _viewModel.newGame(length)

    override fun setTitle(title: String)
    {
        document.title = title
        _titleH1.innerText = title
    }

    override fun triggerRedraw()
    {
        // Intellij warns, that '?.' is unnecessary, but that's wrong. The method is called during initialization
        // and will cause an exception otherwise
        _viewModel?.repaint(_drawingContext, Size(_canvas.width.toDouble(), _canvas.height.toDouble()))
    }

    override fun showMoveCount(moveCount: Int)
    {
        _stateDiv.innerHTML = "$moveCount moves"
    }

    override fun handleGameOver(moveCount: Int)
    {
        kotlinx.browser.window.alert("Solved in $moveCount moves!")
    }
}

fun main()
{
    val handler = PageHandler()
}