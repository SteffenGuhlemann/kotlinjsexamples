package gulligames.util

internal actual fun readLine() = kotlin.io.readLine()