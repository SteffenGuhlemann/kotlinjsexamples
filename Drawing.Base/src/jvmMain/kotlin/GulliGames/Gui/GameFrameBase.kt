package gulligames.gui

import gullitools.draw2d.IDrawingContext
import gullitools.draw2d.Point
import gullitools.draw2d.Size
import gullitools.controls.DrawCanvas
import gullitools.io.ResourceManager
import java.awt.BorderLayout
import java.awt.Dimension
import javax.swing.*

abstract class GameFrameBase : JFrame
{
    private val _gameMenu : JMenu?

    protected val GameMenu : JMenu
        get() = _gameMenu ?: throw IllegalStateException("No game menu defined")

    protected val CoreContentPanel : JPanel
    protected val GamePanel : DrawCanvas

    private val _frameTitlePrefix : String

    override fun setTitle(newTitle: String)
    {
        // apply a hack here.
        // reason: If using this as an easter egg, we want always something in the title to be visible: "Recipe Exporter easter egg: N-Puzzle"
        // problem: some Games change the title - e.g. to say "8-Puzzle" instead of "N-Puzzle" or "Othello against computer player (black) - whites turn", ...
        //          => this would remove the initial Title including the important prefix
        // => split the title into
        //    a) an always present constant prefix (most of the time "", in the easter egg case "Recipe Exporter easter egg: ")
        //    b) the volatile title, which the game view model might decide to change

        // changing the title usually uses the IView{fun setTitle(newTitle : String)} which normally doesn't need to be implemented,
        // as it is present implicitly in JFrame (kotlin's
        //  frame.title = "blub"
        //  is the same under the hood as
        // frame.setTitle("blub")
        // => override the setTitle-Method and under the hood always set the new title together with the fixed prefix
        super.setTitle(_frameTitlePrefix + newTitle)
    }

    protected constructor(exitOnClose : Boolean, frameTitlePrefix : String, frameTitle : String, iconPath : String, gameMenuTitle : String?, htmlHelpText : String?)
    {
        if(exitOnClose)
            defaultCloseOperation = EXIT_ON_CLOSE

        _frameTitlePrefix = frameTitlePrefix

        title = frameTitle
        iconImage = ResourceManager.loadImageIcon(iconPath).image

        val myContentPane = contentPane
        myContentPane.layout = BorderLayout()

        val menuPane = JMenuBar()
        myContentPane.add(menuPane, BorderLayout.NORTH)

        if(gameMenuTitle != null)
        {
            _gameMenu = JMenu(gameMenuTitle)
            menuPane.add(_gameMenu)
        }
        else
        {
            _gameMenu = null
        }

        if(htmlHelpText != null)
        {
            val helpMenu = JMenu("Help")
            menuPane.add(helpMenu)
            val helpMenuItem = JMenuItem("Help")
            helpMenu.add(helpMenuItem)

            helpMenuItem.addActionListener { _ -> JOptionPane.showMessageDialog(this, htmlHelpText, frameTitle, JOptionPane.INFORMATION_MESSAGE) }
        }

        CoreContentPanel = JPanel()
        myContentPane.add(CoreContentPanel, BorderLayout.CENTER)

        GamePanel = DrawCanvas(::doRepaint, ::gamePanelClick)
        GamePanel.preferredSize= Dimension(500, 500)
    }

    protected fun finishStartup()
    {
        pack()
        isVisible = true
        triggerRedraw()
    }

    open fun triggerRedraw() = GamePanel.repaint()

    protected abstract fun doRepaint(dc: IDrawingContext, drawSize : Size)
    protected open fun gamePanelClick(pos: Point, currPanelSize : Size)
    {}
}