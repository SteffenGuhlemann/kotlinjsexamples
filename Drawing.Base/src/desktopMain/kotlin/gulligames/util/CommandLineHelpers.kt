package gulligames.util

internal expect fun readLine() : String?

fun queryIntInRange(label: String, minIncl : Int, maxExcl : Int, exitNo: Int? = null): Int?
{
    if(minIncl >= maxExcl)
        throw IllegalArgumentException("min ($minIncl) >= max ($maxExcl)")

    if(exitNo != null)
    {
        if(exitNo >= minIncl && exitNo < maxExcl)
            throw IllegalArgumentException("exitNo ($exitNo) must be no valid number, but is within the range of allowed values ($minIncl .. maxExcl)")
    }

    print("Bitte $label eingeben ($minIncl .. ${maxExcl -1 })")
    if(exitNo != null)
        print(" ($exitNo: Abbruch)")

    print(": ")

    while (true)
    {
        val line = readLine()
        try
        {
            if(line == null)
                throw IllegalStateException()

            val userChoice = line.toInt()
            if(userChoice >= minIncl && userChoice < maxExcl)
                return userChoice

            if(userChoice == exitNo)
                return null
        }
        catch (_ : NumberFormatException)
        {}
    }
}

fun queryPositiveInt(label: String, maxExcl : Int, exitNo: Int? = null) = queryIntInRange(label, 0, maxExcl, exitNo)

fun getDigitCount(i: Int): Int
{
    var charCount = 1
    var w = i

    if(i < 0)
    {
        w = -w
        ++charCount
    }

    while (w > 9)
    {
        w /= 10
        ++charCount
    }

    return charCount
}