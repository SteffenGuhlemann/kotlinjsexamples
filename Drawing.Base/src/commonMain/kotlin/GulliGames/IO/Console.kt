package gulligames.io

object Console
{
    var DoLogToConsole = true

    fun log(msg : String)
    {
        if(DoLogToConsole)
            println(msg)
    }
}