package gulligames.gui

import gullitools.draw2d.*
import gullitools.util.Index2D
import kotlin.math.floor
import kotlin.math.min

private fun getGridIndex1d(mousePos : Double, canvasLen : Double, cellCount : Int) : Int?
{
    val cellLen = canvasLen / cellCount

    // compute something like: "clicked on index 1.5" (where 1.5 means the mid of the stone with index 1 (the second stone))
    val roundIndex = mousePos / cellLen

    if(roundIndex < 0 || roundIndex >= cellCount)
        return null;

    return floor(roundIndex).toInt()
}

abstract class GridShapedGameViewModelBase()
{
    protected abstract val BackgroundBrush : Brush?
    protected abstract val BackgroundPen : Pen?
    protected abstract fun getCurrentGridDimensions() : Index2D

    private var _lastDrawSize : Size? = null

    protected abstract fun drawSingleGridCell(dc : IDrawingContext, cellRect : Rect, cellIndex : Index2D)

    fun repaint(dc : IDrawingContext, drawSize : Size)
    {
        _lastDrawSize = drawSize

        // clear background
        dc.drawRectangle(BackgroundBrush, BackgroundPen, Rect(0.0, 0.0, drawSize.Width, drawSize.Height))

        // draw actual stones
        val dimensions = getCurrentGridDimensions()
        val cellSize = Size(drawSize.Width / dimensions.X, drawSize.Height / dimensions.Y);
        val bound = min(cellSize.Width, cellSize.Height) * 0.05;
        val innerFieldSize = Size(cellSize.Width - 2.0 * bound, cellSize.Height - 2.0 * bound)

        for(x in 0 until dimensions.X)
        {
            val left = x * cellSize.Width + bound
            for(logicY in 0 until dimensions.Y)
            {
                val drawY = dimensions.Y - 1 - logicY
                val top = drawY * cellSize.Height + bound
                drawSingleGridCell(dc, Rect(left, top, innerFieldSize.Width, innerFieldSize.Height), Index2D(x, logicY))
            }
        }
    }

    protected fun tryGetGridIndex(mousePos : Point) : Index2D?
    {
        val lastDrawSize = _lastDrawSize
        if(lastDrawSize == null)
            // not drawn yet
            return null

        val dimensions = getCurrentGridDimensions()
        val gridIndexX = getGridIndex1d(mousePos.X, lastDrawSize.Width, dimensions.X)
        val drawIndexY = getGridIndex1d(mousePos.Y, lastDrawSize.Height, dimensions.Y)

        if(gridIndexX == null || drawIndexY == null)
            return null

        return Index2D(gridIndexX, dimensions.Y - 1 - drawIndexY)
    }

    protected open fun clickOnCell(cellIndex : Index2D)
    {}

    fun click(mousePos: Point)
    {
        val gridIndex = tryGetGridIndex(mousePos)
        if(gridIndex != null)
            clickOnCell(gridIndex)
    }
}