package gulligames.othello.base

import gullitools.util.Index2D

interface IHeuristic
{
    val Name : String
    fun getHeuristicEvalBlack(game : Game) : Double
}

object SimpleHeuristic : IHeuristic
{
    override val Name: String
        get() = "Simple"

    override fun getHeuristicEvalBlack(game: Game): Double
    {
        // for now just count stones - could/should be improved later
        val whiteStones = game.WhiteStoneCount
        val blackStones = game.BlackStoneCount
        val totalStones = whiteStones + blackStones

        return 1.0 * blackStones / totalStones
    }
}

object EndWeightingHeuristics : IHeuristic
{
    override val Name: String
        get() = "End-Weighting"

    override fun getHeuristicEvalBlack(game: Game): Double
    {
        val whiteStones = game.WhiteStoneCount
        val blackStones = game.BlackStoneCount

        if(blackStones == whiteStones)
            return 0.5

        if(whiteStones > blackStones)
        {
            val heuristicWhite = getEndWeightingHeuristic(game, whiteStones, blackStones)
            // heuristic is meant to be for black
            return 1.0 - heuristicWhite
        }

        return getEndWeightingHeuristic(game, blackStones, whiteStones)
    }
}

private inline fun getEndWeightingHeuristic(game: Game, betterStones : Int, worseStones : Int) : Double
{
    val absAdvantage = betterStones - worseStones
    val remainingMoves = game.Width * game.Height - betterStones - worseStones
    return 0.5 + 0.5 * absAdvantage / (remainingMoves + absAdvantage)
}

class WeightingCornerHeuristic(val Factor : Double): IHeuristic
{
    override val Name: String
        get() = "Weighting Corner($Factor)"

    override fun getHeuristicEvalBlack(game: Game): Double
    {
        var whiteStones = game.WhiteStoneCount.toDouble()
        var blackStones = game.BlackStoneCount.toDouble()

        fun count(color : PlayerColor)
        {
            if(color == PlayerColor.White)
                whiteStones += Factor
            else if(color == PlayerColor.Black)
                blackStones += Factor
        }

        // count edges
        // a) horizontal
        for(x in 0 until game.Width)
        {
            count(game[Index2D(x, 0)])
            count(game[Index2D(x, game.Height - 1)])
        }

        // b) vertical
        // (corners will automatically be counted twice
        for(y in 0 until game.Height)
        {
            count(game[Index2D(0, y)])
            count(game[Index2D(game.Width - 1, y)])
        }

        return 1.0 * blackStones / (blackStones + whiteStones)
    }
}

class EndWeightingCornerHeuristic(val Factor : Double): IHeuristic
{
    override val Name: String
        get() = "End weighting corner($Factor)"

    override fun getHeuristicEvalBlack(game: Game): Double
    {
        var whiteStones : Double = game.WhiteStoneCount.toDouble()
        var blackStones : Double = game.BlackStoneCount.toDouble()

        val totalStonesSet = whiteStones + blackStones
        val fieldCount = game.Width * game.Height
        val freeStones = fieldCount - totalStonesSet
        val weightEdges = freeStones / fieldCount * Factor

        fun count(color : PlayerColor)
        {
            if(color == PlayerColor.White)
                whiteStones += weightEdges
            else if(color == PlayerColor.Black)
                blackStones += weightEdges
        }

        // count edges
        // a) horizontal
        for(x in 0 until game.Width)
        {
            count(game[Index2D(x, 0)])
            count(game[Index2D(x, game.Height - 1)])
        }

        // b) vertical
        // (corners will automatically be counted twice)
        for(y in 0 until game.Height)
        {
            count(game[Index2D(0, y)])
            count(game[Index2D(game.Width-1, y)])
        }

        return 1.0 * blackStones / (blackStones + whiteStones)
    }
}