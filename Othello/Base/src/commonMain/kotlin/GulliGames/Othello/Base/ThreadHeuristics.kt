package gulligames.othello.base

import gullitools.util.Index2D
import kotlin.math.abs

private abstract class GameAxis(protected val Game : Game)
{
    abstract val Length : Int
    abstract fun getIndex2D(index : Int) : Index2D
    fun getStone(index: Int) : PlayerColor
    {
        val index2d = getIndex2D(index)
        return Game[index2d]
    }
}

private class RowAxis(private val _row : Int, game : Game) : GameAxis(game)
{
    override val Length: Int
        get() = Game.Width

    override fun getIndex2D(index: Int) = Index2D(index, _row)
}

private fun Game.allRows() : Sequence<GameAxis>
{
    return (0 until Height).asSequence().map { RowAxis(it, this) }
}

private fun Game.allColumns() : Sequence<GameAxis>
{
    return (0 until Width).asSequence().map { ColumnAxis(it, this) }
}

private fun Game.allSEDiagonals() : Sequence<GameAxis>
{
    val diagCnt = Width + Width - 1
    return (0 until diagCnt).asSequence().map { SEDiagonalAxis(it, this) }
}

private fun Game.allNEDiagonals() : Sequence<GameAxis>
{
    val diagCnt = Width + Width - 1
    return (0 until diagCnt).asSequence().map { NEDiagonalAxis(it, this) }
}

private fun Game.allAxes() = allRows() + allColumns() + allNEDiagonals() + allSEDiagonals()

private class ColumnAxis(private val _col : Int, game : Game) : GameAxis(game)
{
    override val Length: Int
        get() = Game.Height

    override fun getIndex2D(index: Int) = Index2D(_col, index)
}

private class NEDiagonalAxis(private val _diagNo : Int, game : Game) : GameAxis(game)
{
    override val Length : Int

    private val _firstStoneX : Int
    private val _firstStoneY : Int

    init
    {
        val distFromMaxAxis = abs(game.Height - _diagNo - 1)
        Length = game.Height - distFromMaxAxis

        if(_diagNo + 1 <= game.Height)
        {
            _firstStoneX = 0
            _firstStoneY = _diagNo
        }
        else
        {
            _firstStoneY = game.Height - 1
            _firstStoneX = _diagNo + 1 - game.Height
        }
    }

    override fun getIndex2D(index: Int) = Index2D(_firstStoneX + index, _firstStoneY - index)
}

private class SEDiagonalAxis(private val _diagNo: Int, game : Game) : GameAxis(game)
{
    override val Length : Int

    private val _firstStoneX : Int
    private val _firstStoneY : Int

    override fun getIndex2D(index: Int) = Index2D(_firstStoneX + index, _firstStoneY + index)

    init
    {
        val distFromMaxAxis = abs(game.Height - _diagNo - 1)
        Length = game.Height - distFromMaxAxis

        if(_diagNo + 1 <= game.Height)
        {
            _firstStoneX = 0
            _firstStoneY = game.Height - _diagNo - 1
        }
        else
        {
            _firstStoneY = 0
            _firstStoneX = _diagNo + 1 - game.Height
        }
    }
}

data class ColorStats(val SafeStones : Int, val OneClickThreadedStones : Int, val TwoClickThreadedStones : Int)
{
    operator fun plus(rhs : ColorStats) = ColorStats(SafeStones + rhs.SafeStones,
        OneClickThreadedStones + rhs.OneClickThreadedStones,
        TwoClickThreadedStones + rhs.TwoClickThreadedStones);

    val Score : Int
        get() = SafeStones * 100 + TwoClickThreadedStones * 5 + OneClickThreadedStones
}

data class AxisEval(val WhiteStats : ColorStats, val BlackStats : ColorStats)
{
    operator fun plus(rhs : AxisEval) = AxisEval(WhiteStats + rhs.WhiteStats, BlackStats + rhs.BlackStats)

    val EvalBlack : Double
        get()
        {
            val scoreBlack = BlackStats.Score
            val scoreWhite = WhiteStats.Score
            val sumScore = scoreBlack + scoreWhite

            if(sumScore == 0)
                return 0.5

            return 1.0 * scoreBlack / sumScore
        }
}

private enum class ScanState
{
    Space,
    SpaceBlack,
    SpaceWhite,
    EdgeBlack,
    EdgeWhite,
    BlackWhite,
    WhiteBlack
}

private enum class ColoredScanState
{
    SpaceBefore,
    EdgeBefore,
    OtherBefore
}

private fun ScanState.translateToColoredState() = when(this)
{
    ScanState.Space -> null
    ScanState.SpaceBlack -> ColoredScanState.SpaceBefore
    ScanState.SpaceWhite -> ColoredScanState.SpaceBefore
    ScanState.EdgeBlack -> ColoredScanState.EdgeBefore
    ScanState.EdgeWhite -> ColoredScanState.EdgeBefore
    ScanState.BlackWhite -> ColoredScanState.OtherBefore
    ScanState.WhiteBlack -> ColoredScanState.OtherBefore
}

private class CountableColorStats(var SafeStones : Int = 0, var OneClickThreadedStones : Int = 0, var TwoClickThreadedStones : Int = 0)
private fun CountableColorStats.toFixedStats() = ColorStats(SafeStones, OneClickThreadedStones, TwoClickThreadedStones)

private fun PlayerColor.getFirstStoneState() = when(this)
{
    PlayerColor.White -> ScanState.EdgeWhite
    PlayerColor.Black -> ScanState.EdgeBlack
    PlayerColor.Empty -> ScanState.Space
}

private fun getState(color : PlayerColor, currState : ScanState) = when(color)
{
    PlayerColor.Empty -> ScanState.Space
    PlayerColor.Black -> when(currState)
    {
        ScanState.Space -> ScanState.SpaceBlack
        ScanState.SpaceBlack -> ScanState.SpaceBlack
        ScanState.SpaceWhite -> ScanState.WhiteBlack
        ScanState.EdgeBlack -> ScanState.EdgeBlack
        ScanState.EdgeWhite -> ScanState.WhiteBlack
        ScanState.BlackWhite -> ScanState.WhiteBlack
        ScanState.WhiteBlack -> ScanState.WhiteBlack
    }
    PlayerColor.White -> when(currState)
    {
        ScanState.Space -> ScanState.SpaceWhite
        ScanState.SpaceBlack -> ScanState.BlackWhite
        ScanState.SpaceWhite -> ScanState.SpaceWhite
        ScanState.EdgeBlack -> ScanState.BlackWhite
        ScanState.EdgeWhite -> ScanState.EdgeWhite
        ScanState.BlackWhite -> ScanState.BlackWhite
        ScanState.WhiteBlack -> ScanState.BlackWhite
    }
}

private fun GameAxis.getForwardStates() : List<ColoredScanState?>
{
    val forwardStates = ArrayList<ColoredScanState?>()
    var currState = getStone(0).getFirstStoneState()
    forwardStates.add(currState.translateToColoredState())

    for(i in 1 until Length)
    {
        currState = getState(getStone(i), currState)
        forwardStates.add(currState.translateToColoredState())
    }

    return forwardStates
}

private enum class AxisStoneEval
{
    Safe,
    OneClickThread,
    TwoClicksThread
}

private fun getStoneEval(forwardState: ColoredScanState, backwardState : ColoredScanState) = when(forwardState)
{
    ColoredScanState.EdgeBefore -> AxisStoneEval.Safe
    ColoredScanState.OtherBefore -> when(backwardState)
    {
        ColoredScanState.EdgeBefore -> AxisStoneEval.Safe
        ColoredScanState.OtherBefore -> AxisStoneEval.Safe
        ColoredScanState.SpaceBefore -> AxisStoneEval.OneClickThread
    }
    ColoredScanState.SpaceBefore -> when(backwardState)
    {
        ColoredScanState.SpaceBefore -> AxisStoneEval.TwoClicksThread
        ColoredScanState.EdgeBefore -> AxisStoneEval.Safe
        ColoredScanState.OtherBefore -> AxisStoneEval.OneClickThread
    }
}

private fun GameAxis.getStats() : AxisEval
{
    if(Length < 3)
    {
        // each stone counts full
        var blackCnt = 0
        var whiteCnt = 0
        for(i in 0 until Length)
        {
            when(getStone(i))
            {
                PlayerColor.Black -> ++blackCnt
                PlayerColor.Empty -> {}
                PlayerColor.White -> ++whiteCnt
            }
        }

        return AxisEval(ColorStats(whiteCnt, 0, 0), ColorStats(blackCnt, 0, 0))
    }

    val forwardStates = getForwardStates()
    val whiteCounts = CountableColorStats()
    val blackCounts = CountableColorStats()

    fun addStatistics(color : PlayerColor, forwardState : ColoredScanState?, backwardState : ColoredScanState?)
    {
        fun addStats(stats : CountableColorStats, fState : ColoredScanState, bState : ColoredScanState)
        {
            when(getStoneEval(fState, bState))
            {
                AxisStoneEval.Safe -> stats.SafeStones++
                AxisStoneEval.OneClickThread -> stats.OneClickThreadedStones++
                AxisStoneEval.TwoClicksThread -> stats.TwoClickThreadedStones++
            }
        }

        when(color)
        {
            PlayerColor.Empty -> {}
            PlayerColor.White -> addStats(whiteCounts, forwardState!!, backwardState!!)
            PlayerColor.Black -> addStats(blackCounts, forwardState!!, backwardState!!)
        }
    }

    var currColor = getStone(Length - 1)
    var currBackwardState = currColor.getFirstStoneState()
    addStatistics(currColor, forwardStates[Length - 1], currBackwardState.translateToColoredState())

    for(i in Length - 2 downTo 0)
    {
        currColor = getStone(i)
        currBackwardState = getState(currColor, currBackwardState)
        addStatistics(currColor, forwardStates[i], currBackwardState.translateToColoredState())
    }

    return AxisEval(whiteCounts.toFixedStats(), blackCounts.toFixedStats())
}

private fun GameAxis.updateMultiplicativeStats(evalArr: Array2D)
{
    if(Length < 3)
    {
        // consider stones safe
        // => no change to eval Arr
        return
    }

    val forwardStates = getForwardStates()

    fun addStatistics(i : Int, backwardState : ColoredScanState?)
    {
        if(backwardState == null)
            return

        val forwardState = forwardStates[i]!!
        val factor = when(getStoneEval(forwardState, backwardState))
        {
            AxisStoneEval.Safe -> 1.0
            AxisStoneEval.TwoClicksThread -> 0.3
            AxisStoneEval.OneClickThread -> 0.1
        }

        val index2d = getIndex2D(i)
        evalArr[index2d.X, index2d.Y] *= factor
    }

    var currBackwardState = getStone(Length - 1).getFirstStoneState()
    addStatistics(Length - 1, currBackwardState.translateToColoredState())

    for(i in Length - 2 downTo 0)
    {
        currBackwardState = getState(getStone(i), currBackwardState)
        addStatistics(i, currBackwardState.translateToColoredState())
    }
}

object ThreadHeuristics2 : IHeuristic
{
    override val Name: String
        get() = "Thread2 Count"

    override fun getHeuristicEvalBlack(game: Game): Double
    {
        var sum = AxisEval(ColorStats(0,0,0), ColorStats(0,0,0))
        for(axis in game.allAxes())
            sum = sum + axis.getStats()

        return sum.EvalBlack
    }
}

object ThreadMulHeuristics : IHeuristic
{
    override val Name: String
        get() = "Thread Mul"

    override fun getHeuristicEvalBlack(game: Game): Double
    {
        val evalArr = Array2D(game.Width, game.Height, 1.0)
        for(axis in game.allAxes())
            axis.updateMultiplicativeStats(evalArr)

        var blackSum = 0.0
        var whiteSum = 0.0
        for(x in 0 until game.Width)
        {
            for(y in 0 until game.Height)
            {
                when(game[Index2D(x, y)])
                {
                    PlayerColor.White -> whiteSum += evalArr[x, y]
                    PlayerColor.Black -> blackSum += evalArr[x, y]
                    PlayerColor.Empty -> {}
                }
            }
        }

        val evalSum = blackSum + whiteSum
        if(evalSum == 0.0)
            return 0.5

        return blackSum / evalSum

    }
}

class Array2D(val Width : Int, val Height : Int, initialVal : Double)
{
    private val _arr = Array(Width * Height) { initialVal }

    private fun index(x : Int, y: Int) = x * Height + y
    operator fun get(x : Int, y : Int) = _arr[index(x, y)]
    operator fun set(x: Int, y: Int, value : Double)
    {
        _arr[index(x, y)] = value
    }
}