package gulligames.othello.base

import gullitools.util.Index2D

private const val _sideLength = 8

private inline fun getBitIndex(x: Int, y : Int) : Int
{
    if(x < 0 || x >= _sideLength || y < 0 || y >= _sideLength)
        throw IllegalArgumentException("index out of range")

    return x + y * _sideLength
}

private inline fun getBit(bitIndex : Int, bitArr : Long) : Boolean
{
    // Kotlin seems to have a strange bug with shift operators:
    // * both for int and long shifts
    // * only for negative values and (u)shr
    // * there are 2 versions: (for example imaging shifts on bytes - which are currently not allowed in kotlin):
    //  a) positive number p = 00101100
    //  b) negative number n1 = 10110100 or n2=-1 = 11111111
    // ** shr shifts in a 1 in case of a negative number and 0 in case of a positive number:
    // *** p shr 2 = 00001011
    // *** n1 shr 2 = 11101101, n2 shr 2 = 11111111 (=-1)
    // ** ushr shifts in always a 0:
    // **** p ushr 2 = 00001011
    // **** n1 ushr 2 = 00101101, n2 ushr 2 = 00111111 (=64)
    // * the operators can be called in 2 ways:
    // a) x shr 2
    // b) x.shr(2)
    // => currently the first works always. The second is buggy:
    // (both for Int and Long, both for shr and ushr, only for negative numbers (leading 1 bit)):
    // * -1 shr 2 = -1 as expected, -1 ushr 2 = 64 (as expected)
    // * but -1.shr(2) = 0, -1.ushr(2) = 0!!!!
    // (this notation works for positive numbers: 3.shr(1) = 1 as expected.
    // => use the infix notation
    return ((bitArr ushr bitIndex) and 1) == 1L
}

private inline fun setBit(bitIndex : Int, bitArr : Long) : Long
{
    val mask = 1L shl bitIndex
    return bitArr or mask
}

private inline fun unSetBit(bitIndex : Int, bitArr : Long) : Long
{
    val mask = (1L shl bitIndex).inv()
    return bitArr and mask
}

private val ConstantPosition: (Int) -> (Int?) = {it}
private val DecrementPosition: (Int) -> (Int?) = {if (it>0) it-1 else null}
private val IncrementPosition : (Int) -> (Int?) = {if (it< _sideLength -1) it+1 else null}

private fun createGameState(stoneCountWhite : Int, stoneCountBlack : Int, playersTurn : PlayerColor) : Int
{
    val playerCode = when(playersTurn)
    {
        PlayerColor.Empty -> 0
        PlayerColor.White -> 1
        PlayerColor.Black -> 2
    }

    return (((playerCode shl 7) or stoneCountWhite) shl 7) or stoneCountBlack
}

class Game
{
    private fun setGameState(stoneCountWhite : Int, stoneCountBlack : Int, playersTurn : PlayerColor)
    {
        _gameState = createGameState(stoneCountWhite, stoneCountBlack, playersTurn)
    }

    constructor()
        :this(createGameState(0, 0, PlayerColor.White), 0, 0)
    {
    }

    private constructor(gameState : Int, whiteStones : Long, blackStones : Long)
    {
        _gameState = gameState
        _whiteStones = whiteStones
        _blackStones = blackStones
    }

    private var _gameState : Int
    private var _whiteStones : Long
    private var _blackStones : Long

    fun clone() : Game
    {
        return Game(_gameState, _whiteStones, _blackStones)
    }

    val BlackStoneCount
        get() = _gameState and 127

    val WhiteStoneCount
        get() = (_gameState ushr 7) and 127

    val Winner : PlayerColor?
        get()
        {
            return when
            {
                !IsGameOver -> null
                WhiteStoneCount > BlackStoneCount -> PlayerColor.White
                BlackStoneCount > WhiteStoneCount -> PlayerColor.Black
                else -> PlayerColor.Empty
            }
        }

    private fun getPlayersTurn() : PlayerColor
    {
        val playerCode = _gameState ushr 14
        return when(playerCode)
        {
            0 -> PlayerColor.Empty
            1 -> PlayerColor.White
            2 -> PlayerColor.Black
            else -> throw  IllegalStateException()
        }
    }

    val WhitesTurn : Boolean
        get() = getPlayersTurn() == PlayerColor.White

    val IsGameOver : Boolean
        get() = getPlayersTurn() == PlayerColor.Empty

    val Width : Int
        get() = _sideLength

    val Height : Int
        get() = _sideLength

    private inline fun getStone(bitIndex : Int) : PlayerColor
    {
        val isWhite = getBit(bitIndex, _whiteStones)
        val isBlack = getBit(bitIndex, _blackStones)
        if(isWhite)
        {
            if(isBlack)
                throw IllegalStateException("Both black and white can not be at the same position");

            return PlayerColor.White
        }

        return if(isBlack)
            PlayerColor.Black
        else
            PlayerColor.Empty
    }

    operator fun get(stone: Index2D) : PlayerColor = getStone(getBitIndex(stone.X, stone.Y))

    private fun doSetStone(stone: Index2D, forWhite : Boolean)
    {
        val bitIndex = getBitIndex(stone.X, stone.Y)

        if(forWhite)
        {
            _whiteStones = setBit(bitIndex, _whiteStones)
            _blackStones = unSetBit(bitIndex, _blackStones)
        }
        else
        {
            _blackStones = setBit(bitIndex, _blackStones)
            _whiteStones = unSetBit(bitIndex, _whiteStones)
        }
    }

    private fun setStoneAndConductTurns(stone : Index2D, forWhite : Boolean) : Int
    {
        if(!canSet(stone, forWhite))
            throw IllegalArgumentException("Can not set this position")

        doSetStone(stone, forWhite)

        // turn stones
        // if we are in the start situation, the 4 center fields (or some of them) were still empty.
        // so if this set is for one of the initial fields (and we are still here - i.e. the set is possible)
        val x = stone.X
        val y = stone.Y
        if(x >= 3 && x <= 4 && y >= 3 && y <= 4)
            return 0

        // now turn in every direction, where it is possible
        val eval = TurnEvaluator(x, y, forWhite)
        var turned = 0
        if(x >= 2)
        {
            // a) to the left
            turned += eval.turnIfPossible(DecrementPosition, ConstantPosition)

            // b) diagonally left upwards
            if(y >= 2)
                turned += eval.turnIfPossible(DecrementPosition, DecrementPosition)

            // c) diagonally left downwards
            if(y<= _sideLength -3)
                turned += eval.turnIfPossible(DecrementPosition, IncrementPosition)
        }

        // for constant x
        // d) upwards
        if(y >= 2)
            turned += eval.turnIfPossible(ConstantPosition, DecrementPosition)

        // e) downwards
        if(y <= _sideLength -3)
            turned += eval.turnIfPossible(ConstantPosition, IncrementPosition)

        // rightwards
        if(x <= _sideLength -3)
        {
            // a) to the right
            turned += eval.turnIfPossible(IncrementPosition, ConstantPosition)

            // b) diagonally right upwards
            if(y >= 2)
                turned += eval.turnIfPossible(IncrementPosition, DecrementPosition)

            // c) diagonally right downwards
            if(y<= _sideLength -3)
                turned += eval.turnIfPossible(IncrementPosition, IncrementPosition)
        }

        return turned
    }

    private fun canSetAnywhere(forWhite : Boolean) : Boolean
    {
        for(x in 0 until _sideLength)
            for(y in 0 until _sideLength)
                if(canSet(Index2D(x, y), forWhite))
                    return true

        return false
    }

    fun set(stone : Index2D)
    {
        when(getPlayersTurn())
        {
            PlayerColor.Empty -> return
            PlayerColor.White ->
            {
                val turned = setStoneAndConductTurns(stone, true)
                setGameState(WhiteStoneCount + 1 + turned, BlackStoneCount - turned, PlayerColor.Black)
                if(!canSetAnywhere(false))
                {
                    // it's blacks turn, but black can not move
                    // can white? if not, its game over
                    val nextPlayer = if (canSetAnywhere(true)) PlayerColor.White else PlayerColor.Empty
                    setGameState(WhiteStoneCount, BlackStoneCount, nextPlayer)
                }
            }
            PlayerColor.Black ->
            {
                val turned = setStoneAndConductTurns(stone, false)
                setGameState(WhiteStoneCount - turned, BlackStoneCount + 1 + turned, PlayerColor.White)
                if(!canSetAnywhere(true))
                {
                    // it's White's turn, but white can not move
                    // can black? if not, its game over
                    val nextPlayer = if (canSetAnywhere(false)) PlayerColor.Black else PlayerColor.Empty
                    setGameState(WhiteStoneCount, BlackStoneCount, nextPlayer)
                }
            }
        }
    }

    private inner class TurnEvaluator(private val _x: Int, private  val _y: Int, private val _forWhite: Boolean)
    {
        private val _player : PlayerColor
        private val _otherPlayer : PlayerColor

        init
        {
            if(_forWhite)
            {
                _player = PlayerColor.White
                _otherPlayer = PlayerColor.Black
            }
            else
            {
                _player = PlayerColor.Black
                _otherPlayer = PlayerColor.White
            }
        }

        fun turnIfPossible(incX: (Int) -> (Int?), incY : (Int) -> (Int?)) : Int
        {
            return if(canTurn(incX, incY))
                doTurn(incX, incY)
            else
                0
        }

        private fun doTurn(incX: (Int) -> (Int?), incY : (Int) -> (Int?)) : Int
        {
            var nextX = incX(_x)
            var nextY = incY(_y)

            var turnCnt = 0
            while (true)
            {
                if(nextX == null || nextY == null)
                    throw IllegalStateException()

                val stone = get(Index2D(nextX, nextY))
                if(stone == _player)
                    // we are finished
                    return turnCnt

                if(stone == PlayerColor.Empty)
                    throw IllegalStateException()

                doSetStone(Index2D(nextX, nextY), _forWhite)
                ++turnCnt

                nextX = incX(nextX)
                nextY = incY(nextY)
            }
        }

        fun canTurn(incX: (Int) -> (Int?), incY : (Int) -> (Int?)) : Boolean
        {
            var nextX = incX(_x)
            var nextY = incY(_y)

            if(nextX == null || nextY == null)
                return false

            var currX: Int = nextX
            var currY: Int = nextY
            if(get(Index2D(currX, currY)) != _otherPlayer)
                return false

            while (true)
            {
                nextX = incX(currX)
                nextY = incY(currY)

                if(nextX == null || nextY == null)
                    return false

                currX = nextX
                currY = nextY

                val stone = get(Index2D(currX, currY))
                if(stone == _player)
                // this is the surrounding stone we have been waiting for
                    return true

                if(stone == PlayerColor.Empty)
                // the run of the opposite color ended in nothing
                    return false

                // we still have a stone of the opposite color
                // => try the next stone
            }
        }
    }

    private fun canSet(stone: Index2D, forWhite: Boolean) : Boolean
    {
        if(get(stone) != PlayerColor.Empty)
            return false

        // if we are in the start situation, the 4 center fields (or some of them) are still empty.
        // so if this query is for one of the initial fields (and we are still here - i.e. the field is empty)
        // it is possible
        val x = stone.X
        val y = stone.Y
        if(WhiteStoneCount + BlackStoneCount < 4)
            return x >= 3 && x <= 4 && y >= 3 && y <= 4

        // determine if we can turn something
        val eval = TurnEvaluator(x, y, forWhite)

        if(x >= 2)
        {
            // a) to the left
            if(eval.canTurn(DecrementPosition, ConstantPosition))
                return true

            // b) diagonally left upwards
            if(y >= 2 && eval.canTurn(DecrementPosition, DecrementPosition))
                return true

            // c) diagonally left downwards
            if(y <= _sideLength -3 && eval.canTurn(DecrementPosition, IncrementPosition))
                return true
        }

        // for constant x
        // d) upwards
        if(y >= 2 && eval.canTurn(ConstantPosition, DecrementPosition))
            return true

        // e) downwards
        if(y <= _sideLength -3 && eval.canTurn(ConstantPosition, IncrementPosition))
            return true

        // rightwards
        if(x <= _sideLength -3)
        {
            // a) to the right
            if(eval.canTurn(IncrementPosition, ConstantPosition))
                return true

            // b) diagonally right upwards
            if(y >= 2 && eval.canTurn(IncrementPosition, DecrementPosition))
                return true

            // c) diagonally right downwards
            if(y<= _sideLength -3 && eval.canTurn(IncrementPosition, IncrementPosition))
                return true
        }

        return false
    }

    fun canSet(stone: Index2D) : Boolean
    {
        return when(getPlayersTurn())
        {
            PlayerColor.White -> canSet(stone, true)
            PlayerColor.Black -> canSet(stone, false)
            PlayerColor.Empty -> false
        }
    }

    override fun equals(other: Any?): Boolean
    {
        if(other !is Game)
            return false

        return other._blackStones == _blackStones && other._whiteStones==_whiteStones && other._gameState==_gameState
    }

    override fun hashCode(): Int
    {
        return (((_blackStones * 3) xor _whiteStones)*3).hashCode() xor _gameState
    }
}