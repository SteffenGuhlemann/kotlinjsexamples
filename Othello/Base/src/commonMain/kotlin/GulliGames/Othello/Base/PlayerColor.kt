package gulligames.othello.base

enum class PlayerColor
{
    White,
    Black,
    Empty
}