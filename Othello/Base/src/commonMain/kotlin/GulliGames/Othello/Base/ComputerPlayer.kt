package gulligames.othello.base

import gulligames.io.Console
import gullitools.math.RandomProvider
import gullitools.util.Index2D
import gullitools.util.Stopwatch
import kotlin.math.max
import kotlin.math.min
import kotlin.random.Random

private fun Game.getPossibleMoves() : List<Index2D>
{
    val possibleMoves = ArrayList<Index2D>()
    for(x in 0 until Width)
    {
        for(y in 0 until Height)
        {
            val move = Index2D(x, y)
            if(canSet(move))
                possibleMoves.add(move)
        }
    }

    return possibleMoves
}

private fun Game.cloneAndMove(move: Index2D) : Game
{
    val clonedGame = this.clone()
    clonedGame.set(move)
    return clonedGame
}

private val _defaultHeuristics : IHeuristic = EndWeightingCornerHeuristic(10.0)

class SimpleComputerPlayer(val MaxDepth : Int = 5, heuristics : IHeuristic = _defaultHeuristics) : ComputerPlayerBase(heuristics)
{
    override val SearchStrategyName: String
        get() = "FixedDepth($MaxDepth)"

    override fun createEvaluator(game: Game): IEvaluator
    {
        return EvalRun(game, MaxDepth, Heuristics)
    }

    private class EvalRun(override val Game : Game, private val _maxDepth : Int, private val _heuristics : IHeuristic) : IEvaluator
    {
        private val _knownEvalsBlack = HashMap<Game, Double>()
        private var _cacheHit = 0

        override fun getStatistics() = "Cache size: ${_knownEvalsBlack.size}, cache hits: $_cacheHit"

        private fun tryGetFinalizingEvalBlack(gameState: Game, depth: Int) : Double?
        {
            if(gameState.IsGameOver)
            {
                val whiteStoneCnt = gameState.WhiteStoneCount
                val blackStoneCnt = gameState.BlackStoneCount

                if(whiteStoneCnt > blackStoneCnt)
                    return 0.0

                if(blackStoneCnt > whiteStoneCnt)
                    return 1.0

                return 0.5
            }

            val knownEval = _knownEvalsBlack[gameState]
            if(knownEval != null)
            {
                ++_cacheHit
                return knownEval
            }

            if(depth >= _maxDepth)
            {
                // do eval heuristically
                val score = _heuristics.getHeuristicEvalBlack(gameState)
                _knownEvalsBlack[gameState] = score

                return score
            }

            return null
        }

        private fun evaluateBlackWinChance(gameState: Game, depth: Int, parentLowerBound : Double = 0.0, parentUpperBound : Double = 1.0): Double
        {
            val possibleEval = tryGetFinalizingEvalBlack(gameState, depth)
            if (possibleEval != null)
                return possibleEval

            // evaluate all moves
            val possibleMoves = gameState.getPossibleMoves()
            if (possibleMoves.size == 0)
                throw IllegalStateException("should never happen")

            val nextDepth = depth + 1

            var currentLowerBound = parentLowerBound
            var currentUpperBound = parentUpperBound

            fun canAlphaBetaPrune(currBestScore : Double) : Boolean
            {
                if(gameState.WhitesTurn)
                {
                    // white tries to minimize score
                    // the result will be <= currBestScore
                    currentUpperBound = min(currentUpperBound, currBestScore)

                    // however, black already can reach at least lowerBound
                    // => will never choose this, if this is smaller than lower bound
                    return parentLowerBound > currBestScore
                }
                else
                {
                    // black tries to maximize
                    // => result will be >= currBestScore
                    currentLowerBound = max(currentLowerBound, currBestScore)

                    // worst case for white is currently upper bound
                    // => will never choose us, if currBestScore > upperBound
                    return  parentUpperBound < currBestScore
                }
            }

            var bestScore = evaluateBlackWinChance(gameState.cloneAndMove(possibleMoves[0]), nextDepth, currentLowerBound, currentUpperBound)
            if(canAlphaBetaPrune(bestScore))
                return bestScore

            for (i in 1 until possibleMoves.size)
            {
                val score = evaluateBlackWinChance(gameState.cloneAndMove(possibleMoves[i]), nextDepth, currentLowerBound, currentUpperBound)

                if (gameState.WhitesTurn)
                {
                    if (score < bestScore)
                    {
                        bestScore = score
                        if(canAlphaBetaPrune(bestScore))
                            return bestScore
                    }
                }
                else
                {
                    if (score > bestScore)
                    {
                        bestScore = score
                        if(canAlphaBetaPrune(bestScore))
                            return bestScore
                    }
                }
            }

            _knownEvalsBlack[gameState] = bestScore
            return bestScore
        }

        override fun evaluateBlackWinChance(gameState: Game) = evaluateBlackWinChance(gameState, 0)
    }
}

class ConstantTimeComputerPlayer(val EvalStateCnt : Long, heuristics : IHeuristic = _defaultHeuristics) : ComputerPlayerBase(heuristics)
{
    override val SearchStrategyName: String
        get() = "ConstTime($EvalStateCnt)"

    override fun createEvaluator(game: Game): IEvaluator
    {
        return EvalRun(game, EvalStateCnt, Heuristics)
    }

    private class EvalRun(override val Game : Game, private val _maxStates : Long, private val _heuristics : IHeuristic) : IEvaluator
    {
        private val _knownEvalsBlack = HashMap<Game, Double>()
        private var _cacheHit = 0

        override fun getStatistics() = "Cache size: ${_knownEvalsBlack.size}, cache hits: $_cacheHit"

        private fun tryGetFinalizingEvalBlack(gameState : Game, stateBudget : Budget) : Double?
        {
            if(gameState.IsGameOver)
            {
                val whiteStoneCnt = gameState.WhiteStoneCount
                val blackStoneCnt = gameState.BlackStoneCount

                if(whiteStoneCnt > blackStoneCnt)
                    return 0.0

                if(blackStoneCnt > whiteStoneCnt)
                    return 1.0

                return 0.5
            }

            val knownEval = _knownEvalsBlack[gameState]
            if(knownEval != null)
            {
                ++_cacheHit
                return knownEval
            }

            if(stateBudget.isEmpty)
            {
                stateBudget.evalState()

                // do eval heuristically
                val score = _heuristics.getHeuristicEvalBlack(gameState)
                _knownEvalsBlack[gameState] = score

                return score
            }

            return null
        }

        private class Budget
        {
            private val _totalBudget : Long
            private var _evaluatedStates : Long = 0
            private val _parent : Budget?

            constructor(totalBudget : Long, parent : Budget? = null)
            {
                _totalBudget = totalBudget
                _parent = parent
            }

            val isEmpty : Boolean
                get() = _totalBudget <= _evaluatedStates

            fun evalState()
            {
                ++_evaluatedStates

                if(_parent != null)
                    _parent.evalState()
            }

            fun getPartBudget(parts : Int) : Budget
            {
                val remaining = _totalBudget - _evaluatedStates
                val perPart = remaining / parts
                return Budget(perPart, this)
            }
        }

        private fun evaluateBlackWinChance(gameState: Game, stateBudget: Budget, parentLowerBound : Double = 0.0, parentUpperBound : Double = 1.0): Double
        {
            val possibleEval = tryGetFinalizingEvalBlack(gameState, stateBudget)
            if (possibleEval != null)
                return possibleEval

            // evaluate all moves
            val possibleMoves = gameState.getPossibleMoves()
            if (possibleMoves.size == 0)
                throw IllegalStateException("should never happen")

            var currentLowerBound = parentLowerBound
            var currentUpperBound = parentUpperBound

            fun canAlphaBetaPrune(currBestScore : Double) : Boolean
            {
                if(gameState.WhitesTurn)
                {
                    // white tries to minimize score
                    // the result will be <= currBestScore
                    currentUpperBound = min(currentUpperBound, currBestScore)

                    // however, black already can reach at least lowerBound
                    // => will never choose this, if this is smaller than lower bound
                    return parentLowerBound > currBestScore
                }
                else
                {
                    // black tries to maximize
                    // => result will be >= currBestScore
                    currentLowerBound = max(currentLowerBound, currBestScore)

                    // worst case for white is currently upper bound
                    // => will never choose us, if currBestScore > upperBound
                    return  parentUpperBound < currBestScore
                }
            }

            var bestScore = evaluateBlackWinChance(gameState.cloneAndMove(possibleMoves[0]), stateBudget.getPartBudget(possibleMoves.size), currentLowerBound, currentUpperBound)

            if(canAlphaBetaPrune(bestScore))
                return bestScore

            for (i in 1 until possibleMoves.size)
            {
                val score = evaluateBlackWinChance(gameState.cloneAndMove(possibleMoves[i]), stateBudget.getPartBudget(possibleMoves.size - i), currentLowerBound, currentUpperBound)

                if (gameState.WhitesTurn)
                {
                    if (score < bestScore)
                    {
                        bestScore = score
                        if(canAlphaBetaPrune(bestScore))
                            return bestScore
                    }
                }
                else
                {
                    if (score > bestScore)
                    {
                        bestScore = score
                        if(canAlphaBetaPrune(bestScore))
                            return bestScore
                    }
                }
            }

            _knownEvalsBlack[gameState] = bestScore
            return bestScore
        }

        override fun evaluateBlackWinChance(gameState: Game) = evaluateBlackWinChance(gameState, Budget(_maxStates))
    }
}

private fun IEvaluator.evaluateMoveWinChance(move: Index2D) : Double
{
    val stateAfterMove = Game.cloneAndMove(move)

    if(Game.WhitesTurn)
    {
        val winChanceBlack = evaluateBlackWinChance(stateAfterMove)
        return 1.0 - winChanceBlack
    }

    return evaluateBlackWinChance(stateAfterMove)
}

internal interface IEvaluator
{
    val Game : Game
    fun evaluateBlackWinChance(gameState : Game) : Double
    fun getStatistics() : String
}

abstract class ComputerPlayerBase(val Heuristics: IHeuristic) : IPlayer
{
    override val PlayerCategoryName: String
        get() = "Computer opponent"

    internal abstract fun createEvaluator(game : Game) : IEvaluator

    protected abstract val SearchStrategyName : String

    override val StrategyName: String
        get() = "$SearchStrategyName (heuristics: ${Heuristics.Name})"

    override fun proposeMove(game: Game): ScoredMove
    {
        val possibleMoves = game.getPossibleMoves()
        if(possibleMoves.size == 0)
            throw IllegalStateException("No move possible")

        val bestMoves = arrayListOf(possibleMoves[0])
        val eval = createEvaluator(game)
        var bestScore = eval.evaluateMoveWinChance(possibleMoves[0])

        for(i in 1 until possibleMoves.size)
        {
            val move = possibleMoves[i]
            val score = eval.evaluateMoveWinChance(move)
            if(score > bestScore)
            {
                bestScore = score
                bestMoves.clear()
                bestMoves.add(move)
            }
            else if(score == bestScore)
            {
                bestMoves.add(move)
            }
        }

        val bestMove = bestMoves[RandomProvider.next(bestMoves.size)]
        val randomChoiceStr = if(bestMoves.size == 1)
            "(1 single best move)"
        else
            " (random choice out of ${bestMoves.size} equal moves)"

        val statistics = "${possibleMoves.size} possible moves$randomChoiceStr (evaluator: ${eval.getStatistics()})"

        return ScoredMove(bestMove, bestScore, {statistics})
    }

    override fun toString() = StrategyName
}

fun IPlayer.getVerbose() : IPlayer = VerbosePlayer(TimeCollectingComputerPlayer((this)))

private class VerbosePlayer(private val _origPlayer : TimeCollectingComputerPlayer) : IPlayer
{
    override val PlayerCategoryName: String
        get() = _origPlayer.PlayerCategoryName

    override val StrategyName: String
        get() = _origPlayer.StrategyName

    override fun proposeMove(game: Game): ScoredMove
    {
        val sw = Stopwatch()

        val move = _origPlayer.proposeMove(game)

        Console.log(move.getStatistics())
        Console.log("\tcomputer moves to [${move.Move.X}, ${move.Move.Y}], expected score: ${move.Score}, time: ${sw.ElapsedSeconds}s, total: ${_origPlayer.TotalElapsedSeconds}s")

        return move
    }
}

fun IPlayer.collectTime() = TimeCollectingComputerPlayer(this)

class TimeCollectingComputerPlayer(private val _origPlayer: IPlayer) : IPlayer
{
    override val PlayerCategoryName: String
        get() = _origPlayer.PlayerCategoryName

    override val StrategyName: String
        get() = _origPlayer.StrategyName

    private var _totalElapsedMilliseconds = 0L

    val TotalElapsedSeconds : Double
        get() = 1e-3 * _totalElapsedMilliseconds

    override fun proposeMove(game: Game): ScoredMove
    {
        val sw = Stopwatch()
        val move = _origPlayer.proposeMove(game)
        _totalElapsedMilliseconds += sw.ElapsedMilliseconds

        return move
    }
}

object RandomPlayer : IPlayer
{
    private val _rnd = Random(Random.nextInt())

    override val PlayerCategoryName: String
        get() = "Computer opponent"

    override val StrategyName: String
        get() = "Random"

    override fun toString() = StrategyName

    override fun proposeMove(game: Game): ScoredMove
    {
        val possibleMoves = game.getPossibleMoves()
        val move = possibleMoves[_rnd.nextInt(possibleMoves.size)]
        return ScoredMove(move, 0.5) { "" }
    }
}