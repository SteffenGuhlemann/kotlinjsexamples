package gulligames.othello.base

import gullitools.util.Index2D

data class ScoredMove(val Move : Index2D, val Score : Double, val getStatistics : () -> String)
interface IPlayer
{
    val PlayerCategoryName : String
    val StrategyName : String
    fun proposeMove(game : Game) : ScoredMove
}
