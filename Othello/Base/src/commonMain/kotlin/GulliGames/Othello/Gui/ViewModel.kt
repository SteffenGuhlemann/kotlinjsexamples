package gulligames.othello.gui

import gulligames.gui.GridShapedGameViewModelBase
import gullitools.draw2d.*
import gulligames.othello.base.*
import gullitools.util.Index2D
import kotlin.math.min
import kotlin.random.Random
import gullitools.charts.*

private fun getWinnerText(winner : PlayerColor) : String
{
    return when(winner)
    {
        PlayerColor.Black -> "Black wins!"
        PlayerColor.White -> "White wins!"
        PlayerColor.Empty -> "Draw"
    }
}

private fun getPlayerBrush(isWhite : Boolean) : Brush
{
    return SolidColorBrush(if(isWhite) White else Black)
}

enum class Opponent
{
    LocalHuman,
    ComputerPlayer,
    RemoteWhite,
    RemoteBlack
}

private data class OpponentAgent(val Color : PlayerColor, val Agent : IPlayer)

class ViewModel(private val _view : IView, opponent: Opponent) : GridShapedGameViewModelBase()
{
    override val BackgroundBrush: Brush
        get() = LightGray.AsBrush

    override val BackgroundPen: Pen?
        get() = null

    override fun getCurrentGridDimensions() = Index2D(_game.Width, _game.Height)

    private var _game : Game = Game()
    private var _remoteOrComputerOpponentAgent : OpponentAgent? = null

    private fun handleRemoteOrComputerMove() : Boolean
    {
        if(_game.IsGameOver)
            return false

        fun doComputerMove(agent : IPlayer)
        {
            _game.set(agent.proposeMove(_game).Move)
            handleRemoteOrComputerMove()
        }

        val opponentAgent = _remoteOrComputerOpponentAgent
        if(opponentAgent == null)
            return false

        when (opponentAgent.Color)
        {
            PlayerColor.White ->
            {
                if(_game.WhitesTurn)
                {
                    doComputerMove(opponentAgent.Agent)
                    return true
                }

                return false
            }
            PlayerColor.Black ->
            {
                if(!_game.WhitesTurn)
                {
                    doComputerMove(opponentAgent.Agent)
                    return true
                }
            }
            PlayerColor.Empty -> throw IllegalStateException("should never happen")
        }

        return false
    }

    init
    {
        newGame(opponent)
    }

    fun newGame(opponent: Opponent)
    {
        _game = Game()
        _remoteOrComputerOpponentAgent = when(opponent)
        {
            Opponent.LocalHuman -> null
            Opponent.ComputerPlayer -> OpponentAgent(
                if(Random.nextBoolean()) PlayerColor.Black else PlayerColor.White,
                SimpleComputerPlayer().getVerbose())
            Opponent.RemoteWhite, Opponent.RemoteBlack -> TODO()
        }

        fun getTitleForOpponent(opponentAgent: OpponentAgent?) : String
        {
            if(opponentAgent == null)
                return "Local Game"

            val colorTxt = when(opponentAgent.Color)
            {
                PlayerColor.Black -> "black"
                PlayerColor.White -> "white"
                PlayerColor.Empty -> throw IllegalStateException("should never happen")
            }
            return "${opponentAgent.Agent.PlayerCategoryName} ($colorTxt)"
        }

        _view.setTitle("Othello - ${getTitleForOpponent(_remoteOrComputerOpponentAgent)}")

        handleRemoteOrComputerMove()
        updateView()
    }

    private val StateText : String
        get()
        {
            val winner = _game.Winner;
            if(winner != null)
                return "Game over - ${getWinnerText(winner)}"

            val farbeAmZug = if(_game.WhitesTurn) "White" else "Black"
            return "$farbeAmZug's turn"
        }

    private fun updateView()
    {
        _view.triggerRedraw()
        _view.setStateText(StateText)
    }

    override fun drawSingleGridCell(dc: IDrawingContext, cellRect: Rect, cellIndex: Index2D)
    {
        dc.drawRectangle(DarkGray.AsBrush, null, cellRect)

        val currentStone = _game[cellIndex]
        val centerStone = Point(cellRect.Left + 0.5 * cellRect.Width, cellRect.Top + 0.5 * cellRect.Height)
        val stoneRadius = min(cellRect.Width, cellRect.Height) * 0.5 * 0.5;

        val shape = when
        {
            currentStone != PlayerColor.Empty -> createShape(DrawShape.DiamondFilled, getPlayerBrush(currentStone == PlayerColor.White))
            _game.canSet(cellIndex) -> createShape(DrawShape.DiamondOutline, getPlayerBrush(_game.WhitesTurn))
            else -> null
        }

        shape?.draw(dc, centerStone, 2.0 * stoneRadius)
    }

    override fun clickOnCell(cellIndex: Index2D)
    {
        if(_game.canSet(cellIndex))
            _game.set(cellIndex);

        updateView()

        if(handleRemoteOrComputerMove())
            updateView()

        val winner = _game.Winner
        if(winner != null)
            _view.handleGameOver(winner, getWinnerText(winner))
    }
}
