package gulligames.othello.gui

import gulligames.othello.base.PlayerColor

interface IView
{
    fun setTitle(titleTxt : String)
    fun setStateText(stateTxt : String)
    fun handleGameOver(winner: PlayerColor, winnerText : String)
    fun triggerRedraw()
}