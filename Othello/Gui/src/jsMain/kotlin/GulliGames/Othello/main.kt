package gulligames.othello

import gullitools.draw2d.IDrawingContext
import gullitools.draw2d.*
import gullitools.html.HtmlCanvasDrawingContext
import gullitools.html.getElement
import gulligames.othello.base.PlayerColor
import gulligames.othello.gui.*
import kotlinx.browser.document
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLElement

private class PageHandler : IView
{
    private val _stateDiv : HTMLDivElement = getElement("StateDiv")
    private val _canvas : HTMLCanvasElement = getElement("mainCanvas")
    private val _newComputerGameBtn : HTMLButtonElement = getElement("NewComputerGameBtn")
    private val _newLocalGameBtn : HTMLButtonElement = getElement("NewLocalGameBtn")
    private val _drawingContext : IDrawingContext
    private val _viewModel : ViewModel
    private val _gameWrapDiv : HTMLDivElement = getElement("GameWrapDiv")
    private val _titleH1 : HTMLElement = getElement("TitleH1")

    private fun showOpponent(opponent: Opponent)
    {
        when(opponent)
        {
            Opponent.LocalHuman -> {
                _newLocalGameBtn.style.background = "lightgreen"
                _newComputerGameBtn.style.background = ""
            }
            Opponent.ComputerPlayer ->{
                _newComputerGameBtn.style.background = "lightgreen"
                _newLocalGameBtn.style.background = ""
            }
            Opponent.RemoteBlack, Opponent.RemoteWhite -> TODO()
        }
    }

    override fun setTitle(titleTxt: String)
    {
        document.title = titleTxt
        _titleH1.innerText = titleTxt
    }

    private fun newGame(opponent: Opponent)
    {
        showOpponent(opponent)
        _viewModel.newGame(opponent)
    }

    constructor(opponent: Opponent)
    {
        _gameWrapDiv.onresize = { _ -> resize()}
        resize()

        _drawingContext = HtmlCanvasDrawingContext(_canvas)
        _viewModel = ViewModel(this, opponent)
        showOpponent(opponent)

        _canvas.onclick = {e -> _viewModel.click(Point(e.offsetX, e.offsetY))}
        _newComputerGameBtn.onclick = {_ -> newGame(Opponent.ComputerPlayer)}
        _newLocalGameBtn.onclick = {_ -> newGame(Opponent.LocalHuman)}
        triggerRedraw()
    }

    private fun resize()
    {
        val len = _gameWrapDiv.clientWidth
        _canvas.width = len
        _canvas.height = len
    }

    override fun triggerRedraw()
    {
        // Intellij warns, that '?.' is unnecessary, but that's wrong. The method is called during initialization
        // and will cause an exception otherwise
        _viewModel?.repaint(_drawingContext, Size(_canvas.width.toDouble(), _canvas.height.toDouble()))
    }

    override fun setStateText(stateTxt: String)
    {
        _stateDiv.innerHTML = stateTxt;
    }

    override fun handleGameOver(winner: PlayerColor, winnerText: String)
    {
        kotlinx.browser.window.alert("Game over - $winnerText");
    }
}

fun main()
{
    val handler = PageHandler(Opponent.ComputerPlayer)
}
