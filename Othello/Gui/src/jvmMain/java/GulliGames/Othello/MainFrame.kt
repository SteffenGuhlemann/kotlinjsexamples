package gulligames.othello

import gulligames.gui.GameFrameBase
import gullitools.draw2d.IDrawingContext
import gullitools.draw2d.*
import gulligames.othello.base.PlayerColor
import gulligames.othello.gui.*
import java.awt.BorderLayout
import javax.swing.*

private val _helpTxt = "<html><h3>Othello (Reversi) instructions</h3>" +
        "Black and White move alternating. (It is shown, whose turn it is and which moves are possible.)" +
        "<ul>" +
            "<li>Initially the 4 middle fields need to be filled.</li>" +
            "<li>Afterwards," +
                "<ul>" +
                    "<li>the new stone must be placed in a way, that between the new stone and another stone of the same color (horizontally, diagonally or vertically)<br/>" +
                    "stones of the opposite color are enclosed without gaps.</li>" +
                    "<li>The enclosed stones (possibly in multiple lines at once) switch color from the opposite to the current players color.</li>" +
                "</ul>" +
            "</li>" +
            "<li>In case no more more moves are possible," +
                "<ul>" +
                    "<li>for the player on turn, the player is skipped and the other player can move again.</li>" +
                    "<li>for any player, game ends. The player wins who has the majority of stones colored in his color.</li>" +
                "</ul>" +
            "</li>" +
        "</ul></html>"

class MainFrame : GameFrameBase, IView
{
    private val _viewModel : ViewModel
    private val _statusLabel = JLabel()

    constructor(exitOnClose : Boolean = true, frameTitlePrefix : String = " ", frameTitle : String = "Othello")
        : super(exitOnClose, frameTitlePrefix, frameTitle, AppIcons.Icon, "Game", _helpTxt)
    {
        val newComputerGameItem = JMenuItem("New Game (Computer opponent)")
        GameMenu.add(newComputerGameItem)
        newComputerGameItem.addActionListener{_ -> newComputerGameClick()}

        val newLocalGameItem = JMenuItem("New Game (locale human opponent)")
        GameMenu.add(newLocalGameItem)
        newLocalGameItem.addActionListener{_ -> newLocalGameClick()}

        val statusPanel = JPanel()
        CoreContentPanel.layout = BorderLayout()
        CoreContentPanel.add(statusPanel, BorderLayout.SOUTH);
        statusPanel.layout = BoxLayout(statusPanel, BoxLayout.X_AXIS)
        statusPanel.add(JLabel("  "))
        statusPanel.add(_statusLabel)

        CoreContentPanel.add(GamePanel, BorderLayout.CENTER)

        _viewModel = ViewModel(this, Opponent.ComputerPlayer)

        finishStartup()
    }

    private fun newComputerGameClick() = _viewModel.newGame(Opponent.ComputerPlayer)
    private fun newLocalGameClick() = _viewModel.newGame(Opponent.LocalHuman)

    override fun handleGameOver(winner: PlayerColor, winnerText : String)
    {
        JOptionPane.showMessageDialog(this, "Game over - $winnerText", "Game over", JOptionPane.INFORMATION_MESSAGE);
    }

    override fun doRepaint(dc: IDrawingContext, drawSize : Size)
    {
        _viewModel?.repaint(dc, drawSize)
    }

    override fun gamePanelClick(pos: Point, currPanelSize : Size) = _viewModel.click(pos)

    override fun setStateText(stateTxt: String)
    {
        _statusLabel.text = stateTxt
    }
}