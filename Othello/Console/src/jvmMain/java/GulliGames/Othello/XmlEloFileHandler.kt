package gulligames.othello

import gulligames.elo.EloFileEntry
import gulligames.elo.GameStatistics
import gulligames.elo.IEloFileHandler
import gullitools.io.saveAsFile
import gullitools.xml.*

private val ENTRIES_NODE = "Entries"
private val ENTRY_NODE = "E"
private val NAME_PROP = "n"
private val GAME_CNT_PROP = "c"
private val ELO_PROP = "e"
private val THINK_TIME_PROP = "t"

private val _xFac = JvmXmlFactory()

class XmlEloFileHandler : IEloFileHandler
{
    override fun load(fileName: String): List<EloFileEntry>
    {
        val xDoc = _xFac.loadDocument(fileName)
        val root = xDoc.getRootNode(ENTRIES_NODE)
        val children = root.findAllElementsAndTestTheirCommonName(ENTRY_NODE)

        fun parseEntry(entryNode : XmlElement) : EloFileEntry
        {
            val name = entryNode.getStringAttribute(NAME_PROP)
            val gameCount = entryNode.getIntAttribute(GAME_CNT_PROP)
            val elo = entryNode.getIntAttribute(ELO_PROP)
            val time = entryNode.getLongAttribute(THINK_TIME_PROP)
            return EloFileEntry(name, GameStatistics(gameCount, elo, time))
        }

        return children.map (::parseEntry)
    }

    override fun save(entries: List<EloFileEntry>, fileName: String)
    {
        val xDoc = _xFac.createDocument()
        val root = xDoc.createAndSetRoot(ENTRIES_NODE)
        for(entry in entries)
        {
            val entryNode = root.createAndAppendChild(ENTRY_NODE)
            entryNode.appendAttribute(NAME_PROP, entry.PlayerName)
            entryNode.appendAttribute(GAME_CNT_PROP, entry.Statistics.GameCount)
            entryNode.appendAttribute(ELO_PROP, entry.Statistics.Elo)
            entryNode.appendAttribute(THINK_TIME_PROP, entry.Statistics.TotalMsThinkTime)
        }

        xDoc.saveAsFile(fileName)
    }
}