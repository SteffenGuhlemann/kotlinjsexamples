package gulligames.othello

import gulligames.elo.FileEloStore

fun main(args: Array<String>)
{
    eloMain(FileEloStore("elo.xml", XmlEloFileHandler(), AvailablePlayers), args)
    //parallelConsoleMain(args)
    //runConsoleMain(args)
}
