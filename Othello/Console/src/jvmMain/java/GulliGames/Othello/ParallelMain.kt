package gulligames.othello

import gulligames.othello.base.*
import kotlinx.datetime.*
import kotlin.math.min
import kotlin.math.roundToInt
import gullitools.util.Stopwatch

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel

private fun getOptionalParallelDirectedComputerVsComputerSimulation(turns : Int, white : IPlayer, black : IPlayer, parallel: Boolean) : DirectedRunResult
{
    return if(parallel)
        getParallelDirectedComputerVsComputerSimulation(turns, white, black)
    else
        getDirectedComputerVsComputerSimulation(turns, white, black)
}

private fun getParallelDirectedComputerVsComputerSimulation(turns : Int, white : IPlayer, black : IPlayer) : DirectedRunResult
{
    val playerWhite = white.collectTime()
    val playerBlack = black.collectTime()

    var whiteWinCnt = 0
    var blackWinCnt = 0
    var drawCnt = 0

    val uniqueResults = HashSet<Game>()

    val sw = Stopwatch()
    var lastElapsedMs = sw.ElapsedMilliseconds
    var lastTimeStepMs = 10000.0

    fun printStats(run : Int)
    {
        val elapsedMs = sw.ElapsedMilliseconds
        if(elapsedMs - lastElapsedMs < lastTimeStepMs )
            return

        val avgTimePerRun = sw.ElapsedMilliseconds / run
        val expectedRemainingTimeMs  = (turns - run) * avgTimePerRun
        if(expectedRemainingTimeMs < 0.3 * lastTimeStepMs)
            return // short before finishing this directed run

        lastElapsedMs = elapsedMs

        // was this the preLast or last log msg
        if(expectedRemainingTimeMs < 2.0 * lastTimeStepMs && expectedRemainingTimeMs > 1.7 * lastTimeStepMs)
        {
            // yes - just break in the middle
            lastTimeStepMs = expectedRemainingTimeMs * 0.5
        }
        else if(expectedRemainingTimeMs > 2.0 * lastTimeStepMs)
        {
            // compute expected total time to adjust timeStep
            val expectedLogMessages = (avgTimePerRun * turns) / lastTimeStepMs
            val maxLogMessages = 10
            if (expectedLogMessages > maxLogMessages)
            {
                val maxFactor = min(2.0, expectedRemainingTimeMs / lastTimeStepMs / 2.0)

                val adjustmentFactor = min(maxFactor, 1.0 * expectedLogMessages / maxLogMessages)
                lastTimeStepMs *= adjustmentFactor
            }
        }

        val total = whiteWinCnt + blackWinCnt + drawCnt

        fun getPercentStr(cnt : Int) : String
        {
            val percent = (100.0 * cnt / total).roundToInt()
            return "$percent %"
        }

        fun TimeCollectingComputerPlayer.getTimeStr() = getTimeString(TotalElapsedSeconds, run)

        val now = Clock.System.now()
        val approxEnd = now.plus(expectedRemainingTimeMs.toLong(), DateTimeUnit.MILLISECOND, TimeZone.currentSystemDefault())

        fun dateTimeStr(dateTime : Instant) : String
        {
            val localTime = dateTime.toLocalDateTime(TimeZone.currentSystemDefault())

            fun digit2Str(number : Int) : String
            {
                if(number < 0 || number > 100)
                    throw IllegalArgumentException("number is out of range")

                if(number == 0)
                    return "00"

                if(number < 10)
                    return "0$number"

                return number.toString()
            }

            return "${digit2Str(localTime.dayOfMonth)}.${digit2Str(localTime.monthNumber)}. ${digit2Str(localTime.hour)}:${digit2Str(localTime.minute)}:${digit2Str(localTime.second)}"
        }

        println("$run / $turns: wins: white: ${getPercentStr(whiteWinCnt)}, black: ${getPercentStr(blackWinCnt)}, draws: ${getPercentStr(drawCnt)}, (duplicate games: ${run - uniqueResults.size})")
        println("\t${dateTimeStr(now)}: white: ${playerWhite.getTimeStr()}, black: ${playerBlack.getTimeStr()}, approx.End: ${dateTimeStr(approxEnd)}")
    }

    val channel = Channel<GameResult>()
    for(i in 1 .. turns)
    {
        GlobalScope.async {
            val result = runSingleComputerVsComputerRun(playerWhite, playerBlack, SimulationVerbosity.Silent)
            channel.send(result)
        }
    }

    runBlocking {
        for(i in 1 .. turns)
        {
            val result = channel.receive()
            when(result.FinalGameState.Winner)
            {
                PlayerColor.White -> ++whiteWinCnt
                PlayerColor.Black -> ++blackWinCnt
                PlayerColor.Empty -> ++drawCnt
                null -> throw IllegalStateException("Should never happen")
            }

            uniqueResults.add(result.FinalGameState)

            printStats(i)
        }
    }

    return DirectedRunResult(whiteWinCnt, blackWinCnt, drawCnt, playerWhite.TotalElapsedSeconds, playerBlack.TotalElapsedSeconds, sw.ElapsedSeconds)
}

private fun runComputerVsComputerSimulation(turns : Int, commonPlayer : IPlayer, parallel: Boolean)
{
    println("Automatic game: ${turns}x ${commonPlayer.StrategyName} vs. itself:")
    val result = getOptionalParallelDirectedComputerVsComputerSimulation(turns, commonPlayer, commonPlayer, parallel)
    fun winStr(cnt : Int) = getWinsString(cnt, turns)

    println("\twins: white: ${winStr(result.WhiteWinCnt)}, black: ${winStr(result.BlackWinCnt)}, draws: ${winStr(result.DrawCnt)}")

    fun Double.timeStr() = getTimeString(this, turns)
    println("\ttimes: white: ${result.WhiteSeconds.timeStr()}, black: ${result.BlackSeconds.timeStr()}, total: ${result.TotalElapsedSeconds.toInt()} s")
}

private fun runComputerVsComputerSimulation(turns : Int, player1 : IPlayer, player2: IPlayer, parallel : Boolean)
{
    println("Automatic game: ${turns}x ${player1.StrategyName} vs. ${player2.StrategyName}:")

    fun totalTimeStr(totalSeconds : Double) = "${totalSeconds.toInt()} s"

    fun playDirected(playerWhite: IPlayer, playerBlack : IPlayer) : DirectedRunResult
    {
        println("\tdirection: ${turns}x  white: ${playerWhite.StrategyName} vs. black: ${player2.StrategyName}")
        val result = getOptionalParallelDirectedComputerVsComputerSimulation(turns, playerWhite, playerBlack, parallel)

        fun printPlayerRes(player : IPlayer, colorStr : String, winCnt : Int, totalSeconds : Double)
        {
            println("\t\t${player.StrategyName}\r\n\t\t\t($colorStr): wins: ${getWinsString(winCnt, turns)}, Time: ${getTimeString(totalSeconds, turns)}")
        }

        if(player1 == playerWhite)
        {
            printPlayerRes(player1, "white", result.WhiteWinCnt, result.WhiteSeconds)
            printPlayerRes(player2, "black", result.BlackWinCnt, result.BlackSeconds)
        }
        else
        {
            printPlayerRes(player1, "black", result.BlackWinCnt, result.BlackSeconds)
            printPlayerRes(player2, "white", result.WhiteWinCnt, result.WhiteSeconds)
        }
        println("\t\tdraws: ${getWinsString(result.DrawCnt, turns)}, total time: ${totalTimeStr(result.TotalElapsedSeconds)}")

        return result
    }

    val result1 = playDirected(player1, player2)
    val result2 = playDirected(player2, player1)

    fun totalWinStr(totalWinCnt : Int) = getWinsString(totalWinCnt, 2*turns)

    println("\tTotal results:")
    println("\t\tby Color:")
    println("\t\t\twins: white: ${totalWinStr(result1.WhiteWinCnt+result2.WhiteWinCnt)}, black: ${totalWinStr(result1.BlackWinCnt + result2.BlackWinCnt)}, Draws: ${totalWinStr(result1.DrawCnt + result2.DrawCnt)}")

    fun timeStr(totalTime1: Double, totalTime2 : Double) = getTimeString(totalTime1 + totalTime2, 2*turns)
    println("\t\t\ttime: white: ${timeStr(result1.WhiteSeconds,result2.WhiteSeconds)}, black: ${timeStr(result1.BlackSeconds,result2.BlackSeconds)}, total: ${totalTimeStr(result1.TotalElapsedSeconds + result2.TotalElapsedSeconds)}")
    println("\t\tby strategy/player:")
    println("\t\t\t${player1.StrategyName}:\r\n\t\t\t\twins: ${totalWinStr(result1.WhiteWinCnt + result2.BlackWinCnt)}, Time: ${timeStr(result1.WhiteSeconds, result2.BlackSeconds)}")
    println("\t\t\t${player2.StrategyName}:\r\n\t\t\t\twins: ${totalWinStr(result1.BlackWinCnt + result2.WhiteWinCnt)}, Time: ${timeStr(result1.BlackSeconds, result2.WhiteSeconds)}")
}

fun parallelConsoleMain(args : Array<String>)
{
    var turns = 100

    fun parseInt(argIndex : Int, name : String) : Int
    {
        val arg = args[argIndex]
        val intVal = arg.toIntOrNull()
        if(intVal == null)
            throw UsageException("$name ('$arg') is supposed to be an int")

        return intVal
    }

    fun parsePlayer(argIndex : Int, name : String) : IPlayer
    {
        val arg = args[argIndex]
        if(arg.equals("random", true))
            return RandomPlayer

        fun getDepth(argument : String, startText : String) : Int?
        {
            if(!argument.startsWith(startText, true))
                return null

            val noTxt = argument.substring(startText.length)
            return noTxt.toIntOrNull()
        }

        fun getDepth(startText : String) = getDepth(arg, startText)

        var d = getDepth("simple")
        if(d != null)
            return SimpleComputerPlayer(d, SimpleHeuristic)

        d= getDepth("endweighting")
        if(d != null)
            return SimpleComputerPlayer(d, EndWeightingHeuristics)

        // endweighting<FACTOR>corner<DEPTH> or weighting<FACTOR>corner<DEPTH>
        fun parsePart1FactorPart2Depth(part1 : String, part2: String, createHeuristic : (Double) -> IHeuristic) : IPlayer?
        {
            if(!arg.startsWith(part1, true))
                return null

            fun Char.isDigit() = this >= '0' && this <= '9'

            var digitCnt = 0
            while (arg.length > digitCnt + part1.length && arg[digitCnt + part1.length].isDigit())
                ++digitCnt

            if(digitCnt == 0)
                return null

            val factor = arg.substring(part1.length, part1.length + digitCnt).toInt()
            val restArg = arg.substring(part1.length + digitCnt)

            val depth = getDepth(restArg, part2)
            if(depth == null)
                return null

            val heuristic = createHeuristic(factor.toDouble())
            return SimpleComputerPlayer(depth, heuristic)
        }

        val endWeightingCornerPlayer = parsePart1FactorPart2Depth("endweighting", "corner", {factor -> EndWeightingCornerHeuristic(factor) })
        if(endWeightingCornerPlayer != null)
            return endWeightingCornerPlayer

        val weightingCornerPlayer = parsePart1FactorPart2Depth("weighting", "corner", {factor -> WeightingCornerHeuristic(factor) })
        if(weightingCornerPlayer != null)
            return weightingCornerPlayer

        throw UsageException("'$arg' is no valid computer player ($name)")
    }

    try
    {
        if(args.size > 0)
            turns = parseInt(0, "turns")

        val player1 : IPlayer = if(args.size > 1)
            parsePlayer(1, "player1")
        else
            SimpleComputerPlayer()

        val player2 = if(args.size > 2)
            parsePlayer(2, "player2")
        else
            player1

        var parallel = false
        if(args.size > 3)
            if(args[3] != "-p")
                throw UsageException("")
            else
                parallel = true

        if(args.size>4)
            throw UsageException("")

        if(player1 == player2)
            runComputerVsComputerSimulation(turns, player1, parallel)
        else
            runComputerVsComputerSimulation(turns, player1, player2, parallel)
    }
    catch (uEx : UsageException)
    {
        val msg = uEx.message
        if(msg != null && msg.isNotEmpty())
            println("Error: $msg")

        println("Usage: Othello [<turns> [<player1> [<player2 [-p]]]]")
        println("\twhere:")
        println("\tturns: number of games played")
        println("\tplayer1: one <PLAYER> (see below)")
        println("\tplayer2: one <PLAYER> (see below)")
        println("\tPLAYER: RANDOM or SIMPLE<Depth>, ENDWEIGHTING<DEPTH>, WEIGHTING<FACTOR>CORNER<DEPTH>, ENDWEIGHTING<FACTOR>CORNER<DEPTH>")
    }
    //runConsoleGameWithRandomComputerOpponent()
}