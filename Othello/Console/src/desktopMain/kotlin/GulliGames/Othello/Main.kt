package gulligames.othello

import gulligames.othello.base.*
import gulligames.util.*
import gullitools.util.*
import kotlinx.datetime.*
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.random.Random

private const val _whiteChar = 'x'
private const val _blackChar = 'o'

private fun printGame(game: Game, extraSpacePerCol: Int = 1)
{
    fun printLine()
    {
        for(x in 0 until game.Width)
            for(i in 1 .. extraSpacePerCol+2)
                print('-')

        println("-")
    }

    printLine()

    fun getCharToPrint(x : Int, y: Int) : Char
    {
        val stone = Index2D(x, y)
        return when(game[stone])
        {
            PlayerColor.White -> _whiteChar
            PlayerColor.Black -> _blackChar
            PlayerColor.Empty -> if(game.canSet(stone)) '#' else ' '
        }
    }

    for(y in game.Height-1 downTo 0)
    {
        for(x in 0 until game.Width)
        {
            print('|')
            print(getCharToPrint(x, y))

            for(i in 1 .. extraSpacePerCol)
                print(' ')
        }

        println("|  $y")
    }

    printLine()

    for(x in 0 until game.Width)
    {
        print(' ')
        print(x)

        val extraSpaces = extraSpacePerCol + 1 - getDigitCount(x)
        for(i in 1 .. extraSpaces)
            print(' ')
    }

    println()
    if(game.IsGameOver)
    {
        val winner = when
        {
            game.WhiteStoneCount > game.BlackStoneCount -> "Gewinner: $_whiteChar!!"
            game.BlackStoneCount > game.WhiteStoneCount -> "Gewinner: $_blackChar!!"
            else                                        -> "Unentschieden"
        };

        println("Game over - $winner")
    }
    else if(game.WhitesTurn)
    {
        println("$_whiteChar am Zug")
    }
    else
    {
        println("$_blackChar am Zug")
    }
}

private fun runConsoleGameWithRandomComputerOpponent()
{
    val game = Game()
    val computerPlayerColor = if(Random.nextBoolean()) PlayerColor.Black else PlayerColor.White

    val computerPlayerAgent = SimpleComputerPlayer()

    fun isComputersTurn() : Boolean
    {
        return when(computerPlayerColor)
        {
            PlayerColor.Empty -> false
            PlayerColor.Black -> !game.WhitesTurn
            PlayerColor.White -> game.WhitesTurn
        }
    }

    while (!game.IsGameOver)
    {
        printGame(game)

        if(isComputersTurn())
        {
            game.set(computerPlayerAgent.proposeMove(game).Move)
            continue
        }

        while (true)
        {
            val x = queryPositiveInt("X", game.Width, -1)
            if (x == null)
                return

            val y = queryPositiveInt("Y", game.Height, -1)
            if (y == null)
                return

            val stone = Index2D(x, y)
            if(game.canSet(stone))
            {
                game.set(stone)
                break
            }

            println("Dieser Zug ist nicht möglich")
        }
    }

    printGame(game)
}

data class GameResult(val FinalGameState: Game, val Moves : List<Index2D>)

fun runSingleComputerVsComputerRun(playerWhite : IPlayer, playerBlack : IPlayer, simulationVerbosity: SimulationVerbosity) : GameResult
{
    val game = Game()

    if(simulationVerbosity == SimulationVerbosity.ShowProgress)
    {
        // show some reference to the dot's
        for(x in 0 until game.Width)
            for(y in 0 until game.Height)
                print("#")

        println()
    }

    fun IPlayer.getActualPlayer() = if(simulationVerbosity == SimulationVerbosity.ShowGame) getVerbose() else this
    val actualPlayerWhite = playerWhite.getActualPlayer()
    val actualPlayerBlack = playerBlack.getActualPlayer()

    val moves = ArrayList<Index2D>()

    fun showState()
    {
        when(simulationVerbosity)
        {
            SimulationVerbosity.ShowGame -> printGame(game)
            SimulationVerbosity.ShowProgress -> print(".")
            SimulationVerbosity.Silent -> {}
        }
    }

    while (!game.IsGameOver)
    {
        showState()

        val player = if(game.WhitesTurn) actualPlayerWhite else actualPlayerBlack
        val move = player.proposeMove(game).Move
        game.set(move)
        moves.add(move)
    }

    showState()
    if(simulationVerbosity!= SimulationVerbosity.Silent)
        println()

    return GameResult(game, moves)
}

enum class SimulationVerbosity
{
    ShowGame,
    ShowProgress,
    Silent
}

fun getTimeString(totalElapsedSeconds : Double, runs : Int) : String
{
    val avg = ((totalElapsedSeconds / runs) * 1000.0).roundToInt() / 1000.0
    return "avg. $avg s"
}

fun getWinsString(cnt : Int, total : Int) : String
{
    val percent = (100.0 * cnt / total).roundToInt()
    return "${cnt}x ($percent %)"
}

private fun runComputerVsComputerSimulation(turns : Int, commonPlayer : IPlayer)
{
    println("Automatic game: ${turns}x ${commonPlayer.StrategyName} vs. itself:")
    val result = getDirectedComputerVsComputerSimulation(turns, commonPlayer, commonPlayer)
    fun winStr(cnt : Int) = getWinsString(cnt, turns)

    println("\twins: white: ${winStr(result.WhiteWinCnt)}, black: ${winStr(result.BlackWinCnt)}, draws: ${winStr(result.DrawCnt)}")

    fun Double.timeStr() = getTimeString(this, turns)
    println("\ttimes: white: ${result.WhiteSeconds.timeStr()}, black: ${result.BlackSeconds.timeStr()}, total: ${result.TotalElapsedSeconds.toInt()} s")
}

private fun runComputerVsComputerSimulation(turns : Int, player1 : IPlayer, player2: IPlayer)
{
    println("Automatic game: ${turns}x ${player1.StrategyName} vs. ${player2.StrategyName}:")

    fun totalTimeStr(totalSeconds : Double) = "${totalSeconds.toInt()} s"

    fun playDirected(playerWhite: IPlayer, playerBlack : IPlayer) : DirectedRunResult
    {
        println("\tdirection: ${turns}x  white: ${playerWhite.StrategyName} vs. black: ${player2.StrategyName}")
        val result = getDirectedComputerVsComputerSimulation(turns, playerWhite, playerBlack)

        fun printPlayerRes(player : IPlayer, colorStr : String, winCnt : Int, totalSeconds : Double)
        {
            println("\t\t${player.StrategyName}\r\n\t\t\t($colorStr): wins: ${getWinsString(winCnt, turns)}, Time: ${getTimeString(totalSeconds, turns)}")
        }

        if(player1 == playerWhite)
        {
            printPlayerRes(player1, "white", result.WhiteWinCnt, result.WhiteSeconds)
            printPlayerRes(player2, "black", result.BlackWinCnt, result.BlackSeconds)
        }
        else
        {
            printPlayerRes(player1, "black", result.BlackWinCnt, result.BlackSeconds)
            printPlayerRes(player2, "white", result.WhiteWinCnt, result.WhiteSeconds)
        }
        println("\t\tdraws: ${getWinsString(result.DrawCnt, turns)}, total time: ${totalTimeStr(result.TotalElapsedSeconds)}")

        return result
    }

    val result1 = playDirected(player1, player2)
    val result2 = playDirected(player2, player1)

    fun totalWinStr(totalWinCnt : Int) = getWinsString(totalWinCnt, 2*turns)

    println("\tTotal results:")
    println("\t\tby Color:")
    println("\t\t\twins: white: ${totalWinStr(result1.WhiteWinCnt + result2.WhiteWinCnt)}, black: ${totalWinStr(result1.BlackWinCnt + result2.BlackWinCnt)}, Draws: ${totalWinStr(result1.DrawCnt + result2.DrawCnt)}")

    fun timeStr(totalTime1: Double, totalTime2 : Double) = getTimeString(totalTime1 + totalTime2, 2*turns)
    println("\t\t\ttime: white: ${timeStr(result1.WhiteSeconds,result2.WhiteSeconds)}, black: ${timeStr(result1.BlackSeconds,result2.BlackSeconds)}, total: ${totalTimeStr(result1.TotalElapsedSeconds + result2.TotalElapsedSeconds)}")
    println("\t\tby strategy/player:")
    println("\t\t\t${player1.StrategyName}:\r\n\t\t\t\twins: ${totalWinStr(result1.WhiteWinCnt + result2.BlackWinCnt)}, Time: ${timeStr(result1.WhiteSeconds, result2.BlackSeconds)}")
    println("\t\t\t${player2.StrategyName}:\r\n\t\t\t\twins: ${totalWinStr(result1.BlackWinCnt + result2.WhiteWinCnt)}, Time: ${timeStr(result1.BlackSeconds, result2.WhiteSeconds)}")
}

class DirectedRunResult(val WhiteWinCnt: Int, val BlackWinCnt : Int, val DrawCnt : Int, val WhiteSeconds : Double, val BlackSeconds: Double, val TotalElapsedSeconds : Double)

fun getDirectedComputerVsComputerSimulation(turns : Int, white : IPlayer, black : IPlayer) : DirectedRunResult
{
    val playerWhite = white.collectTime()
    val playerBlack = black.collectTime()

    var whiteWinCnt = 0
    var blackWinCnt = 0
    var drawCnt = 0

    val uniqueResults = HashSet<Game>()

    val sw = Stopwatch()
    var lastElapsedMs = sw.ElapsedMilliseconds
    var lastTimeStepMs = 10000.0

    fun printStats(run : Int)
    {
        val elapsedMs = sw.ElapsedMilliseconds
        if(elapsedMs - lastElapsedMs < lastTimeStepMs )
            return

        val avgTimePerRun = sw.ElapsedMilliseconds / run
        val expectedRemainingTimeMs  = (turns - run) * avgTimePerRun
        if(expectedRemainingTimeMs < 0.3 * lastTimeStepMs)
            return // short before finishing this directed run

        lastElapsedMs = elapsedMs

        // was this the preLast or last log msg
        if(expectedRemainingTimeMs < 2.0 * lastTimeStepMs && expectedRemainingTimeMs > 1.7 * lastTimeStepMs)
        {
            // yes - just break in the middle
            lastTimeStepMs = expectedRemainingTimeMs * 0.5
        }
        else if(expectedRemainingTimeMs > 2.0 * lastTimeStepMs)
        {
            // compute expected total time to adjust timeStep
            val expectedLogMessages = (avgTimePerRun * turns) / lastTimeStepMs
            val maxLogMessages = 10
            if (expectedLogMessages > maxLogMessages)
            {
                val maxFactor = min(2.0, expectedRemainingTimeMs / lastTimeStepMs / 2.0)

                val adjustmentFactor = min(maxFactor, 1.0 * expectedLogMessages / maxLogMessages)
                lastTimeStepMs *= adjustmentFactor
            }
        }

        val total = whiteWinCnt + blackWinCnt + drawCnt

        fun getPercentStr(cnt : Int) : String
        {
            val percent = (100.0 * cnt / total).roundToInt()
            return "$percent %"
        }

        fun TimeCollectingComputerPlayer.getTimeStr() = getTimeString(TotalElapsedSeconds, run)

        val now = Clock.System.now()
        val approxEnd = now.plus(expectedRemainingTimeMs.toLong(), DateTimeUnit.MILLISECOND, TimeZone.currentSystemDefault())

        fun dateTimeStr(dateTime : Instant) : String
        {
            val localTime = dateTime.toLocalDateTime(TimeZone.currentSystemDefault())
            return "${localTime.dayOfMonth.AsTwoDigitString}.${localTime.monthNumber.AsTwoDigitString}. ${localTime.hour.AsTwoDigitString}:${localTime.minute.AsTwoDigitString}:${localTime.second.AsTwoDigitString}"
        }

        println("$run / $turns: wins: white: ${getPercentStr(whiteWinCnt)}, black: ${getPercentStr(blackWinCnt)}, draws: ${getPercentStr(drawCnt)}, (duplicate games: ${run - uniqueResults.size})")
        println("\t${dateTimeStr(now)}: white: ${playerWhite.getTimeStr()}, black: ${playerBlack.getTimeStr()}, approx.End: ${dateTimeStr(approxEnd)}")
    }

    for(i in 1 .. turns)
    {
        val result = runSingleComputerVsComputerRun(playerWhite, playerBlack, SimulationVerbosity.Silent)
        //println("\twinner: ${result.Winner}")

        when(result.FinalGameState.Winner!!)
        {
            PlayerColor.White -> ++whiteWinCnt
            PlayerColor.Black -> ++blackWinCnt
            PlayerColor.Empty -> ++drawCnt
        }

        uniqueResults.add(result.FinalGameState)

        printStats(i)
    }

    return DirectedRunResult(whiteWinCnt, blackWinCnt, drawCnt, playerWhite.TotalElapsedSeconds, playerBlack.TotalElapsedSeconds, sw.ElapsedSeconds)
}

class UsageException(msg : String) : Exception(msg)

fun runConsoleMain(args : Array<String>)
{
    var turns = 100

    fun parseInt(argIndex : Int, name : String) : Int
    {
        val arg = args[argIndex]
        val intVal = arg.toIntOrNull()
        if(intVal == null)
            throw UsageException("$name ('$arg') is supposed to be an int")

        return intVal
    }

    fun parsePlayer(argIndex : Int, name : String) : IPlayer
    {
        val arg = args[argIndex]
        if(arg.equals("random", true))
            return RandomPlayer

        fun getDepth(argument : String, startText : String) : Int?
        {
            if(!argument.startsWith(startText, true))
                return null

            val noTxt = argument.substring(startText.length)
            return noTxt.toIntOrNull()
        }

        fun getDepth(startText : String) = getDepth(arg, startText)

        var d = getDepth("simple")
        if(d != null)
            return SimpleComputerPlayer(d, SimpleHeuristic)

        d = getDepth("endweighting")
        if(d != null)
            return SimpleComputerPlayer(d, EndWeightingHeuristics)

        // endweighting<FACTOR>corner<DEPTH> or weighting<FACTOR>corner<DEPTH>
        fun parsePart1FactorPart2Depth(part1 : String, part2: String, createHeuristic : (Double) -> IHeuristic) : IPlayer?
        {
            if(!arg.startsWith(part1, true))
                return null

            fun Char.isDigit() = this >= '0' && this <= '9'

            var digitCnt = 0
            while (arg.length > digitCnt + part1.length && arg[digitCnt + part1.length].isDigit())
                ++digitCnt

            if(digitCnt == 0)
                return null

            val factor = arg.substring(part1.length, part1.length + digitCnt).toInt()
            val restArg = arg.substring(part1.length + digitCnt)

            val depth = getDepth(restArg, part2)
            if(depth == null)
                return null

            val heuristic = createHeuristic(factor.toDouble())
            return SimpleComputerPlayer(depth, heuristic)
        }

        val endWeightingCornerPlayer = parsePart1FactorPart2Depth("endweighting", "corner", {factor -> EndWeightingCornerHeuristic(factor)})
        if(endWeightingCornerPlayer != null)
            return endWeightingCornerPlayer

        val weightingCornerPlayer = parsePart1FactorPart2Depth("weighting", "corner", {factor -> WeightingCornerHeuristic(factor)})
        if(weightingCornerPlayer != null)
            return weightingCornerPlayer

        throw UsageException("'$arg' is no valid computer player ($name)")
    }

    try
    {
        if(args.isNotEmpty())
            turns = parseInt(0, "turns")

        val player1 : IPlayer = if(args.size > 1)
            parsePlayer(1, "player1")
        else
            SimpleComputerPlayer()

        val player2 = if(args.size > 2)
            parsePlayer(2, "player2")
        else
            player1

        if(args.size > 3)
            throw UsageException("")

        if(player1 == player2)
            runComputerVsComputerSimulation(turns, player1)
        else
            runComputerVsComputerSimulation(turns, player1, player2)
    }
    catch (uEx : UsageException)
    {
        val msg = uEx.message
        if(!msg.isNullOrEmpty())
            println("Error: $msg")

        println("Usage: Othello [<turns> [<player1> [<player2]]]")
        println("\twhere:")
        println("\tturns: number of games played")
        println("\tplayer1: one <PLAYER> (see below)")
        println("\tplayer2: one <PLAYER> (see below)")
        println("\tPLAYER: RANDOM or SIMPLE<Depth>, ENDWEIGHTING<DEPTH>, WEIGHTING<FACTOR>CORNER<DEPTH>, ENDWEIGHTING<FACTOR>CORNER<DEPTH>")
    }
    //runConsoleGameWithRandomComputerOpponent()
}