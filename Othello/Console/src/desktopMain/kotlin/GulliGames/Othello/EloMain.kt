package gulligames.othello

import gulligames.elo.*
import gulligames.othello.base.*
import gullitools.util.Stopwatch

private class OptionalArg<T>(private val _title: String, val Default : T)
{
    private var _value : T? = null
    fun setValue(value : T)
    {
        if(_value != null)
            throw UsageException("Duplicate specification of switch $_title")

        _value = value
    }

    val FinalValue : T
        get() = _value ?: Default
}

fun eloMain(eloStore : IEloStore<IPlayer>, args: Array<String>)
{
    val onlyShowEloArg = OptionalArg("show elo-statistics and exit (-showElo)", false)
    val showSingleGamesArg = OptionalArg("show single games (-s)", false)
    val turnsToLogArg = OptionalArg("Turns to big log", 100)
    val maxTurnsArg = OptionalArg<Int?>("Turns to conduct", null)

    try
    {
        val parsedInts = ArrayList<Int>()

        for(arg in args)
        {
            if(arg.lowercase() == "-s")
            {
                showSingleGamesArg.setValue(true)
            }
            else if(arg.lowercase() == "-showelo")
            {
                onlyShowEloArg.setValue(true)
            }
            else
            {
                val possiblyInt = arg.toIntOrNull()
                if(possiblyInt != null && possiblyInt > 0)
                    parsedInts.add(possiblyInt)
                else
                    throw UsageException("")
            }
        }

        if(parsedInts.size > 0)
            turnsToLogArg.setValue(parsedInts[0])

        if(parsedInts.size > 1)
            maxTurnsArg.setValue(parsedInts[1])

        if(parsedInts.size > 2)
            throw UsageException("")

        eloMain(eloStore, showSingleGamesArg.FinalValue, turnsToLogArg.FinalValue, maxTurnsArg.FinalValue, onlyShowEloArg.FinalValue)
    }
    catch (uex : UsageException)
    {
        val msg = uex.message
        if(msg != null && msg.length > 0)
            println(msg)

        println("Usage: Othello.Console ([-s] [turnsToLog [totalTurns]] | -showElo)")
        println("where:")
        println("\t-s: show each game outcome and not only statistics after a certain number of games")
        println("\tturnsToLog: By default after ${turnsToLogArg.Default} games a log is presented. To change this, supply a positive number here.")
        println("\ttotalTurns: By default, the evaluation runs forever (until stopped by e.g. ^C). To change this, supply a positive number here")
        println("\t            to denote, how many games should be played.")
        println("\t-showElo: Only show statistics in current elo file and exist")
    }
}

private fun playSingleGame(playerWhite : IPlayer, playerBlack : IPlayer) : SingleGameStatistics
{
    val game = Game()

    var thinkTimeWhite : Long = 0
    var thinkTimeBlack : Long = 0

    while (!game.IsGameOver)
    {
        val isWhite = game.WhitesTurn
        val player = if(isWhite) playerWhite else playerBlack
        val sw = Stopwatch()
        val move = player.proposeMove(game).Move
        val thinkTimeMs = sw.ElapsedMilliseconds
        if(isWhite)
            thinkTimeWhite += thinkTimeMs
        else
            thinkTimeBlack += thinkTimeMs

        game.set(move)
    }

    val outcome = when(game.Winner!!)
    {
        PlayerColor.White -> GameOutcome.Win
        PlayerColor.Black -> GameOutcome.Loss
        PlayerColor.Empty -> GameOutcome.Draw
    }

    return SingleGameStatistics(outcome, thinkTimeWhite, thinkTimeBlack)
}

private fun log(txt : String) = println(txt)

private fun dontLog(txt: String) {}
private fun doLog(txt: String) = log(txt)

private fun gatherAvailablePlayers() : Set<IPlayer>
{
    val availablePlayers = HashSet<IPlayer>()
    availablePlayers.add(RandomPlayer)

    val availableHeuristic = setOf(SimpleHeuristic, EndWeightingHeuristics, ThreadHeuristics2, ThreadMulHeuristics,
      /*  EndWeightingCornerHeuristic(1.0), EndWeightingCornerHeuristic(2.0), EndWeightingCornerHeuristic(5.0),*/ /*EndWeightingCornerHeuristic(10.0),
        EndWeightingCornerHeuristic(20.0), EndWeightingCornerHeuristic(30.0),*/ EndWeightingCornerHeuristic(50.0),
        /*WeightingCornerHeuristic(1.0), WeightingCornerHeuristic(2.0), WeightingCornerHeuristic(5.0),*/ /*WeightingCornerHeuristic(10.0),*/
        /*WeightingCornerHeuristic(20.0),*/ /*WeightingCornerHeuristic(30.0),*/ WeightingCornerHeuristic(50.0))

    for(i in 2 until 6)
        for(heuristics in availableHeuristic)
            availablePlayers.add(SimpleComputerPlayer(i, heuristics))

    val evals = setOf(100L, 200L, 1000L, 2000L, 5000L, 7000L)
    for(e in evals)
        for(heuristics in availableHeuristic)
            availablePlayers.add(ConstantTimeComputerPlayer(e, heuristics))

    return availablePlayers
}

internal val AvailablePlayers = gatherAvailablePlayers()

private fun eloMain(eloStore : IEloStore<IPlayer>, showSingleGames : Boolean, turnsToLogStats : Int, maxTurns : Int?, showOnlyEloStatsAndExit : Boolean)
{
    val logSingleGame = if(showSingleGames) ::doLog else ::dontLog
    val logSettings = GameLogSettings(logSingleGame, ::doLog, turnsToLogStats)

    val playerEvaluator = ComputerPlayerEvaluator(eloStore, AvailablePlayers, ::playSingleGame, logSettings)

    if(showOnlyEloStatsAndExit)
        playerEvaluator.showEloStatistics()
    else
        playerEvaluator.runInfinite(maxTurns)
}