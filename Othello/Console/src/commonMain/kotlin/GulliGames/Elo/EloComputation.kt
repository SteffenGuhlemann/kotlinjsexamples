package gulligames.elo

import kotlin.math.*

enum class GameOutcome
{
    Win,
    Draw,
    Loss
}

data class SingleGameStatistics(val OutcomePlayer1 : GameOutcome, val ThinkTimePlayer1Ms : Long, val ThinkTimePlayer2Ms : Long)

private fun getExpectedScore(oldElo : Int, oldEloOtherPlayer : Int) : Double
{
    // trim difference to range -400 .. +400
    val trimmedDifference = min(400, max(-400, oldEloOtherPlayer - oldElo))
    return 1.0 / (1.0 + 10.0.pow(1.0 * trimmedDifference / 400.0))
}

private val GameOutcome.Score : Double
    get()
    {
        return when(this)
        {
            GameOutcome.Win  -> 1.0
            GameOutcome.Draw -> 0.5
            GameOutcome.Loss -> 0.0
        }
    }

private fun getAdaptedElo(oldStats : GameStatistics, score : Double, expectedOutcome : Double) : Int
{
    // conversion rate k:
    // * k commonly is 20,
    // * for top players (elo > 2400) 10,
    //  * at less than 30 weighted games and for youth player (<18y, Elo < 2300) 40
    // for now use 20
    val k = if(oldStats.GameCount < 30)
        40
    else if(oldStats.Elo > 1500)
        10
    else
        20

    return oldStats.Elo + (k * (score - expectedOutcome)).toInt()
}

data class PlayerUpdateStatistics(val OldElo : Int, val NewStatistics : GameStatistics)
{
    override fun toString() = "${NewStatistics.GameCount} games, Elo: $OldElo -> ${NewStatistics.Elo}"
}

data class UpdateStatistics(val Player1Statistics : PlayerUpdateStatistics, val Player2Statistics : PlayerUpdateStatistics)
{
    override fun toString() = "player1: $Player1Statistics, player2: $Player2Statistics"
}

fun <TPlayer> updateElo(eloStore : IEloStore<TPlayer>, player1 : TPlayer, player2 : TPlayer, singleGameStatistics : SingleGameStatistics) : UpdateStatistics
{
    val scorePlayer1 = singleGameStatistics.OutcomePlayer1.Score

    val newPlayerElo = 1000
    val oldStatsPlayer1 = eloStore.getStatisticOrNewPlayerVal(player1, newPlayerElo)
    val oldStatsPlayer2 = eloStore.getStatisticOrNewPlayerVal(player2, newPlayerElo)

    val expectedOutcomePlayer1 = getExpectedScore(oldStatsPlayer1.Elo, oldStatsPlayer2.Elo)

    val newEloPlayer1 = getAdaptedElo(oldStatsPlayer1, scorePlayer1, expectedOutcomePlayer1)
    val newEloPlayer2 = getAdaptedElo(oldStatsPlayer2, 1.0 - scorePlayer1, 1.0 - expectedOutcomePlayer1)

    eloStore.addGame(player1, newEloPlayer1, singleGameStatistics.ThinkTimePlayer1Ms)
    eloStore.addGame(player2, newEloPlayer2, singleGameStatistics.ThinkTimePlayer2Ms)

    fun getPlayerStats(oldStats : GameStatistics, newElo : Int, thinkTimeMs : Long) : PlayerUpdateStatistics
    {
        return PlayerUpdateStatistics(oldStats.Elo, GameStatistics(oldStats.GameCount + 1, newElo, oldStats.TotalMsThinkTime + thinkTimeMs))
    }

    val player1Stats = getPlayerStats(oldStatsPlayer1, newEloPlayer1, singleGameStatistics.ThinkTimePlayer1Ms)
    val player2Stats = getPlayerStats(oldStatsPlayer2, newEloPlayer2, singleGameStatistics.ThinkTimePlayer2Ms)
    return UpdateStatistics(player1Stats, player2Stats)
}