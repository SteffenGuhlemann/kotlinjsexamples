package gulligames.elo

import kotlin.math.abs
import gullitools.math.RandomProvider
import gullitools.util.*

data class GameLogSettings(val LogSingleGameOutcome : (String) -> Unit, val RegularLogStats : (String) -> Unit, val TurnsToLogStats : Int)

class ComputerPlayerEvaluator<TPlayer>
{
    private val _store : IEloStore<TPlayer>
    private val _availablePlayers : List<TPlayer>
    private val _playGame : (TPlayer, TPlayer) -> SingleGameStatistics
    private val _logSettings : GameLogSettings

    constructor(store : IEloStore<TPlayer>, availablePlayers : Set<TPlayer>, playGame : (TPlayer, TPlayer) -> SingleGameStatistics,
                logSettings: GameLogSettings)
    {
        if(availablePlayers.size < 2)
            throw IllegalArgumentException("At least 2 distinct players need to be supplied")

        _store = store
        _availablePlayers = availablePlayers.toList()
        _playGame = playGame
        _logSettings = logSettings
    }

    private class CurrentRunInformation<TPlayer>(val Elos : HashMap<TPlayer, Int>, val Ranks : HashMap<TPlayer, Int>, val RunCount : Int,
                                                 val ElapsedTimeString : String)

    private fun doShowEloStatistics(currentRunInformation: CurrentRunInformation<TPlayer>?)
    {
        val stats = _availablePlayers.asSequence()
            .filter { _store.getStatistics(it) != null }
            .map { PlayerStats(it, _store.getStatistics(it)!!) }
            .toMutableList()

        stats.sortBy { -it.Statistics.Elo }

        // each game is counted in 2 different players
        val totalGamesPlayedStr = (stats.sumOf { it.Statistics.GameCount } / 2).formatWithThousandSeparator()
        val totalPlayerThinkingTimeString = msToTimeString(stats.sumOf { it.Statistics.TotalMsThinkTime })

        if(currentRunInformation != null)
        {
            var totalRankChanges = 0
            var totalEloChange = 0
            for (i in stats.indices)
            {
                val stat = stats[i]
                totalRankChanges += abs(i + 1 - currentRunInformation.Ranks[stat.Player]!!)
                totalEloChange += abs(stat.Statistics.Elo - currentRunInformation.Elos[stat.Player]!!)
            }

            _logSettings.RegularLogStats("\r\nAfter ${currentRunInformation.RunCount} (total: ${currentRunInformation.ElapsedTimeString}) turns: evaluated ${stats.size} / ${_availablePlayers.size} players (total changes: elo: $totalEloChange, rank: $totalRankChanges)")
            _logSettings.RegularLogStats("(Total in elo-file: $totalGamesPlayedStr games, $totalPlayerThinkingTimeString player thinking time)")
        }
        else
        {
            _logSettings.RegularLogStats("Elo-Information in file:")
            _logSettings.RegularLogStats("\tevaluated ${stats.size} / ${_availablePlayers.size} players")
            _logSettings.RegularLogStats("\tTotal games played: $totalGamesPlayedStr")
            _logSettings.RegularLogStats("\tTotal player think time: $totalPlayerThinkingTimeString\r\n")
        }

        for(i in stats.indices)
        {
            val stat = stats[i]

            if(currentRunInformation != null)
            {
                fun getChangeStr(oldVal : Int, newVal : Int) = if(oldVal == newVal)
                    ""
                else if (oldVal < newVal)
                    " (+${newVal - oldVal})"
                else
                    " (${newVal - oldVal})"

                val rankChangeStr = getChangeStr(i + 1, currentRunInformation.Ranks[stat.Player]!!) // exchange + (for rank increase => position goes down (e.g. from 2nd place to 1rst place => print (+1)
                val eloChangeStr = getChangeStr(currentRunInformation.Elos[stat.Player]!!, stat.Statistics.Elo)
                _logSettings.RegularLogStats("${i+1}.$rankChangeStr ${stat.Player}: Elo: ${stat.Statistics.Elo.formatWithThousandSeparator()}$eloChangeStr, ${stat.Statistics.GameCount.formatWithThousandSeparator()} games, avg.: ${msToTimeString(stat.Statistics.TotalMsThinkTime / stat.Statistics.GameCount)} / game")
            }
            else
            {
                _logSettings.RegularLogStats("${i+1}. ${stat.Player}: Elo: ${stat.Statistics.Elo.formatWithThousandSeparator()}, ${stat.Statistics.GameCount.formatWithThousandSeparator()} games, avg.: ${msToTimeString(stat.Statistics.TotalMsThinkTime / stat.Statistics.GameCount)} / game")
            }
        }
    }

    fun showEloStatistics() = doShowEloStatistics(null)

    fun singleTurn()
    {
        // theoretically we assume, the Elos of all players are normally distributed. We could achieve this by
        // using the known elo in assigning probabilities to choosing a particular player for the next turn
        // for now just use equal distribution
        val firstPlayerIndex = RandomProvider.next(_availablePlayers.size)
        val rawNextPlayerIndex = RandomProvider.next(_availablePlayers.size - 1)
        val nextPlayerIndex = if(rawNextPlayerIndex < firstPlayerIndex)
            rawNextPlayerIndex
        else
            rawNextPlayerIndex + 1

        val player1 = _availablePlayers[firstPlayerIndex]
        val player2 = _availablePlayers[nextPlayerIndex]
        val sw = Stopwatch()
        val res = _playGame(player1, player2)
        val stats = updateElo(_store, player1, player2, res)
        _logSettings.LogSingleGameOutcome("$player1 vs. $player2 ${sw.toTimeString()}: ${res.OutcomePlayer1}, elo change: $stats")
    }

    private inner class PlayerStats(val Player : TPlayer, val Statistics : GameStatistics)

    fun runInfinite(maxRuns : Int? = null)
    {
        var runCnt = 0

        val sw = Stopwatch()

        val elos = HashMap<TPlayer, Int>()
        val ranks = HashMap<TPlayer, Int>()
        fun fillMapsFromStats()
        {
            elos.clear()
            ranks.clear()
            for(player in _availablePlayers)
                elos[player] = _store.getStatisticOrNewPlayerVal(player, 1000).Elo

            val rankedPlayers = _availablePlayers.sortedBy { -elos[it]!! }
            for(i in rankedPlayers.indices)
                ranks[rankedPlayers[i]] = i + 1
        }

        fillMapsFromStats()

        while (true)
        {
            ++runCnt
            if(maxRuns != null && runCnt > maxRuns)
            {
                _logSettings.RegularLogStats("\r\nfinished simulation of $maxRuns in ${sw.toTimeString()}")
                return
            }

            singleTurn()

            if(runCnt % _logSettings.TurnsToLogStats == 0)
            {
                // from time to time log general statistics
                doShowEloStatistics(CurrentRunInformation(elos, ranks, runCnt, sw.toTimeString()))
                fillMapsFromStats()
            }
        }
    }
}