package gulligames.elo

import gullitools.util.Stopwatch

data class GameStatistics(val GameCount : Int, val Elo : Int, val TotalMsThinkTime : Long)

interface IEloStore<TPlayer>
{
    fun getStatistics(player : TPlayer) : GameStatistics?
    //fun addGame(player: TPlayer, newElo : Int)
    fun setStatistics(player: TPlayer, newStatistics : GameStatistics)
}

fun <TPlayer> IEloStore<TPlayer>.getStatisticOrNewPlayerVal(player: TPlayer, newPlayerElo : Int) : GameStatistics
{
    return getStatistics(player) ?: GameStatistics(0, newPlayerElo, 0)
}

fun <TPlayer> IEloStore<TPlayer>.addGame(player: TPlayer, newElo: Int, thinkTimeMs : Long)
{
    val oldStats = getStatistics(player)
    val oldGameCount = if(oldStats == null) 0 else oldStats.GameCount
    val oldThinkTime = if(oldStats == null) 0 else oldStats.TotalMsThinkTime

    setStatistics(player, GameStatistics(oldGameCount + 1, newElo, oldThinkTime + thinkTimeMs))
}

class MemoryEloStore<TPlayer> : IEloStore<TPlayer>
{
    private val _store = HashMap<TPlayer, GameStatistics>()
    override fun getStatistics(player: TPlayer) = _store[player]
    override fun setStatistics(player: TPlayer, newStatistics: GameStatistics)
    {
        _store[player] = newStatistics
    }
}

data class EloFileEntry(val PlayerName : String, val Statistics : GameStatistics)

interface IEloFileHandler
{
    fun load(fileName : String) : List<EloFileEntry>
    fun save(entries : List<EloFileEntry>, fileName: String)
}

class FileEloStore<TPlayer>(private val _fileName : String, private val _fileHandler : IEloFileHandler, private val availablePlayers : Set<TPlayer>) : IEloStore<TPlayer>
{
    private val _store = HashMap<TPlayer, GameStatistics>()
    private val _corpseEntries : List<EloFileEntry>
    private var _lastFileWrite : Stopwatch
    init
    {
        val unknownEntries = ArrayList<EloFileEntry>()
        _corpseEntries = unknownEntries

        try
        {
            val currentEntries = _fileHandler.load(_fileName)

            val playersByName = HashMap<String, TPlayer>()
            for(p in availablePlayers)
            {
                val name = p.toString()
                if(playersByName.containsKey(name))
                    throw IllegalStateException("Multiple players have same name $name")

                playersByName[name] = p
            }


            for(entry in currentEntries)
            {
                val player = playersByName[entry.PlayerName]
                if(player == null)
                    unknownEntries.add(entry)
                else if(_store.containsKey(player))
                    throw IllegalStateException("Multiple entries for '$player'")
                else
                    _store[player] = entry.Statistics
            }
        }
        catch (ex : Exception)
        {
            // something went wrong in reading the file - probably it was not present
            // just assume it was empty
        }
        finally
        {
            _lastFileWrite = Stopwatch()
        }
    }

    override fun getStatistics(player: TPlayer) = _store[player]

    private fun writeToFile()
    {
        val entries = _store.asSequence().map { EloFileEntry(it.key.toString(), it.value) }
            .plus(_corpseEntries)
            .sortedBy { -it.Statistics.Elo }
            .toList()

        _fileHandler.save(entries, _fileName)
    }

    override fun setStatistics(player: TPlayer, newStatistics: GameStatistics)
    {
        _store[player] = newStatistics

        // if time since last write is long enough, write file again
        if(_lastFileWrite.ElapsedSeconds > 10.0)
        {
            writeToFile()
            _lastFileWrite = Stopwatch()
        }
    }
}