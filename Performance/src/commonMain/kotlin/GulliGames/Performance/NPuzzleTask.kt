package gulligames.performance

import gullitools.math.RandomProvider
import gulligames.io.Console
import gulligames.npuzzle.base.NPuzzleGame

private fun solveNPuzzle(length: Int, seed: Int)
{
    RandomProvider.makeRepeatable(seed)
    val game = NPuzzleGame.createWithRandomState(length)
    val moveCnt = gulligames.npuzzle.base.solveNPuzzle(game)
    Console.log("$moveCnt moves")
}

fun nPuzzleTask(length: Int, seed : Int) = PerformanceMeasureTask("NPuzzle-$length($seed)",
    "Solve a ${length}x${length}-NPuzzle initialized with a random seed of $seed")
    { solveNPuzzle(length, seed) }