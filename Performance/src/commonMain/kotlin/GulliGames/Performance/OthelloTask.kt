package gulligames.performance

import gulligames.othello.base.*
import gulligames.io.Console
import gullitools.util.Index2D
import gullitools.util.Stopwatch
import kotlin.math.min
import kotlin.math.roundToInt

private fun getTimeString(totalElapsedSeconds : Double, runs : Int) : String
{
    val avg = ((totalElapsedSeconds / runs) * 1000.0).roundToInt() / 1000.0
    return "avg. $avg s"
}

private fun getWinsString(cnt : Int, total : Int) : String
{
    val percent = (100.0 * cnt / total).roundToInt()
    return "${cnt}x ($percent %)"
}

private data class GameResult(val FinalGameState: Game, val Moves : List<Index2D>)

private class DirectedRunResult(val WhiteWinCnt: Int, val BlackWinCnt : Int, val DrawCnt : Int, val WhiteSeconds : Double, val BlackSeconds: Double, val TotalElapsedSeconds : Double)

private fun runSingleComputerVsComputerRun(playerWhite : IPlayer, playerBlack : IPlayer) : GameResult
{
    val game = Game()

    fun IPlayer.getActualPlayer() = this
    val actualPlayerWhite = playerWhite.getActualPlayer()
    val actualPlayerBlack = playerBlack.getActualPlayer()

    val moves = ArrayList<Index2D>()

    while (!game.IsGameOver)
    {
        val player = if(game.WhitesTurn) actualPlayerWhite else actualPlayerBlack
        val move = player.proposeMove(game).Move
        game.set(move)
        moves.add(move)
    }

    return GameResult(game, moves)
}

private fun getDirectedComputerVsComputerSimulation(turns : Int, white : IPlayer, black : IPlayer) : DirectedRunResult
{
    val playerWhite = white.collectTime()
    val playerBlack = black.collectTime()

    var whiteWinCnt = 0
    var blackWinCnt = 0
    var drawCnt = 0

    val uniqueResults = HashSet<Game>()

    val sw = Stopwatch()
    var lastElapsedMs = sw.ElapsedMilliseconds
    var lastTimeStepMs = 10000.0

    fun printStats(run : Int)
    {
        val elapsedMs = sw.ElapsedMilliseconds
        if(elapsedMs - lastElapsedMs < lastTimeStepMs )
            return

        val avgTimePerRun = sw.ElapsedMilliseconds / run
        val expectedRemainingTimeMs  = (turns - run) * avgTimePerRun
        if(expectedRemainingTimeMs < 0.3 * lastTimeStepMs)
            return // short before finishing this directed run

        lastElapsedMs = elapsedMs

        // was this the preLast or last log msg
        if(expectedRemainingTimeMs < 2.0 * lastTimeStepMs && expectedRemainingTimeMs > 1.7 * lastTimeStepMs)
        {
            // yes - just break in the middle
            lastTimeStepMs = expectedRemainingTimeMs * 0.5
        }
        else if(expectedRemainingTimeMs > 2.0 * lastTimeStepMs)
        {
            // compute expected total time to adjust timeStep
            val expectedLogMessages = (avgTimePerRun * turns) / lastTimeStepMs
            val maxLogMessages = 10
            if (expectedLogMessages > maxLogMessages)
            {
                val maxFactor = min(2.0, expectedRemainingTimeMs / lastTimeStepMs / 2.0)

                val adjustmentFactor = min(maxFactor, 1.0 * expectedLogMessages / maxLogMessages)
                lastTimeStepMs *= adjustmentFactor
            }
        }

        val total = whiteWinCnt + blackWinCnt + drawCnt

        fun getPercentStr(cnt : Int) : String
        {
            val percent = (100.0 * cnt / total).roundToInt()
            return "$percent %"
        }

        println("$run / $turns: wins: white: ${getPercentStr(whiteWinCnt)}, black: ${getPercentStr(blackWinCnt)}, draws: ${getPercentStr(drawCnt)}, (duplicate games: ${run - uniqueResults.size})")
    }

    for(i in 1 .. turns)
    {
        val result = runSingleComputerVsComputerRun(playerWhite, playerBlack)

        when(result.FinalGameState.Winner!!)
        {
            PlayerColor.White -> ++whiteWinCnt
            PlayerColor.Black -> ++blackWinCnt
            PlayerColor.Empty -> ++drawCnt
        }

        uniqueResults.add(result.FinalGameState)

        printStats(i)
    }

    return DirectedRunResult(whiteWinCnt, blackWinCnt, drawCnt, playerWhite.TotalElapsedSeconds, playerBlack.TotalElapsedSeconds, sw.ElapsedSeconds)
}

private fun runOthello(turns: Int, player1: IPlayer, player2: IPlayer)
{
    fun totalTimeStr(totalSeconds : Double) = "${totalSeconds.toInt()} s"

    fun playDirected(playerWhite: IPlayer, playerBlack : IPlayer) : DirectedRunResult
    {
        return getDirectedComputerVsComputerSimulation(turns, playerWhite, playerBlack)
    }

    val result1 = playDirected(player1, player2)
    val result2 = playDirected(player2, player1)

    fun totalWinStr(totalWinCnt : Int) = getWinsString(totalWinCnt, 2*turns)

    Console.log("Total results:")
    Console.log("\tby Color:")
    Console.log("\t\twins: white: ${totalWinStr(result1.WhiteWinCnt+result2.WhiteWinCnt)}, black: ${totalWinStr(result1.BlackWinCnt + result2.BlackWinCnt)}, Draws: ${totalWinStr(result1.DrawCnt + result2.DrawCnt)}")

    fun timeStr(totalTime1: Double, totalTime2 : Double) = getTimeString(totalTime1 + totalTime2, 2*turns)
    Console.log("\t\ttime: white: ${timeStr(result1.WhiteSeconds,result2.WhiteSeconds)}, black: ${timeStr(result1.BlackSeconds,result2.BlackSeconds)}, total: ${totalTimeStr(result1.TotalElapsedSeconds + result2.TotalElapsedSeconds)}")
    Console.log("\tby strategy/player:")
    Console.log("\t\t${player1.StrategyName}:\r\n\t\t\twins: ${totalWinStr(result1.WhiteWinCnt + result2.BlackWinCnt)}, Time: ${timeStr(result1.WhiteSeconds, result2.BlackSeconds)}")
    Console.log("\t\t${player2.StrategyName}:\r\n\t\t\twins: ${totalWinStr(result1.BlackWinCnt + result2.WhiteWinCnt)}, Time: ${timeStr(result1.BlackSeconds, result2.WhiteSeconds)}")
}

fun othelloTask(turns: Int, player1 : IPlayer, player2: IPlayer, player1ShortName : String = player1.StrategyName, player2ShortName : String = player2.StrategyName) : PerformanceMeasureTask
{
    val player2Str = if(player1ShortName == player2ShortName) "itself" else player2ShortName

    // 2* turns, because both players can play as white and as black
    return PerformanceMeasureTask (
            "${turns * 2}x Othello ($player1ShortName vs. $player2Str)", "Play ${2 * turns}x Othello ($player1ShortName vs. $player2Str)",
    { runOthello(turns, player1, player2) })
}