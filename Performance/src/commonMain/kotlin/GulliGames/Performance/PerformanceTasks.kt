package gulligames.performance

import gulligames.othello.base.ConstantTimeComputerPlayer
import gulligames.othello.base.SimpleComputerPlayer
import gullitools.draw2d.Rect
import gullitools.draw2d.Size

val PerformanceTasks = arrayOf(
    //computePrimTask(10000),
    computePrimTask(20000),
    computePrimTask(50000),
    //computePrimTask(100000),
    //mandelTask(Rect(-2.0, -2.0, 4.0, 4.0), Size(1000.0, 1000.0)),
    mandelTask(Rect(-0.75, -0.25, 0.01, 0.01), Size(1000.0, 1000.0)),
    mandelTask(Rect(-0.7395, -0.2423, 0.0005, 0.0005), Size(1000.0, 1000.0)),

    //nPuzzleTask(3, 0),
    nPuzzleTask(3, 37),
    nPuzzleTask(4, 4),
    //nPuzzleTask(4, 20),

   // othelloTask(1, SimpleComputerPlayer(4), SimpleComputerPlayer(4)),
    othelloTask(1, SimpleComputerPlayer(3), ConstantTimeComputerPlayer(100), "fixed-Depth-3", "constTime(100)"),
    othelloTask(1, SimpleComputerPlayer(4), ConstantTimeComputerPlayer(100), "fixed-Depth-4", "constTime(100)"),
    othelloTask(1, SimpleComputerPlayer(3), ConstantTimeComputerPlayer(1000), "fixed-Depth-3", "constTime(1000)"),
)