package gulligames.performance

import gullitools.util.Stopwatch
import gullitools.util.msToTimeString

data class PerformanceMeasureTask(val Title : String, val Description : String, private val _action : () -> Unit)
{
    fun execute() : PerformanceMeasureResult
    {
        val sw = Stopwatch()
        _action()
        return PerformanceMeasureResult(this, sw.ElapsedMilliseconds)
    }

    override fun toString() = Title
}

data class PerformanceMeasureResult(val Task : PerformanceMeasureTask, val ElapsedMilliseconds : Long)
{
    val TimeString : String
        get() = msToTimeString(ElapsedMilliseconds)

    override fun toString() = "$Task: $TimeString"
}
