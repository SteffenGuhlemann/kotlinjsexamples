package gulligames.performance

import gulligames.mandelbrot.base.InterpolatingColorProvider
import gulligames.mandelbrot.base.SimpleAdaptiveDepthMandelbrotRenderer
import gullitools.draw2d.*
import gulligames.mandelbrot.gui.*

private object DrawingContextMock : IDrawingContext
{
    override fun drawRectangle(fill: Brush?, pen: Pen?, rect: Rect)
    {
    }

    override fun drawLine(pen: Pen, start: Point, end: Point)
    {
    }

    override fun drawPolygon(fill: Brush?, pen: Pen?, polygon: CombinedPolygon)
    {
    }

    override fun drawEllipse(fill: Brush?, pen: Pen?, center: Point, horizontalRadius: Double, verticalRadius: Double)
    {
    }

    override fun drawText(text: FormattedText, position: Point)
    {
    }

    override fun measureMinWidthWithoutTrimming(text: FormattedText) = measureText(text).Width

    override fun measureText(text: FormattedText): Size
    {
        val h = text.FontSize
        val w = text.Text.length * text.FontSize
        return Size(w, h)
    }

    override fun pop()
    {
    }

    override fun pushClip(clipRegion: Rect)
    {
    }

    override fun pushTransform(transform: Transform)
    {
    }

    override fun tryDrawAsBitmapIntoRect(bmpRect: Rect) = null

    override fun drawPolygon(fill: Brush?, pen: Pen?, polygon: Polygon)
    {
    }
}

private class MandelViewMock(private val _drawSize : Size) : IView
{
    val ViewModel = ViewModel(this, InterpolatingColorProvider(Blue, Black, White, 10),
        SimpleAdaptiveDepthMandelbrotRenderer(), true)

    override fun drawRubberBandOnTopOfImage(pen: Pen, rubberBandRect: Rect)
    {
    }

    override fun triggerRedraw(functionRect: Rect)
    {
        ViewModel.repaint(DrawingContextMock, _drawSize, functionRect)
    }

    override fun triggerRefreshOverviewCanvas(functionRectHistory: List<Rect>, currMainFunctionRect: Rect)
    {
    }
}

private fun computeMandel(fctRect : Rect, drawSize : Size)
{
    val view = MandelViewMock(drawSize)
    view.ViewModel.setPosition(fctRect)
}

fun mandelTask(fctRect : Rect, drawSize : Size) : PerformanceMeasureTask
{
    val shortRctStr = "[${fctRect.Left};${fctRect.Top})->${fctRect.Size}]"
    return PerformanceMeasureTask("Mandel($shortRctStr, draw:$drawSize)", "Compute a Mandelbrot set across size $drawSize at $shortRctStr") { computeMandel(fctRect, drawSize) }
}