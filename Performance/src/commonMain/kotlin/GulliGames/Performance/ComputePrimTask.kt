package gulligames.performance

import gulligames.io.Console
import kotlin.math.floor
import kotlin.math.roundToLong
import kotlin.math.sqrt

private fun isPrim(number : Long) : Boolean
{
    if(number.mod(2L) == 0L)
        return false

    val maxIncl = floor(sqrt(number.toDouble())).roundToLong()
    var curr = 3
    while (curr <= maxIncl)
    {
        if(number.mod(curr) == 0)
            return false

        curr += 2
    }

    return true
}

private fun computePrim(primCnt : Int)
{
    var primsFound = 1
    var curr : Long = 1L
    while (primsFound < primCnt)
    {
        curr += 2L
        if(isPrim(curr))
            ++primsFound
    }

    Console.log("The ${primCnt}. prim is $curr")
}

fun computePrimTask(primCnt : Int) = PerformanceMeasureTask("Prim($primCnt)", "Compute first $primCnt prims"){ computePrim(primCnt) }
