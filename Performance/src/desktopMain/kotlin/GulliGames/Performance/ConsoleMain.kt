package gulligames.performance

import gulligames.io.Console
import gullitools.util.msToTimeString

fun consoleMain(platform : String)
{
    println("\r\nTesting performance for $platform!")
    println("Conducting ${PerformanceTasks.size} tasks")
    Console.DoLogToConsole = false

    var totalTimeMs = 0L
    for(i in PerformanceTasks.indices)
    {
        val task = PerformanceTasks[i]
        print("${i + 1}. ${task.Title}: ")
        val res = task.execute()
        println(res.TimeString)
        totalTimeMs += res.ElapsedMilliseconds
    }

    println()
    println("Total: ${msToTimeString(totalTimeMs)}")
}

