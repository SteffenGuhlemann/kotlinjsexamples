package gulligames.performance

import gullitools.util.msToTimeString
import gullitools.html.createAndAdd
import gullitools.html.getElement
import kotlinx.browser.window
import org.w3c.dom.*

private val _titleH1 : HTMLElement = getElement("titleH1")
private val _mainDiv : HTMLDivElement = getElement("mainDiv")

fun main()
{
    val coreTitle = "Performance von Kotlin-2-Javascript"

    _titleH1.innerText = "$coreTitle - testing ${PerformanceTasks.size} tasks ..."
    println("testing ${PerformanceTasks.size} tasks")
    val table = _mainDiv.createAndAdd("table") /*{ it.style.border = "1px solid black" }*/ as HTMLTableElement
    val headerRow = table.createAndAdd("tr") as HTMLTableRowElement
    headerRow.createAndAdd("th")
    for(colTitle in sequenceOf("Task", "Description", "Time"))
        headerRow.createAndAdd("th") {
            it.style.border = "1px solid black"
            it.innerText = colTitle
        }

    var totalTimeMs = 0L
    for(i in PerformanceTasks.indices)
    {
        window.setTimeout({
                val task = PerformanceTasks[i]
                println("executing task ${i + 1} / ${PerformanceTasks.size}: ${task.Title}")
                val res = task.execute()
                println("\ttook ${res.TimeString}")

                val row = table.createAndAdd("tr") as HTMLTableRowElement
                fun String.addAsCell() = row.createAndAdd("td"){
                    it.style.border = "0.5px solid black"
                    it.innerText = this
                }

                "${i + 1}.".addAsCell()
                task.Title.addAsCell()
                task.Description.addAsCell()
                res.TimeString.addAsCell()
                totalTimeMs += res.ElapsedMilliseconds

                _titleH1.innerText = "$coreTitle - finished task ${i + 1} / ${PerformanceTasks.size} ..."
            }, 10)
    }

    window.setTimeout({
        _titleH1.innerText = coreTitle
        table.createAndAdd("th")
        table.createAndAdd("th") {
            it.style.border = "1px solid black"
            it.innerText = "Total"
            (it as HTMLTableCellElement).colSpan = 2
        }
        table.createAndAdd("th"){
            it.style.border = "1px solid black"
            it.innerText = msToTimeString(totalTimeMs)
        }

        println("finished in $totalTimeMs ms")
                      }, 10)
}