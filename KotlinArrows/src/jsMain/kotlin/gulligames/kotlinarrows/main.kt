package gulligames.kotlinarrows

import gullitools.html.HtmlCanvasDrawingContext
import gullitools.html.getElement
import gullitools.draw2d.*
import org.w3c.dom.*
import kotlin.math.PI

class PageHandler
{
    constructor()
    {
        _originXTb = getAndRegister("OriginXTb");
        _originYTb = getAndRegister("OriginYTb");
        _endXTb = getAndRegister("EndXTb");
        _endYTb = getAndRegister("EndYTb");
        _canvas = getElement("MyCanvas");
        _context = HtmlCanvasDrawingContext(_canvas);

        _arrowLengthTb = getAndRegister("ArrowLengthTb");
        _arrowWidthTb = getAndRegister("ArrowWidthTb");

        TbValChanged()
    }

    private val _originXTb: HTMLInputElement;
    private val _originYTb: HTMLInputElement;
    private val _endXTb: HTMLInputElement;
    private val _endYTb: HTMLInputElement;
    private val _arrowLengthTb: HTMLInputElement;
    private val _arrowWidthTb: HTMLInputElement;
    private val _canvas: HTMLCanvasElement;
    private val _context: HtmlCanvasDrawingContext;

    private fun getAndRegister(id: String): HTMLInputElement
    {
        val tb: HTMLInputElement = getElement(id);
        tb.onchange = { _ -> TbValChanged() };
        return tb;
    }

    private fun TbValChanged()
    {
        val start = Point(_originXTb.valueAsNumber, _originYTb.valueAsNumber)
        val end = Point(_endXTb.valueAsNumber, _endYTb.valueAsNumber)

        _context.drawRectangle(White.AsBrush, null, Rect(0.0, 0.0, 1.0 * _canvas.width, 1.0 * _canvas.height))
        _context.drawLine(Pen(Blue), start, end)

        // arrow:
        // vector from start to end
        val startEndVec = end - start
        val startEndNormalized = startEndVec.AsNormalizedVector

        val tipOfArrow = end + (startEndNormalized * _arrowLengthTb.valueAsNumber)

        val rotatedVec = startEndNormalized.getRotated(PI * 0.5)
        val lowerArrowCorner = end + rotatedVec * (_arrowWidthTb.valueAsNumber * 0.5)
        val upperArrowCorner = end + rotatedVec * (-_arrowWidthTb.valueAsNumber * 0.5)

        val pen = Pen(Red)
        _context.drawLine(pen, tipOfArrow, lowerArrowCorner)
        _context.drawLine(pen, lowerArrowCorner, upperArrowCorner)
        _context.drawLine(pen, upperArrowCorner, tipOfArrow)
    }
}

fun main()
{
    val handler = PageHandler()
}

